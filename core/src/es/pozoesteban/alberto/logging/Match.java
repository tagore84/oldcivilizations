package es.pozoesteban.alberto.logging;
import es.pozoesteban.alberto.model.GameState;
import es.pozoesteban.alberto.model.entities.BoardCardsSimple;
import es.pozoesteban.alberto.model.entities.ProgressToken;
import es.pozoesteban.alberto.model.entities.Wonder;
import es.pozoesteban.alberto.model.enums.Era;
import es.pozoesteban.alberto.model.enums.WinType;
import es.pozoesteban.alberto.model.players.Move;
import es.pozoesteban.alberto.model.players.Player;

import java.io.Serializable;
import java.util.*;

public class Match implements Serializable {

    private static final long serialVersionUID = 1L;

    private final List<Wonder> firstFourWonders;
    private final List<Wonder> secondFourWonders;

    private final List<ProgressToken> boarddProgressTokens;
    private final List<ProgressToken> extraThreeProgressTokens;

    private final EnumMap<Era, BoardCardsSimple> figures;

    private final HashMap<Player, List<MoveWithGameState>> playerMoves;

    private final GameState gameState;

    private final boolean downFirstPlayer;
    private boolean downWins;
    private WinType winType;

    public Match(GameState gameState, Player down, Player up, boolean downFirstPlayer) {
        this.gameState = gameState;
        firstFourWonders = new ArrayList();
        secondFourWonders = new ArrayList();
        boarddProgressTokens = new ArrayList();
        extraThreeProgressTokens = new ArrayList();
        figures = new EnumMap<Era, BoardCardsSimple>(Era.class);
        playerMoves = new HashMap();
        playerMoves.put(down, new ArrayList());
        playerMoves.put(up, new ArrayList());
        this.downFirstPlayer = downFirstPlayer;
    }

    public void saveWinner(boolean downWins, WinType winType) {
        this.downWins = downWins;
        this.winType = winType;
    }

    public void saveInititalWonder(boolean firstFour, Wonder wonder) {
        if (firstFour) firstFourWonders.add(wonder);
        else secondFourWonders.add(wonder);
    }

    public void saveProgressToken(boolean board, ProgressToken token) {
        if (board) boarddProgressTokens.add(token);
        else extraThreeProgressTokens.add(token);
    }

    public void saveMove(Player player, Move move, GameState gameState) throws CloneNotSupportedException {
        playerMoves.get(player).add(new MoveWithGameState(move, gameState.clone()));
    }

    public void saveFigure(Era era, BoardCardsSimple figure) throws CloneNotSupportedException {
        figures.put(era, figure.clone());
    }

    public Player getWinner() {
        for (Player player : playerMoves.keySet()) {
            if (player.isDownPlayer() == downWins) return player;
        }
        return null;
    }
    public Player getLosser() {
        for (Player player : playerMoves.keySet()) {
            if (player.isDownPlayer() != downWins) return player;
        }
        return null;
    }

    private class MoveWithGameState implements Serializable{
        private final Move move;
        private final GameState gameState;
        public MoveWithGameState(Move move, GameState gameState) {
            this.move = move;
            this.gameState = gameState;
        }
    }

    //<editor-fold desc="Overrides">
    @Override
    public String toString() {
        Player winner = null;
        Player losser = null;
        for (Player player : playerMoves.keySet()) {
            if (player.isDownPlayer() == downWins) winner = player;
            else losser = player;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(winner).append(" [").append(winner.getFinalPoints(gameState)).append(" pts] wins to ")
                .append(losser).append(" [").append(losser.getFinalPoints(gameState)).append(" pts] ").append(winType);
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Match match = (Match) o;
        return downFirstPlayer == match.downFirstPlayer &&
                downWins == match.downWins &&
                Objects.equals(firstFourWonders, match.firstFourWonders) &&
                Objects.equals(secondFourWonders, match.secondFourWonders) &&
                Objects.equals(boarddProgressTokens, match.boarddProgressTokens) &&
                Objects.equals(extraThreeProgressTokens, match.extraThreeProgressTokens) &&
                Objects.equals(figures, match.figures) &&
                Objects.equals(playerMoves, match.playerMoves) &&
                winType == match.winType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstFourWonders, secondFourWonders, boarddProgressTokens, extraThreeProgressTokens, figures, playerMoves, downFirstPlayer, downWins, winType);
    }

    //</editor-fold>
}