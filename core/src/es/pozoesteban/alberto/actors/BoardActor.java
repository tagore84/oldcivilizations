package es.pozoesteban.alberto.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Logger;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;
import static es.pozoesteban.alberto.screens.BoardScreen.LAYOUT;

public class BoardActor extends Actor {

    private final static Logger LOG = new Logger("BoardActor", LOG_LEVEL);

    private Texture texture;
    private TextureRegion region;

    public BoardActor(Texture texture) {
        this.texture = texture;
        this.setTouchable(Touchable.disabled);
        region = new TextureRegion(texture, 0, 0, 200, 712);
        this.setZIndex(0);
        this.setVisible(true);
        this.setSize(LAYOUT.boardAbsoluteSize()[0], LAYOUT.boardAbsoluteSize()[1]);
        this.setPosition(LAYOUT.board()[0], LAYOUT.board()[2]);

    }
    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(region, getX(), getY(), getWidth(), getHeight());
    }
}
