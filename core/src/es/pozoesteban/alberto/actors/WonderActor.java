package es.pozoesteban.alberto.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Logger;

import es.pozoesteban.alberto.GameApplication;
import es.pozoesteban.alberto.model.entities.Wonder;
import es.pozoesteban.alberto.model.enums.WonderPosition;
import es.pozoesteban.alberto.model.enums.WonderState;
import es.pozoesteban.alberto.model.exceptions.UnknownOrInvalidPosition;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;
import static es.pozoesteban.alberto.screens.BoardScreen.LAYOUT;

public class WonderActor extends EntityActor {

    private final static Logger LOG = new Logger("WonderActor", LOG_LEVEL);

    public final static int REGION_COMPLETE_WIDTH = 500;
    public final static int REGION_COMPLETE_HEIGHT = 325;

    public final static int REGION_RESUME_WIDTH = 204;
    public final static int REGION_RESUME_HEIGHT = 300;

    private final TextureRegion resumeRegion;
    private final float[] sizeResume;
    private final float[] sizeComplete;

	
    public WonderActor(GameApplication gameApplication, Wonder wonder, Texture completeTexture, Texture resumeTexture) {
        super(gameApplication, wonder);

        int id = wonder.getId();
        int xComplete = (id % 3) * REGION_COMPLETE_WIDTH;
        int yComplete = (id / 3) * REGION_COMPLETE_HEIGHT;
        region = new TextureRegion(completeTexture, xComplete, yComplete, REGION_COMPLETE_WIDTH, REGION_COMPLETE_HEIGHT);

        int xResume = (id % 5) * REGION_RESUME_WIDTH;
        int yResume = (id / 5) * REGION_RESUME_HEIGHT;
        resumeRegion = new TextureRegion(resumeTexture, xResume, yResume, REGION_RESUME_WIDTH, REGION_RESUME_HEIGHT);


        sizeResume = LAYOUT.wonderResumeAbsoluteSize();
        sizeComplete = LAYOUT.wonderCompleteAbsoluteSize();
    }

    //<editor-fold desc="Overrides">
    @Override
    public void draw(Batch batch, float parentAlpha) {
        updateSizeWithoutZoom();
        switch ((WonderState)entity.peekState()) {
            case OUT:
                break;
            case SELECTABLE:
                batch.draw(region, getX(), getY(), getWidth(), getHeight());
                break;
            case SELECTED_BY_DOWN: case SELECTED_BY_UP: case PLAYED_BY_DOWN: case PLAYED_BY_UP:
                batch.draw(resumeRegion, getX(), getY(), getWidth(), getHeight());
                break;
        }
    }
    @Override
    public float[] calcPosition() throws UnknownOrInvalidPosition {
        WonderState state = (WonderState) entity.peekState();
        WonderPosition position = (WonderPosition) entity.peekPosition();
        switch (state) {
            case PLAYED_BY_DOWN: case SELECTED_BY_DOWN:
                return LAYOUT.wonders(true, position.getIndex());
            case PLAYED_BY_UP: case SELECTED_BY_UP:
                return LAYOUT.wonders(false, position.getIndex());
            case SELECTABLE:
                return LAYOUT.wonderSelectable(position.getIndex());
            default:
                return new float[] {-1 * getWidth(), -1 * getHeight()};
        }
    }
    @Override
    protected void updateSizeWithoutZoom() {
        switch ((WonderState)entity.peekState()) {
            case SELECTABLE:
                setWidth(sizeComplete[0]);
                setHeight(sizeComplete[1]);
                break;
            case SELECTED_BY_DOWN: case SELECTED_BY_UP: case PLAYED_BY_DOWN: case PLAYED_BY_UP:
                setWidth(sizeResume[0]);
                setHeight(sizeResume[1]);
                break;
            case OUT:
                setWidth(0);
                setHeight(0);
                break;
        }
    }
    //</editor-fold>
}
