package es.pozoesteban.alberto.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Logger;

import es.pozoesteban.alberto.GameApplication;
import es.pozoesteban.alberto.model.entities.ProgressToken;
import es.pozoesteban.alberto.model.enums.ProgressTokenPosition;
import es.pozoesteban.alberto.model.enums.ProgressTokenState;
import es.pozoesteban.alberto.model.exceptions.UnknownOrInvalidPosition;
import es.pozoesteban.alberto.screens.BoardScreen;
import es.pozoesteban.alberto.screens.BoardScreenPosition;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;
import static es.pozoesteban.alberto.screens.BoardScreen.LAYOUT;

public class ProgressTokenActor extends EntityActor {

    private final static Logger LOG = new Logger("ProgressTokenActor", LOG_LEVEL);

    private final static int REGION_WITH = 400;
    private final static int REGION_HEIGHT = 400;
    private final float[] size;
    public ProgressTokenActor(GameApplication gameApplication, ProgressToken progressToken, Texture texture) {
        super(gameApplication, progressToken);

        int id = progressToken.getId();
        int x = (id % 2) * REGION_WITH;
        int y = (id / 2) * REGION_HEIGHT;

        size = LAYOUT.progressTokensAbsoluteSize();
        setWidth(size[0]);
        setHeight(size[1]);

        region = new TextureRegion(texture, x, y, REGION_WITH, REGION_HEIGHT);
    }

    //<editor-fold desc="Overrides">
    @Override
    public float[] calcPosition() throws UnknownOrInvalidPosition {
        ProgressTokenPosition position = (ProgressTokenPosition) entity.peekPosition();
        ProgressTokenState state = (ProgressTokenState) entity.peekState();
        switch (state) {
            case ON_BOARD:
                return LAYOUT.progressTokenOnBoard(position.getIndex());
            case PLAYED_BY_DOWN:
                return LAYOUT.progressTokenPlayed(true, position.getIndex());
            case PLAYED_BY_UP:
                return LAYOUT.progressTokenPlayed(false, position.getIndex());
            case SELECTABLE:
                return LAYOUT.progressTokenSelectable(position.getIndex());
            default:
                return new float[]{-1 * getWidth(), -1 * getHeight()};
        }
    }
    @Override
    protected void updateSizeWithoutZoom() {
        // Do nothing, fixed size...
    }
    //</editor-fold>
}
