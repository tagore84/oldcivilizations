package es.pozoesteban.alberto.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

import static es.pozoesteban.alberto.screens.BoardScreen.HEIGHT_SCREEN;
import static es.pozoesteban.alberto.screens.BoardScreen.WIDTH_SCREEN;

public class BackgroundActor extends Actor {

    private final Texture texture;

    public BackgroundActor(Texture texture) {
        this.texture = texture;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(texture, 0, 0, WIDTH_SCREEN, HEIGHT_SCREEN);
    }
}
