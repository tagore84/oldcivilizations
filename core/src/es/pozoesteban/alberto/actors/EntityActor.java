package es.pozoesteban.alberto.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.utils.Logger;

import es.pozoesteban.alberto.GameApplication;
import es.pozoesteban.alberto.model.entities.Card;
import es.pozoesteban.alberto.model.entities.Entity;
import es.pozoesteban.alberto.model.enums.CardPosition;
import es.pozoesteban.alberto.model.enums.WonderPosition;
import es.pozoesteban.alberto.model.exceptions.UnknownOrInvalidPosition;
import es.pozoesteban.alberto.model.players.Move;
import es.pozoesteban.alberto.model.players.Player;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;
import static es.pozoesteban.alberto.screens.BoardScreen.*;


public abstract class EntityActor extends Actor {

    private static Logger LOG = new Logger("EntityActor", LOG_LEVEL);

    protected static final float MAX_DIST_RELATIVE = 0.01f;
    protected static final float ZOOM_FACTOR = 2.5f;

    protected final GameApplication gameApplication;
    protected Entity entity;
    protected TextureRegion region;
    private boolean isMoving;
    private float[] oldPosition;
    private boolean needMoreMovementX;
    private boolean needMoreMovementY;
    private float movementSpeedX;
    private float movementSpeedY;
    private boolean dragging;


    public EntityActor(final GameApplication gameApplication, final Entity entity) {
        this.gameApplication = gameApplication;
        this.entity = entity;
        this.isMoving = false;
        this.needMoreMovementX = true;
        this.needMoreMovementY = true;

        this.addListener(new DragListener() {
            @Override
            public void drag(InputEvent event, float x, float y, int pointer) {
                if (entity.isSelectable() && !gameApplication.getGameState().getCurrentPlayer().isAIPlayer()) {
                    moveBy(x - getWidth() / 2, y - getHeight() / 2);
                }
            }

        });
        this.addListener(new ClickListener() {

            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
//                 LOG.info("Enter in actor: " + this);
            }
            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor fromActor) {
//                LOG.info("Exit in actor: " + this);
            }

            @Override
            public boolean isOver() {
                // ToDo set zoom view... ¿enter and exit?
//                LOG.info("isOver in actor: " + this);
                return true;
            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (entity.isSelectable() && !gameApplication.getGameState().getCurrentPlayer().isAIPlayer()) {
                    dragging = true;
                    gameApplication.getGameState().getCurrentPlayer().isDragging(true);
                    return true;
                } else {
                    return false;
                }
            }
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                gameApplication.getGameState().getCurrentPlayer().isDragging(false);
                try {
                    Move move = getMove();
                    if (move != null && gameApplication.isValidMove(move)) {
                        gameApplication.humanMoveDo(move);
                    } else {
                        // Vuelve sola a su posición porque como no se ha cambiado entity.position...
                    }
                } catch (Exception ex) {
                    LOG.error("Error moving ", ex);
                }
                dragging = false;
            }
        });
    }

    private void normalAct(float delta) {
        updateSizeWithoutZoom();
        try {
            float[] newPosition = calcPosition();
            if (needMove(newPosition)) {
                if (gameApplication.isSimulatedMatch()) {
                    instantMove(newPosition);
                    aiMovementFinished();
                }
                if (isMoving) {
                    if (!needMoreMovementX && !needMoreMovementY) {
                        fineMove(newPosition);
                    }
                    if (needMoreMovementX) {
                        horizontalMove(delta, newPosition);
                    }
                    if (needMoreMovementY) {
                        verticalMove(delta, newPosition);
                    }
                } else if (entity.peekPosition() == WonderPosition.SELECTABLE_0 ||
                        entity.peekPosition() == WonderPosition.SELECTABLE_1 ||
                        entity.peekPosition() == WonderPosition.SELECTABLE_2 ||
                        entity.peekPosition() == WonderPosition.SELECTABLE_3){
                    instantMove(newPosition);
                } else {
                    initMovement(newPosition);
                }
            } else {
                aiMovementFinished();
            }
        } catch (UnknownOrInvalidPosition ex) {
            LOG.error("Error acting ", ex);
        }
    }
    private void zoomAct() {
        setWidth(getWidth() * ZOOM_FACTOR);
        setHeight(getHeight() * ZOOM_FACTOR);
        setX((WIDTH_SCREEN / 2.0f) - (getWidth() / 2.0f));
        setY((HEIGHT_SCREEN / 2.0f) - (getHeight() / 2.0f));

    }

    //<editor-fold desc="Overrides">
    @Override
    public void act(float delta) {
        this.setTouchable(entity.isSelectable() ? Touchable.enabled : Touchable.disabled);
        if (dragging) return;
        normalAct(delta);

    }
    private void fineMove(float[] newPosition) {
        LOG.debug("Afinando la posición un pixel...");
        if (getX() < newPosition[0]) {
            setX(getX() + 1);
        }
        if (getX() > newPosition[0]) {
            setX(getX() - 1);
        }
        if (getY() < newPosition[1]) {
            setY(getY() + 1);
        }
        if (getY() > newPosition[1]) {
            setY(getY() - 1);
        }
    }
    private void horizontalMove(float delta, float[] newPosition) {
        if (oldPosition[0] < newPosition[0]) {
            if (getX() < newPosition[0]) {
                setX(getX() + movementSpeedX * delta);
            } else {
                needMoreMovementX = false;
            }
        } else if (oldPosition[0] > newPosition[0]) {
            if (getX() > newPosition[0]) {
                setX(getX() - movementSpeedX * delta);
            } else {
                needMoreMovementX = false;
            }
        } else {
            LOG.error("oldPosition = " + oldPosition[0] + ", " + oldPosition[1]
                    + " newPosition: " + newPosition[0] + ", " + newPosition[1]);
            needMoreMovementX = false;
        }
    }
    private void verticalMove(float delta, float[] newPosition) {
        if (oldPosition[1] < newPosition[1]) {
            if (getY() < newPosition[1]) {
                setY(getY() + movementSpeedY * delta);
            } else {
                needMoreMovementY = false;
            }
        } else if (oldPosition[1] > newPosition[1]) {
            if (getY() > newPosition[1]) {
                setY(getY() - movementSpeedY * delta);
            } else {
                needMoreMovementY = false;
            }
        } else {
            LOG.error("oldPosition = " + oldPosition[0] + ", " + oldPosition[1]
                    + " newPosition: " + newPosition[0] + ", " + newPosition[1]);
            needMoreMovementY = false;
        }
    }
    private void instantMove(float[] newPosition) {
        setX(newPosition[0]);
        setY(newPosition[1]);
    }
    private void initMovement(float[] newPosition) {
        LOG.debug("Moving " + entity.getName() + " to (" + newPosition[0] + ", " + newPosition[1] + ")");
        isMoving = true;
        oldPosition = new float[]{getX(), getY()};
        needMoreMovementX = true;
        needMoreMovementY = true;
        movementSpeedX =  Math.abs(getX() - newPosition[0]) / MOVEMENT_TIME;
        movementSpeedY =  Math.abs(getY() - newPosition[1]) / MOVEMENT_TIME;
    }
    private void aiMovementFinished() {
        LOG.debug(entity + " moved to " + entity.peekPosition() + " (" + getX() + ", " + getY() + ")");
        isMoving = false;
        Player current = gameApplication.getGameState().getCurrentPlayer();
        if (current.isAIPlayer() && entity.isMovedByAIPlayer()) {
            try {
                LOG.debug("Teminado el movimiento de " + entity + ", llamando a aiMoveDo()");
                entity.isMovedByAIPlayer(false);
                gameApplication.onCurrentPlayerEndTurn();
            } catch (Exception ex) {
                LOG.error("Error on movement finish AI", ex);
            }
        }
    }

    protected boolean needMove(float[] newPos) {
        float dist = dist(newPos, new float[]{getX(), getY()});
        return dist > (MAX_DIST_RELATIVE * WIDTH_SCREEN);

    }
    protected float dist(float[] p1, float[] p2) {
        return (p1[0] - p2[0]) * (p1[0] - p2[0]) + (p1[1] - p2[1]) * (p1[1] - p2[1]);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (entity.isVisible()) {
            batch.draw(region, getX(), getY(), getWidth(), getHeight());
        }
    }
    //</editor-fold>

    //<editor-fold desc="Interface">
    protected abstract float[] calcPosition() throws UnknownOrInvalidPosition;
    protected abstract void updateSizeWithoutZoom();
    //</editor-fold>

    //<editor-fold desc="Implemented tools">
    protected Move getMove() throws UnknownOrInvalidPosition {
        return LAYOUT.guessMove(gameApplication.getGameState().getCurrentPlayer().isDownPlayer(), entity, getCenter());
    }
    protected float[] getCenter() {
        return new float[] {getX() + (getWidth()/2), getY() + (getHeight()/2)};
    }
    //</editor-fold>

    //<editor-fold desc="Getters">
    public Entity getEntity() {
        return this.entity;
    }
    //</editor-fold>
}
