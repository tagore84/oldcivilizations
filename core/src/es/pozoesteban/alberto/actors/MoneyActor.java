package es.pozoesteban.alberto.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.model.players.Player;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;
import static es.pozoesteban.alberto.screens.BoardScreen.LAYOUT;

public class MoneyActor extends Actor {

    private final static Logger LOG = new Logger("MoneyActor", LOG_LEVEL);

    // 2048 x 2048 -> 64 -> [0,63]
    public final static int MONEY_REGION_WITH = 256;
    public final static int MONEY_REGION_HEIGHT = 256;
    public final static int MONEY_REGIONS_BY_ROW = 8;

    // 1024 x 1024 -> 16 -> [2,15]
    public final static int SELLING_REGION_WITH = 256;
    public final static int SELLING_REGION_HEIGHT = 256;
    public final static int SELLING_REGIONS_BY_ROW = 4;

    private Texture moneyTexture;
    private Texture sellingTexture;
    private Player player;

    public MoneyActor(Texture moneyTexture, Texture sellingTexture, Player player) {
        this.moneyTexture = moneyTexture;
        this.sellingTexture = sellingTexture;
        this.player = player;
        this.setTouchable(Touchable.disabled);
        this.setZIndex(100);
        this.setVisible(true);
        this.setSize(LAYOUT.moneyAbsoluteSize()[0], LAYOUT.moneyAbsoluteSize()[1]);
        this.setPosition(LAYOUT.money(player.isDownPlayer())[0], LAYOUT.money(player.isDownPlayer())[2]);
    }

    @Override
    public void act(float delta) {

    }
    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (player.isDragging()) {
            int sellerPrice = player.getSellerPrice();
            int x = (sellerPrice % SELLING_REGIONS_BY_ROW) * SELLING_REGION_WITH;
            int y = (sellerPrice / SELLING_REGIONS_BY_ROW) * SELLING_REGION_HEIGHT;
            TextureRegion region = new TextureRegion(sellingTexture, x, y, SELLING_REGION_WITH, SELLING_REGION_HEIGHT);
            batch.draw(region, getX(), getY(), getWidth(), getHeight());
        } else {
            int money = player.getMoney();
            int x = (money % MONEY_REGIONS_BY_ROW) * MONEY_REGION_WITH;
            int y = (money / MONEY_REGIONS_BY_ROW) * MONEY_REGION_HEIGHT;
            TextureRegion region = new TextureRegion(moneyTexture, x, y, MONEY_REGION_WITH, MONEY_REGION_HEIGHT);
            batch.draw(region, getX(), getY(), getWidth(), getHeight());
        }


    }
}
