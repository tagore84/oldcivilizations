package es.pozoesteban.alberto.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import com.badlogic.gdx.utils.Logger;

import es.pozoesteban.alberto.GameApplication;
import es.pozoesteban.alberto.model.entities.CardPurple;
import es.pozoesteban.alberto.model.enums.CardPosition;
import es.pozoesteban.alberto.model.enums.CardState;
import es.pozoesteban.alberto.model.entities.Card;
import es.pozoesteban.alberto.model.enums.Era;
import es.pozoesteban.alberto.model.exceptions.UnknownOrInvalidPosition;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;
import static es.pozoesteban.alberto.screens.BoardScreen.LAYOUT;

public class CardActor extends EntityActor {

    private final static Logger LOG = new Logger("CardActor", LOG_LEVEL);

    public final static int REGION_COMPLETE_WIDTH = 259;
    public final static int REGION_COMPLETE_HEIGHT = 400;
    public final static int REGION_RESUME_WIDTH = 160;
    public final static int REGION_RESUME_HEIGHT = 80;


    private final TextureRegion back;
    private final TextureRegion resume;
    private final float[] sizeResume;
    private final float[] sizeComplete;

    public CardActor(GameApplication gameApplication, Card card, Texture texture, Texture resumeTexture) {
        super(gameApplication, card);

        int id = card.getIndex();
        int xComplete = (id % 6) * REGION_COMPLETE_WIDTH;
        int yComplete = (id / 6) * REGION_COMPLETE_HEIGHT;
        int xResume = (id % 6) * REGION_RESUME_WIDTH;
        int yResume = (id / 6) * REGION_RESUME_HEIGHT;

        float widthResume = LAYOUT.cardResumeAbsoluteSize()[0];
        float heightResume = widthResume * REGION_RESUME_HEIGHT / REGION_RESUME_WIDTH;
        sizeResume = new float[]{widthResume, heightResume};

        float widthComplete = LAYOUT.cardCompleteAbsoluteSize()[0];
        float heightComplete = widthComplete * REGION_COMPLETE_HEIGHT / REGION_COMPLETE_WIDTH;
        sizeComplete = new float[]{widthComplete, heightComplete};

        region = new TextureRegion(texture, xComplete, yComplete, REGION_COMPLETE_WIDTH, REGION_COMPLETE_HEIGHT);
        if (((Card)entity).getEra() == Era.THIRD) {
            if (entity instanceof CardPurple) {
                back = new TextureRegion(texture, REGION_COMPLETE_WIDTH * 4, REGION_COMPLETE_HEIGHT * 4, REGION_COMPLETE_WIDTH, REGION_COMPLETE_HEIGHT);
            } else {
                back = new TextureRegion(texture, REGION_COMPLETE_WIDTH * 3, REGION_COMPLETE_HEIGHT * 4, REGION_COMPLETE_WIDTH, REGION_COMPLETE_HEIGHT);
            }
        } else {
            back = new TextureRegion(texture, REGION_COMPLETE_WIDTH * 5, REGION_COMPLETE_HEIGHT * 3, REGION_COMPLETE_WIDTH, REGION_COMPLETE_HEIGHT);
        }
        resume = new TextureRegion(resumeTexture, xResume, yResume, REGION_RESUME_WIDTH, REGION_RESUME_HEIGHT);
    }

    //<editor-fold desc="Overrides">
    @Override
    protected void updateSizeWithoutZoom() {
        switch ((CardState)entity.peekState()) {
            case ON_FIGURE: case WONDERED_BY_DOWN: case WONDERED_BY_UP:
                setSize(sizeComplete[0], sizeComplete[1]);
                break;
            case PLAYED_BY_DOWN: case PLAYED_BY_UP:
                setSize(sizeResume[0], sizeResume[1]);
                break;
            case OUT: case SELLED:
                setSize(0, 0);
                break;
        }
    }
    @Override
    public void draw(Batch batch, float parentAlpha) {
        switch ((CardState)entity.peekState()) {
            case OUT: case SELLED:
                break;
            case ON_FIGURE:
                if (((Card)entity).isFaceUp()) {
                    batch.draw(region, getX(), getY(), getWidth(), getHeight());
                } else {
                    batch.draw(back, getX(), getY(), getWidth(), getHeight());
                }
                break;
            case PLAYED_BY_DOWN: case PLAYED_BY_UP:
                batch.draw(resume, getX(), getY(), getWidth(), getHeight());
                break;
            case WONDERED_BY_DOWN: case WONDERED_BY_UP:
                batch.draw(back, getX(), getY(), getWidth(), getHeight());
                break;
        }
    }
    @Override
    public float[] calcPosition() throws UnknownOrInvalidPosition {
        CardPosition position = (CardPosition) entity.peekPosition();
        switch ((CardState)entity.peekState()) {
            case ON_FIGURE:
                return LAYOUT.figure(gameApplication.getGameState().getCurrentEra(), position.getIndex());
            case PLAYED_BY_DOWN:
                return LAYOUT.playedCard(true, position.getIndex());
            case PLAYED_BY_UP:
                return LAYOUT.playedCard(false, position.getIndex());
            case WONDERED_BY_DOWN:
                return LAYOUT.wonderedCard(true, position.getIndex());
            case WONDERED_BY_UP:
                return LAYOUT.wonderedCard(false, position.getIndex());
            default:
                return new float[]{-1 * getWidth(), -1 * getHeight()};
        }
    }
    //</editor-fold>
}
