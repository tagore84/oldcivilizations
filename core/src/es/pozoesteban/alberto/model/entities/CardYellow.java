package es.pozoesteban.alberto.model.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import es.pozoesteban.alberto.model.enums.CardColor;
import es.pozoesteban.alberto.model.enums.Era;
import es.pozoesteban.alberto.model.enums.PointsOrMoneyByType;
import es.pozoesteban.alberto.model.enums.Resource;
import es.pozoesteban.alberto.model.interfaces.ResourceProvider;
import es.pozoesteban.alberto.screens.BoardScreenPosition;

public class CardYellow extends Card implements ResourceProvider {

    private static final long serialVersionUID = 1L;

    private final List<Resource> specialPrice;
    private final Resource providerType;
    private final int providerAmount;
    private final int money;
    private final PointsOrMoneyByType moneyByType;
    private final int moneyByAmount;

    public CardYellow(int id, String name, Era era, HashMap<Resource, Integer> cost, String chaining, int points, Resource providerType, int amount, List<Resource> specialPrice, int money, PointsOrMoneyByType moneyByType, int moneyByAmount) {
        super(id, name, era, cost, chaining, points);
        this.specialPrice = specialPrice;
        this.providerType = providerType;
        this.providerAmount = amount;
        this.money = money;
        this.moneyByType = moneyByType;
        this.moneyByAmount = moneyByAmount;
    }
    public CardYellow(int id, String name, Era era, HashMap<Resource, Integer> cost, String chaining, int points, Resource providerType, int amount, List<Resource> specialPrice, int money) {
        this(id, name, era, cost, chaining, points, providerType, amount, specialPrice, money, PointsOrMoneyByType.NOTHING, 0);
    }
    public List<Resource> getSpecialPrice() {
        return this.specialPrice;
    }
    @Override
    public Card clone() {
        List<Resource> clonedSpecialPrice = new ArrayList();
        clonedSpecialPrice.addAll(specialPrice);
        return new CardYellow(id, getName().toString(), era, clonedCost(), chaining.toString(), points, providerType, providerAmount, clonedSpecialPrice, money);
    }
    public int getMoney() {
        return money;
    }

    @Override
    protected CardColor color() {
        return CardColor.YELLOW;
    }

    @Override
    public Resource getType() {
        return providerType;
    }

    @Override
    public int getAmount() {
        return providerAmount;
    }

    public PointsOrMoneyByType getMoneyByType() {
        return moneyByType;
    }
    public int getMoneyByAmount() {
        return moneyByAmount;
    }

}