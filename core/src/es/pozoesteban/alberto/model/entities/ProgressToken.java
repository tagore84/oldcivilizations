package es.pozoesteban.alberto.model.entities;

import es.pozoesteban.alberto.model.enums.*;

public class ProgressToken extends Entity {

    private static final long serialVersionUID = 1L;


    private int money;
    private Resource resourceType;
    private int resourceAmount;
    private boolean economy;
    private boolean bonnusShield;
    private int bonnusChaining;
    private ScientificSymbol scientificSymbol;
    private PointsOrMoneyByType pointsByType;
    private int pointsByAmount;
    private boolean wonderPlayAgain;


    public ProgressToken(int id, String name, int points, int money,
                         Resource resourceType, int resourceAmount, boolean economy,
                         boolean bonnusShield, int bonnusChaining, ScientificSymbol scientificSymbol,
                         PointsOrMoneyByType pointsByType, int pointsByAmount, boolean wonderPlayAgain) {
        super(id, name, points);
        this.money = money;
        this.resourceType = resourceType;
        this.resourceAmount = resourceAmount;
        this.economy = economy;
        this.bonnusShield = bonnusShield;
        this.bonnusChaining = bonnusChaining;
        this.scientificSymbol = scientificSymbol;
        this.pointsByType = pointsByType;
        this.pointsByAmount = pointsByAmount;
        this.wonderPlayAgain = wonderPlayAgain;
        this.pushState(ProgressTokenState.OUT);
    }

    @Override
    public ProgressToken clone() throws CloneNotSupportedException {
        ProgressToken clone = (ProgressToken)super.clone();
        clone.money = money;
        clone.resourceType = resourceType;
        clone.resourceAmount = resourceAmount;
        clone.economy = economy;
        clone.bonnusShield = bonnusShield;
        clone.bonnusChaining = bonnusChaining;
        clone.scientificSymbol = scientificSymbol;
        clone.pointsByType = pointsByType;
        clone.pointsByAmount = pointsByAmount;
        return clone;
    }

    @Override
    public boolean isSelectable() {
        switch ((ProgressTokenState)peekState()) {
            case SELECTABLE:
                return true;
            default:
                return false;
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Getters & Setters">
    public int getPoints() {
        return points;
    }
    public int getMoney() {
        return money;
    }
    public Resource getResourceType() {
        return resourceType;
    }
    public int getResourceAmount() {
        return resourceAmount;
    }
    public boolean isEconomy() {
        return economy;
    }
    public boolean isBonnusShield() {
        return bonnusShield;
    }
    public int getBonnusChaining() {
        return bonnusChaining;
    }
    public ScientificSymbol getScientificSymbol() {
        return scientificSymbol;
    }
    public PointsOrMoneyByType getPointsByType() {
        return pointsByType;
    }
    public int getPointsByAmount() {
        return pointsByAmount;
    }
    public boolean isWonderPlayAgain() {
        return wonderPlayAgain;
    }
    //</editor-fold>

    //<editor-fold desc="Comparable interface">
    @Override
    public int compareTo(Entity e) {
        if (e instanceof ProgressToken) {
            return id > e.getId() ? 1 : -1;
        } else if (e instanceof Wonder) return 1;
        else return 1;
    }
    //</editor-fold>

}
