package es.pozoesteban.alberto.model.entities;

import java.util.HashMap;
import java.util.Map;

import es.pozoesteban.alberto.model.enums.Resource;
import es.pozoesteban.alberto.model.enums.WonderPosition;
import es.pozoesteban.alberto.model.enums.WonderState;
import es.pozoesteban.alberto.model.interfaces.Playable;
import es.pozoesteban.alberto.model.interfaces.ResourceProvider;
import es.pozoesteban.alberto.model.interfaces.War;

public class Wonder extends Entity implements Playable, ResourceProvider, War {

    private static final long serialVersionUID = 1L;

    private HashMap<Resource, Integer> cost;
    private boolean isBuilt;
    private Resource resourceProviderType;
    private int resourceProviderAmount;
    private int shields;
    private int money;
    private int debt;
    private boolean playAgain; //no final beacuse: "Theology"
    private boolean destroyBrownCard;
    private boolean destroyGreyCard;
    private boolean getProgressToken;
    private boolean getFromGraveyard;
    private boolean getLastRowCard;


    public Wonder(int id, String name, HashMap<Resource, Integer> cost, int points,
                  Resource resourceProviderType, int resourceProviderAmount,
                  int shields, int money, int debt, boolean playAgain,
                  boolean destroyBrownCard, boolean destroyGreyCard,
                  boolean getProgressToken, boolean getFromGraveyard, boolean getLastRowCard) {
        super(id, name, points);
        this.cost = cost;
        this.resourceProviderType = resourceProviderType;
        this.resourceProviderAmount = resourceProviderAmount;
        this.shields = shields;
        this.money = money;
        this.debt = debt;
        this.playAgain = playAgain;
        this.destroyBrownCard = destroyBrownCard;
        this.destroyGreyCard = destroyGreyCard;
        this.getProgressToken = getProgressToken;
        this.getFromGraveyard = getFromGraveyard;
        this.getLastRowCard = getLastRowCard;
        this.pushState(WonderState.OUT);
    }

    @Override
    public Wonder clone() throws CloneNotSupportedException {
        Wonder clone = (Wonder)super.clone();
        clone.cost = new HashMap();
        for (Map.Entry<Resource, Integer> c : cost.entrySet()) {
            clone.cost.put(c.getKey(), new Integer(c.getValue()));
        }
        clone.isBuilt = isBuilt;
        clone.resourceProviderType = resourceProviderType;
        clone.resourceProviderAmount = resourceProviderAmount;
        clone.shields = shields;
        clone.money = money;
        clone.debt = debt;
        clone.playAgain = playAgain;
        clone.destroyBrownCard = destroyBrownCard;
        clone.destroyGreyCard = destroyGreyCard;
        clone.getProgressToken = getProgressToken;
        clone.getFromGraveyard = getFromGraveyard;
        clone.getLastRowCard = getLastRowCard;
        return clone;
    }

    //<editor-fold desc="Playable interface">
    @Override
    public HashMap<Resource, Integer> getCost() {
        return cost;
    }
    public String getChainingPre() {
        return "";
    }
    //</editor-fold>

    //<editor-fold desc="ResourceProvider interface">
    @Override
    public Resource getType() {
        return resourceProviderType;
    }
    @Override
    public int getAmount() {
        return resourceProviderAmount;
    }
    //</editor-fold>

    //<editor-fold desc="War interface">
    @Override
    public int getShields() {
        return shields;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Getters & Setters">
    @Override
    public boolean isSelectable() {
        switch ((WonderState)peekState()) {
            case SELECTABLE:
                return true;
            default:
                return false;
        }
    }
    public boolean isBuilt(){
        return isBuilt;
    }
    public void isBuilt(boolean isBuilt) {
        this.isBuilt = isBuilt;
    }
    public boolean isPlayAgain() {
        return playAgain;
    }
    public void isPlayAgain(boolean b) {
        playAgain = b;
    }
    public boolean isDestroyBrownCard() {
        return destroyBrownCard;
    }
    public boolean isDestroyGreyCard() {
        return destroyGreyCard;
    }
    public boolean isGetProgressToken() {
        return getProgressToken;
    }
    public boolean isGetFromGraveyard() {
        return getFromGraveyard;
    }
    public int getMoney() {
        return money;
    }
    //</editor-fold>

    //<editor-fold desc="Comparable interface">
    @Override
    public int compareTo(Entity e) {
        if (e instanceof Wonder) {
            return id > e.getId() ? 1 : -1;
        } else if (e instanceof ProgressToken) return -1;
        else return 1;
    }

    public boolean isGetLastRowCard() {
        return getLastRowCard;
    }


    //</editor-fold>
}

