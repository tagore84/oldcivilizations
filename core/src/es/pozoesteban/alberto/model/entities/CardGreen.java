package es.pozoesteban.alberto.model.entities;

import java.util.HashMap;
import java.util.List;

import es.pozoesteban.alberto.model.enums.CardColor;
import es.pozoesteban.alberto.model.enums.Era;
import es.pozoesteban.alberto.model.enums.Resource;
import es.pozoesteban.alberto.model.enums.ScientificSymbol;
import es.pozoesteban.alberto.screens.BoardScreenPosition;

public class CardGreen extends Card {

    private static final long serialVersionUID = 1L;

    private ScientificSymbol scientificSymbol;

    public CardGreen(int id, String name, Era era, HashMap<Resource, Integer> cost, String chaining, int points, ScientificSymbol scientificSymbol) {
        super(id, name, era, cost, chaining, points);
        this.scientificSymbol = scientificSymbol;
    }
    @Override
    public CardGreen clone() throws CloneNotSupportedException {
        CardGreen clone = (CardGreen)super.clone();
        clone.scientificSymbol = scientificSymbol;
        return clone;
    }
    @Override
    protected CardColor color() { return CardColor.GREEN;
    }

    public ScientificSymbol getScientificSymbol() {
        return scientificSymbol;
    }
}
