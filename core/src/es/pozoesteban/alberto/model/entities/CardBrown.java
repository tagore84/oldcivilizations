package es.pozoesteban.alberto.model.entities;


import java.util.HashMap;
import java.util.List;

import es.pozoesteban.alberto.model.enums.CardColor;
import es.pozoesteban.alberto.model.enums.Era;
import es.pozoesteban.alberto.model.enums.Resource;
import es.pozoesteban.alberto.model.interfaces.ResourceProvider;
import es.pozoesteban.alberto.screens.BoardScreenPosition;

public class CardBrown extends Card implements ResourceProvider {

    private static final long serialVersionUID = 1L;

    private Resource resourceType;
    private int resourceAmount;

    public CardBrown(int id, String name, Era era, HashMap<Resource, Integer> cost, Resource type, int amount) {
        super(id, name, era, cost, "", 0);
        this.resourceType = type;
        this.resourceAmount = amount;
    }
    @Override
    public CardBrown clone() throws CloneNotSupportedException {
        CardBrown clone = (CardBrown)super.clone();
        clone.resourceType = resourceType;
        clone.resourceAmount = resourceAmount;
        return clone;
    }
    @Override
    protected CardColor color() {
        return CardColor.BROWN;
    }

    @Override
    public Resource getType() {
        return resourceType;
    }

    @Override
    public int getAmount() {
        return resourceAmount;
    }
}
