package es.pozoesteban.alberto.model.entities;

import com.badlogic.gdx.utils.Logger;

import java.util.HashMap;
import java.util.Map;

import es.pozoesteban.alberto.model.enums.*;
import es.pozoesteban.alberto.model.exceptions.UnknownOrInvalidPosition;
import es.pozoesteban.alberto.model.interfaces.Playable;
import es.pozoesteban.alberto.model.interfaces.ResourceProvider;
import es.pozoesteban.alberto.model.interfaces.Sellable;
import es.pozoesteban.alberto.screens.BoardScreenPosition;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;

public abstract class Card extends Entity implements Playable, Sellable, Comparable<Entity> {

    private final static Logger LOG = new Logger("Card", LOG_LEVEL);

    protected HashMap<Resource, Integer> cost;
    protected String chaining;
    protected boolean faceUp;
    protected Era era;

    public Card (int id, String name, Era era, HashMap<Resource, Integer> cost, String chaining, int points) {
        super(id, name, points);
        this.cost = cost;
        this.chaining = chaining;
        this.faceUp = false;
        this.era = era;
        this.pushState(CardState.OUT);
    }

    protected abstract CardColor color();

    public void builtWonder(BoardScreenPosition wonderPos) throws UnknownOrInvalidPosition {
        switch ((WonderPosition)wonderPos) {
            case PLAYER_DOWN_0:
                pushPosition(CardPosition.WONDER_DOWN_0);
            case PLAYER_DOWN_1:
                pushPosition(CardPosition.WONDER_DOWN_1);
            case PLAYER_DOWN_2:
                pushPosition(CardPosition.WONDER_DOWN_2);
            case PLAYER_DOWN_3:
                pushPosition(CardPosition.WONDER_DOWN_3);
            case PLAYER_UP_0:
                pushPosition(CardPosition.WONDER_UP_0);
            case PLAYER_UP_1:
                pushPosition(CardPosition.WONDER_UP_1);
            case PLAYER_UP_2:
                pushPosition(CardPosition.WONDER_UP_2);
            case PLAYER_UP_3:
                pushPosition(CardPosition.WONDER_UP_3);
        }
    }

    //<editor-fold desc="Getters & Setters">
    public int getIndex() {
        int offset = 100;
        if (era == Era.SECOND) offset = 200;
        if (era == Era.THIRD) offset = 300;
        return id - offset;
    }
    public Era getEra() {
        return era;
    }
    public CardColor getColor(){
        return color();
    }
    public boolean isFaceUp() {
        return faceUp;
    }
    public void faceDown() {
        this.faceUp = false;
    }
    public void faceUp() {
        this.faceUp = true;
    }
    //</editor-fold>

    //<editor-fold desc="Playable interface">
    @Override
    public HashMap<Resource, Integer> getCost() {
        return this.cost;
    }
    public String getChainingPre() {
        return chaining;
    }
    //</editor-fold>

    //<editor-fold desc="Comparable interface">
    @Override
    public int compareTo(Entity e) {
        if (e instanceof Card) {
            return getColor().compareTo(((Card) e).getColor());
        } else return -1;
    }
    //</editor-fold>

    //<editor-fold desc="Overrides">
    @Override
    public Card clone() throws CloneNotSupportedException {
        Card clone = (Card)super.clone();
        clone.cost = new HashMap();
        for (Map.Entry<Resource, Integer> c : cost.entrySet()) {
            clone.cost.put(c.getKey(), new Integer(c.getValue()));
        }
        clone.faceUp = faceUp;
        clone.era = era;
        clone.chaining = chaining != null ? new String(chaining) : null;
        return clone;
    }
    @Override
    public boolean isSelectable() {
        if (!faceUp) return false;
        switch ((CardState)peekState()) {
            case ON_FIGURE: case SELECTABLE:
                return true;
            default:
                return false;
        }
    }
    protected HashMap<Resource, Integer> clonedCost() {
        HashMap<Resource, Integer> clonedCost = new HashMap();
        for (Map.Entry<Resource, Integer> entry : cost.entrySet()) {
            clonedCost.put(entry.getKey(), entry.getValue().intValue());
        }
        return clonedCost;
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CARD_").append(color()).append(": ").append(getName()).append(" ").append(era).append(" ERA ");
        sb.append("[");
        if (getCost().isEmpty()) {
            sb.append("free");
        } else {
            for (Map.Entry<Resource, Integer> entry : getCost().entrySet()) {
                sb.append(entry.getKey()).append(": ").append(entry.getValue()).append(" ");
            }
        }
        sb.append("] ");
        if (this instanceof ResourceProvider && ((ResourceProvider)this).getType() != Resource.NO_RESOURCE) {
            sb.append("Provide ").append(((ResourceProvider)this).getAmount()).append(" of ").append(((ResourceProvider)this).getType());
        }
        return sb.toString();
    }
    //</editor-fold>
}