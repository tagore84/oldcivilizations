package es.pozoesteban.alberto.model.entities;

import es.pozoesteban.alberto.model.enums.EntityState;
import es.pozoesteban.alberto.model.exceptions.UnknownOrInvalidPosition;
import es.pozoesteban.alberto.screens.BoardScreenPosition;


import java.io.Serializable;
import java.util.Stack;

public abstract class Entity implements Comparable<Entity>, Serializable, Cloneable {

    private final String name;
    protected final int id;
    protected final int points;

    protected boolean movedByAIPlayer;
    private boolean visible;

    protected Stack<BoardScreenPosition> positionStack;
    protected Stack<EntityState> statesStack;

    public Entity(int id, String name, int points) {
        this.name = name;
        this.id = id;
        this.points = points;

        this.movedByAIPlayer = false;
        this.visible = false;
        this.positionStack = new Stack();
        this.statesStack = new Stack();
    }

    @Override
    public Entity clone() throws CloneNotSupportedException {
        Entity clone = (Entity)super.clone();
        BoardScreenPosition[] positionsArray = new BoardScreenPosition[this.positionStack.size()];
        this.positionStack.copyInto(positionsArray);
        clone.positionStack = new Stack();
        for (BoardScreenPosition p : positionsArray) {
            clone.positionStack.push(p);
        }
        EntityState[] statesArray = new EntityState[this.statesStack.size()];
        this.statesStack.copyInto(statesArray);
        clone.statesStack = new Stack();
        for (EntityState s : statesArray) {
            clone.statesStack.push(s);
        }
        return clone;
    }


    //<editor-fold desc="Getters & Setters">
    public int getIndex() {
        return id;
    }
    public void pushState(EntityState state) {
        this.statesStack.push(state);
    }
    public EntityState peekState() {
        return this.statesStack.peek();
    }
    public EntityState popState() {
        return this.statesStack.pop();
    }
    public int getPoints() {
        return points;
    }
    public String getName() {
        return name;
    }
    public int getId() {
        return id;
    }
    public boolean isMovedByAIPlayer() {
        return movedByAIPlayer;
    }
    public void isMovedByAIPlayer(boolean movedByAIPlayer) {
        this.movedByAIPlayer = movedByAIPlayer;
    }
    public boolean isVisible() {
        return visible;
    }
    public void isVisible(boolean b) {
        visible = b;
    }
    public void setVisible(boolean visible) {
        this.visible = visible;
    }
    public abstract boolean isSelectable();

    public BoardScreenPosition peekPosition() {
        return this.positionStack.peek();
    }
    public BoardScreenPosition popPosition() {
        return this.positionStack.pop();
    }
    public void pushPosition(BoardScreenPosition position) throws UnknownOrInvalidPosition {
        this.positionStack.push(position);
    }
    //</editor-fold>

    //<editor-fold desc="Overrides">
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entity entity = (Entity) o;
        return name.equals(entity.name);
    }
    @Override
    public int hashCode() {
        return name.hashCode();
    }
    @Override
    public String toString() {
        return getClass().getSimpleName() + ": " + name + " [" + id + "] " + positionStack.peek();
    }
    //</editor-fold>
}
