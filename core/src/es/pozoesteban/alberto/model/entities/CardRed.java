package es.pozoesteban.alberto.model.entities;

import java.util.HashMap;
import java.util.List;

import es.pozoesteban.alberto.model.enums.CardColor;
import es.pozoesteban.alberto.model.enums.Era;
import es.pozoesteban.alberto.model.enums.Resource;
import es.pozoesteban.alberto.model.interfaces.War;
import es.pozoesteban.alberto.screens.BoardScreenPosition;

public class CardRed extends Card implements War {

    private static final long serialVersionUID = 1L;

    private int shields;

    public CardRed(int id, String name, Era era, HashMap<Resource, Integer> cost, String chaining, int points, int shields) {
        super(id, name, era, cost, chaining, points);
        this.shields = shields;
    }
    @Override
    public CardRed clone() throws CloneNotSupportedException {
        CardRed clone = (CardRed)super.clone();
        clone.shields = shields;
        return clone;
    }
    @Override
    protected CardColor color() { return CardColor.RED;
    }

    @Override
    public int getShields() {
        return shields;
    }
}