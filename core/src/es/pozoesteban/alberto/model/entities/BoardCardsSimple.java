package es.pozoesteban.alberto.model.entities;
import com.badlogic.gdx.utils.Logger;

import es.pozoesteban.alberto.model.enums.CardPosition;
import es.pozoesteban.alberto.model.enums.Era;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;

public class BoardCardsSimple implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    private Era era;
    private Card[] cards;

    private static Logger LOG = new Logger("BoardCardsSimple", LOG_LEVEL);

    public BoardCardsSimple(List<Card> cards, Era era) {
        this.era = era;
        this.cards = new Card[20];
        for (int i = 0; i < 20; i++) {
            this.cards[i] = cards.get(i);

            LOG.debug(isFaceDown(i) ? ("FACE_DOWN " + i) : ("FACE_UP " + i));
            if (isFaceDown(i)) {
                this.cards[i].faceDown();
            } else {
                this.cards[i].faceUp();
            }
        }
    }
    public BoardCardsSimple clone() throws CloneNotSupportedException {
        BoardCardsSimple clone = (BoardCardsSimple) super.clone();
        clone.era = era;
        clone.cards = new Card[cards.length];
        for (int i = 0; i < cards.length; i++){
            clone.cards[i] = cards[i] != null ? cards[i].clone() : null;
        }
        return clone;
    }
    public boolean isEmpty() {
        for (int i = 0; i < 20; i++) {
            if (cards[i] != null) {
                return false;
            }
        }
        return true;
    }
    public Set<Card> getAllCards() {
        Set<Card> output = new HashSet();
        for (int i = 0; i < 20; i++) {
            if (cards[i] != null) {
                output.add(cards[i]);
            }
        }
        return output;
    }
    public void removeCard(Card card) {
        int index = -1;
        for (int i = 0; i < 20; i++) {
            if (card.equals(cards[i])) {
                cards[i] = null;
                index = i;
            }
        }
        if (!isFaceDown(index)) {
            int leftFatherIndex = getLeftFatherIndex(index);
            int leftBrotherIndex = getLeftBrotherIndex(index);
            if (leftFatherIndex >= 0 && leftBrotherIndex >= 0 && cards[leftBrotherIndex] == null) cards[leftFatherIndex].faceUp();

            int rightFatherIndex = getRightFatherIndex(index);
            int rightBrotherIndex = getRightBrotherIndex(index);
            if (rightFatherIndex >= 0 && rightBrotherIndex >= 0 && cards[rightBrotherIndex] == null) cards[rightFatherIndex].faceUp();
        }
    }
    public Set<Card> getFreeCards() {
        Set<Card> freeCards = new HashSet();
        for (int i = 0; i < 20; i++) {
            if (cards[i] != null &&
                    (leftChild(i) == -1 || cards[leftChild(i)] == null) &&
                    (rightChild(i) == -1 || cards[rightChild(i)] == null)) {
                freeCards.add(cards[i]);
            }
        }
        return freeCards;
    }
    public List<Card> getLastRowCard() {
        List<Card> lastRowCards = new ArrayList();
        switch (era) {
            case FIRST:
                for (int i = 18; i < 20; i++) {
                    if (cards[i] != null) lastRowCards.add(cards[i]);
                }
                break;
            case SECOND:
                for (int i = 14; i < 20; i++) {
                    if (cards[i] != null) lastRowCards.add(cards[i]);
                }
                break;
            case THIRD:
                for (int i = 18; i < 20; i++) {
                    if (cards[i] != null) lastRowCards.add(cards[i]);
                }
                break;
        }
        return lastRowCards;
    }

    public String firstRowToString() {
        StringBuilder sb = new StringBuilder();
        List<Card> rowCards = new ArrayList();
        for (Card c : getAllCards()) {
            CardPosition position = (CardPosition) c.peekPosition();
            switch (era) {
                case FIRST:
                    switch (position) {
                        case ON_BOARD_0:
                        case ON_BOARD_1:
                        case ON_BOARD_2:
                        case ON_BOARD_3:
                        case ON_BOARD_4:
                        case ON_BOARD_5:
                            rowCards.add(c);
                            break;
                    }
                    break;
                case SECOND:
                    switch (position) {
                        case ON_BOARD_0: case ON_BOARD_1:
                            rowCards.add(c);
                            break;
                    }
                    break;
                case THIRD:
                    switch (position) {
                        case ON_BOARD_0: case ON_BOARD_1:
                            rowCards.add(c);
                            break;
                    }
                    break;
            }
        }
        for (Card card : rowCards) {
            sb.append("|" + (card.isFaceUp() ? "" : "[") + card.getName() + (card.isFaceUp() ? "" : "]") + " " + card.peekState() +
                    " " + card.peekPosition() +" " + (card.isSelectable() ? "" : "UNSELECTABLE") + " " + (card.isVisible() ? "" : "INVISIBLE") +
                    "|" + System.getProperty("line.separator"));
        }
        return sb.toString();
    }
    //<editor-fold defaultstate="collapsed" desc="Internal bussines">
    private int getLeftBrotherIndex(int index) {
        switch (era) {
            case FIRST:
                if (index > 0 && index < 6) return index - 1;
                if (index > 11 && index < 15) return index - 1;
                break;
            case SECOND:
                if (index == 1) return 0;
                if (index > 5 && index < 9) return index - 1;
                break;
            case THIRD:
                if (index == 1) return 0;
                if (index > 5 && index < 9) return index - 1;
                if (index > 11 && index < 15) return index - 1;
                break;
        }
        return -1;
    }
    private int getRightBrotherIndex(int index) {
        switch (era) {
            case FIRST:
                if (index < 5) return index + 1;
                if (index > 10 && index < 14) return index + 1;
                break;
            case SECOND:
                if (index == 0) return 1;
                if (index > 4 && index < 8) return index + 1;
                break;
            case THIRD:
                if (index == 0) return 1;
                if (index > 4 && index < 8) return index + 1;
                if (index > 10 && index < 14) return index + 1;
                break;
        }
        return -1;
    }
    private int getLeftFatherIndex(int index) {
        switch (era) {
            case FIRST:
                if (index > 0 && index < 6) return index + 5;
                if (index > 11 && index < 15) return index + 3;
                break;
            case SECOND:
                if (index < 2) return index + 2;
                if (index > 4 && index < 9) return index + 4;
                break;
            case THIRD:
                if (index < 2) return index + 2;
                if (index == 6) return 9;
                if (index == 8) return 10;
                if (index > 11 && index < 15) return index + 3;
                break;
        }
        return -1;
    }
    private int leftChild(int index) {
        switch (era) {
            case FIRST:
                if (index > 5 && index < 11) return index - 6;
                if (index > 10 && index < 15) return index - 5;
                if (index > 14 && index < 18) return index - 4;
                if (index > 17) return index - 3;
                break;
            case SECOND:
                if (index > 2 && index < 5) return index - 3;
                if (index > 5 && index < 9) return index - 4;
                if (index > 9 && index < 14) return index - 5;
                if (index > 14) return index - 6;
                break;
            case THIRD:
                if (index > 2 && index < 5) return index - 3;
                if (index > 5 && index < 9) return index - 4;
                if (index == 9) return 5;
                if (index == 10) return 7;
                if (index == 12) return 9;
                if (index == 14) return 10;
                if (index > 14 && index < 18) return index - 4;
                if (index > 17) return index - 3;
                break;
        }
        return -1;
    }
    private int rightChild(int index) {
        switch (era) {
            case FIRST:
                if (index > 5 && index < 11) return index - 5;
                if (index > 10 && index < 15) return index - 4;
                if (index > 14 && index < 18) return index - 3;
                if (index > 17) return index - 2;
                break;
            case SECOND:
                if (index > 1 && index < 4) return index - 2;
                if (index > 4 && index < 8) return index - 3;
                if (index > 8 && index < 13) return index - 4;
                if (index > 13 && index < 19) return index - 5;
                break;
            case THIRD:
                if (index > 1 && index < 4) return index - 2;
                if (index > 4 && index < 8) return index - 3;
                if (index == 9) return 6;
                if (index == 10) return 8;
                if (index == 11) return 9;
                if (index == 13) return 10;
                if (index > 14 && index < 18) return index - 3;
                if (index > 17) return index - 2;
                break;
        }
        return -1;
    }
    private int getRightFatherIndex(int index) {
        switch (era) {
            case FIRST:
                if (index < 5) return index + 6;
                if (index > 10 && index < 14) return index + 4;
                break;
            case SECOND:
                if (index < 2) return index + 3;
                if (index > 4 && index < 9) return index + 5;
                break;
            case THIRD:
                if (index < 2) return index + 3;
                if (index == 5) return 9;
                if (index == 7) return 10;
                if (index > 10 && index < 14) return index + 4;
                break;
        }
        return -1;
    }
    private boolean isFaceDown(int index) {
        switch (era) {
            case FIRST:
                if (index > 5 && index < 11) return true;
                if (index > 14 && index < 18) return true;
                break;
            case SECOND:
                if (index > 1 && index < 5) return true;
                if (index > 8 && index < 14) return true;
                break;
            case THIRD:
                if (index > 1 && index < 5) return true;
                if (index > 8 && index < 11) return true;
                if (index > 14 && index < 18) return true;
                break;
        }
        return false;
    }
    //</editor-fold>
}
