package es.pozoesteban.alberto.model.entities;

import java.util.HashMap;
import java.util.List;

import es.pozoesteban.alberto.model.enums.CardColor;
import es.pozoesteban.alberto.model.enums.Era;
import es.pozoesteban.alberto.model.enums.PointsOrMoneyByType;
import es.pozoesteban.alberto.model.enums.Resource;
import es.pozoesteban.alberto.screens.BoardScreenPosition;

public class CardPurple extends Card {

    private static final long serialVersionUID = 1L;

    private PointsOrMoneyByType pointsByType;
    private int pointsByAmount;
    private PointsOrMoneyByType moneyByType;
    private int moneyByAmount;

    public CardPurple(int id, String name, Era era, HashMap<Resource, Integer> cost, PointsOrMoneyByType pointsByType, int pointsByAmount,
                      PointsOrMoneyByType moneyByType, int moneyByAmount) {
        super(id, name, era, cost, null, 0);
        this.pointsByType = pointsByType;
        this.pointsByAmount = pointsByAmount;
        this.moneyByType = moneyByType;
        this.moneyByAmount = moneyByAmount;
    }
    @Override
    public CardPurple clone() throws CloneNotSupportedException {
        CardPurple clone = (CardPurple)super.clone();
        clone.pointsByType = pointsByType;
        clone.pointsByAmount = pointsByAmount;
        clone.moneyByType = moneyByType;
        clone.moneyByAmount = moneyByAmount;
        return clone;
    }
    @Override
    protected CardColor color() { return CardColor.PURPLE;
    }

    //<editor-fold desc="Gtters & Setters">
    public PointsOrMoneyByType getPointsByType() {
        return pointsByType;
    }
    public int getPointsByAmount() {
        return pointsByAmount;
    }
    public PointsOrMoneyByType getMoneyByType() {
        return moneyByType;
    }
    public int getMoneyByAmount() {
        return moneyByAmount;
    }
    //</editor-fold>
}
