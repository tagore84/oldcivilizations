package es.pozoesteban.alberto.model.entities;

import java.util.HashMap;
import java.util.List;

import es.pozoesteban.alberto.model.enums.CardColor;
import es.pozoesteban.alberto.model.enums.CardPosition;
import es.pozoesteban.alberto.model.enums.Era;
import es.pozoesteban.alberto.model.enums.Resource;
import es.pozoesteban.alberto.screens.BoardScreenPosition;

public class CardBlue extends Card {

    private static final long serialVersionUID = 1L;

    public CardBlue(int id, String name, Era era, HashMap<Resource, Integer> cost, String chaining, int points) {
        super(id, name, era, cost, chaining, points);
    }
    @Override
    public CardBlue clone() throws CloneNotSupportedException {
        CardBlue clone = (CardBlue)super.clone();
        return clone;
    }
    @Override
    protected CardColor color() {
        return CardColor.BLUE;
    }
}
