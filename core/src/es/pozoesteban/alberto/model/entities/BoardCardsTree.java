package es.pozoesteban.alberto.model.entities;

import java.util.*;

import es.pozoesteban.alberto.model.enums.Era;

@Deprecated
public class BoardCardsTree {

    private final Node root;

    @Override
    public BoardCardsTree clone() {
        return new BoardCardsTree(root.clone());
    }
    public BoardCardsTree(Node root) {
        this.root = root;
    }

    public BoardCardsTree(List<Card> cards, Era era) {
        root = new Node(null, false);
        switch (era) {
            case FIRST:
                Node f00 = new Node(cards.get(0), true);
                Node f01 = new Node(cards.get(1), true);
                Node f02 = new Node(cards.get(2), true);
                Node f03 = new Node(cards.get(3), true);
                Node f04 = new Node(cards.get(4), true);
                Node f05 = new Node(cards.get(5), true);
                Node f10 = new Node(cards.get(6), false);
                f10.children.add(f00);
                f10.children.add(f01);
                Node f11 = new Node(cards.get(7), false);
                f11.children.add(f01);
                f11.children.add(f02);
                Node f12 = new Node(cards.get(8), false);
                f12.children.add(f02);
                f12.children.add(f03);
                Node f13 = new Node(cards.get(9), false);
                f13.children.add(f03);
                f13.children.add(f04);
                Node f14 = new Node(cards.get(10), false);
                f14.children.add(f04);
                f14.children.add(f05);
                Node f20 = new Node(cards.get(11), true);
                f20.children.add(f10);
                f20.children.add(f11);
                Node f21 = new Node(cards.get(12), true);
                f21.children.add(f11);
                f21.children.add(f12);
                Node f22 = new Node(cards.get(13), true);
                f22.children.add(f12);
                f22.children.add(f13);
                Node f23 = new Node(cards.get(14), true);
                f23.children.add(f13);
                f23.children.add(f14);
                Node f30 = new Node(cards.get(15), false);
                f30.children.add(f20);
                f30.children.add(f21);
                Node f31 = new Node(cards.get(16), false);
                f31.children.add(f21);
                f31.children.add(f22);
                Node f32 = new Node(cards.get(17), false);
                f32.children.add(f22);
                f32.children.add(f23);
                Node f40 = new Node(cards.get(18), true);
                f40.children.add(f30);
                f40.children.add(f31);
                Node f41 = new Node(cards.get(19), true);
                f41.children.add(f31);
                f41.children.add(f32);
                root.children.add(f40);
                root.children.add(f41);
                break;
            case SECOND:
                Node s00 = new Node(cards.get(0), true);
                Node s01 = new Node(cards.get(1), true);
                Node s10 = new Node(cards.get(2), false);
                s10.children.add(s00);
                Node s11 = new Node(cards.get(3), false);
                s11.children.add(s00);
                s11.children.add(s01);
                Node s12 = new Node(cards.get(4), false);
                s12.children.add(s01);
                Node s20 = new Node(cards.get(5), true);
                s20.children.add(s10);
                Node s21 = new Node(cards.get(6), true);
                s21.children.add(s10);
                s21.children.add(s11);
                Node s22 = new Node(cards.get(7), true);
                s22.children.add(s11);
                s22.children.add(s12);
                Node s23 = new Node(cards.get(8), true);
                s23.children.add(s12);
                Node s30 = new Node(cards.get(9), false);
                s30.children.add(s20);
                Node s31 = new Node(cards.get(10), false);
                s31.children.add(s20);
                s31.children.add(s21);
                Node s32 = new Node(cards.get(11), false);
                s32.children.add(s21);
                s32.children.add(s22);
                Node s33 = new Node(cards.get(12), false);
                s33.children.add(s22);
                s33.children.add(s23);
                Node s34 = new Node(cards.get(13), false);
                s34.children.add(s23);
                Node s40 = new Node(cards.get(14), true);
                s40.children.add(s30);
                Node s41 = new Node(cards.get(15), true);
                s41.children.add(s30);
                s41.children.add(s31);
                Node s42 = new Node(cards.get(16), true);
                s42.children.add(s31);
                s42.children.add(s32);
                Node s43 = new Node(cards.get(17), true);
                s43.children.add(s32);
                s43.children.add(s33);
                Node s44 = new Node(cards.get(18), true);
                s44.children.add(s33);
                s44.children.add(s34);
                Node s45 = new Node(cards.get(19), true);
                s45.children.add(s34);
                root.children.add(s40);
                root.children.add(s41);
                root.children.add(s42);
                root.children.add(s43);
                root.children.add(s44);
                root.children.add(s45);
                break;
            case THIRD:
                Node t00 = new Node(cards.get(0), true);
                Node t01 = new Node(cards.get(1), true);
                Node t10 = new Node(cards.get(2), false);
                t10.children.add(t00);
                Node t11 = new Node(cards.get(3), false);
                t11.children.add(t00);
                t11.children.add(t01);
                Node t12 = new Node(cards.get(4), false);
                t11.children.add(t01);
                Node t20 = new Node(cards.get(5), true);
                t20.children.add(t10);
                Node t21 = new Node(cards.get(6), true);
                t21.children.add(t10);
                t21.children.add(t11);
                Node t22 = new Node(cards.get(7), true);
                t22.children.add(t11);
                t22.children.add(t12);
                Node t23 = new Node(cards.get(8), true);
                t23.children.add(t12);
                Node t30 = new Node(cards.get(9), false);
                t30.children.add(t20);
                t30.children.add(t21);
                Node t31 = new Node(cards.get(10), false);
                t31.children.add(t22);
                t31.children.add(t23);
                Node t40 = new Node(cards.get(11), true);
                t40.children.add(t30);
                Node t41 = new Node(cards.get(12), true);
                t41.children.add(t30);
                Node t42 = new Node(cards.get(13), true);
                t42.children.add(t31);
                Node t43 = new Node(cards.get(14), true);
                t43.children.add(t31);
                Node t50 = new Node(cards.get(15), false);
                t50.children.add(t40);
                t50.children.add(t41);
                Node t51 = new Node(cards.get(16), false);
                t51.children.add(t41);
                t51.children.add(t42);
                Node t52 = new Node(cards.get(17), false);
                t52.children.add(t42);
                t52.children.add(t43);
                Node t60 = new Node(cards.get(18), true);
                t60.children.add(t50);
                t60.children.add(t51);
                Node t61 = new Node(cards.get(19), true);
                t61.children.add(t51);
                t61.children.add(t52);
                root.children.add(t60);
                root.children.add(t61);
                break;
        }
    }
    public boolean isEmpty() {
        return root.children.isEmpty();
    }
    public Set<Card> getFreeCards() {
        Set<Card> playableCards = new HashSet();
        for (Node node : root.children) {
            playableCards.addAll(getFreeCards(playableCards, node));
        }
        return playableCards;
    }
    public Set<Card> getAllCards() {
        Set<Card> allCards = new HashSet();
        for (Node node : root.children) {
            allCards.addAll(getAllCards(allCards, node));
        }
        return allCards;
    }
    public boolean removeCard(Card card) {
        return removeCard(root, new Node(card, false));
    }



    //<editor-fold defaultstate="collapsed" desc="Internal bussines">
    private boolean removeCard(Node whereRemove, Node whatRemove) {
        if (whereRemove.children.isEmpty()) return false;
        if (whereRemove.children.contains(whatRemove)) {
            whereRemove.children.remove(whatRemove);
            if (whereRemove.data != null && !whereRemove.data.faceUp && whereRemove.children.isEmpty()) {
                whatRemove.data.faceUp();
            }
            return true;
        }
        boolean output = false;
        for (Node node : whereRemove.children) {
            if (removeCard(node, whatRemove)) output = true;
        }
        return output;
    }
    private Set<Card> getFreeCards(Set<Card> currentList, Node node) {
        if (node.children.isEmpty()) {
            currentList.add(node.data);
        } else {
            for (Node n : node.children) {
                currentList.addAll(getFreeCards(currentList, n));
            }
        }
        return currentList;
    }
    private Set<Card> getAllCards(Set<Card> currentList, Node node) {
        Set<Card> output = new HashSet();
        output.add(node.data);
        for (Node n : node.children) {
            output.addAll(getAllCards(currentList, n));
        }
        return output;
    }

    public List<Card> getLastRowCard() {
        ArrayList<Card> output = new ArrayList();
        for (Node node : root.children) {
            output.add(node.data);
        }
        return output;
    }

    public static class Node {
        private Card data;
        private Node parent;
        private List<Node> children;
        public Node(Card card, boolean faceUp) {
            this.data = card;
            if (faceUp) {
                if (data != null) this.data.faceUp();
            } else {
                if (data != null) this.data.faceDown();
            }
            this.children = new ArrayList();
        }

        public Node(Card data, Node parent, List<Node> children) {
            this.data = data;
            this.parent = parent;
            this.children = children;
        }

        @Override
        public Node clone() {
            Card data = this.data;
            Node parent = this.parent == null ? null : this.parent.clone();
            List<Node> children = new ArrayList();
            for (Node child : this.children) {
                children.add(child.clone());
            }
            return new Node(data, parent, children);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node other = (Node) o;
            if (data == null) return other.data == null;
            return data.equals(other.data);
        }

        @Override
        public int hashCode() {
            return data.hashCode();
        }

        @Override
        public String toString() {
            return data != null ? data.toString() + " [" + children.size() + "]" : "root";
        }
    }
    //</editor-fold>
}
