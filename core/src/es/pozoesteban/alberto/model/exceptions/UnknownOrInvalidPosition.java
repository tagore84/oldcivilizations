package es.pozoesteban.alberto.model.exceptions;

import es.pozoesteban.alberto.model.entities.Entity;
import es.pozoesteban.alberto.screens.BoardScreenPosition;

public class UnknownOrInvalidPosition extends Exception{

    public UnknownOrInvalidPosition(Entity entity, BoardScreenPosition position) {
        super("Invalid or forbidden position " + entity + " -> " + position);
    }
    public UnknownOrInvalidPosition(BoardScreenPosition position) {
        super("Invalid or forbidden position " + position);
    }
}