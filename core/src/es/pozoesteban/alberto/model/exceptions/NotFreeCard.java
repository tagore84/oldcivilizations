package es.pozoesteban.alberto.model.exceptions;

import es.pozoesteban.alberto.model.entities.Card;

public class NotFreeCard extends Exception{
    public NotFreeCard(Card card) {
        super("Trying to play not free card " + card.toString());
    }
}
