package es.pozoesteban.alberto.model;

import com.badlogic.gdx.utils.Logger;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.FileReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import es.pozoesteban.alberto.model.entities.Entity;
import es.pozoesteban.alberto.model.enums.Era;
import es.pozoesteban.alberto.model.enums.CardColor;
import es.pozoesteban.alberto.model.enums.PointsOrMoneyByType;
import es.pozoesteban.alberto.model.entities.ProgressToken;
import es.pozoesteban.alberto.model.enums.Resource;
import es.pozoesteban.alberto.model.enums.ScientificSymbol;
import es.pozoesteban.alberto.model.entities.Wonder;
import es.pozoesteban.alberto.model.entities.CardBlue;
import es.pozoesteban.alberto.model.entities.CardBrown;
import es.pozoesteban.alberto.model.entities.Card;
import es.pozoesteban.alberto.model.entities.CardGreen;
import es.pozoesteban.alberto.model.entities.CardGrey;
import es.pozoesteban.alberto.model.entities.CardPurple;
import es.pozoesteban.alberto.model.entities.CardRed;
import es.pozoesteban.alberto.model.entities.CardYellow;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;


public class BoxContent implements Serializable {
    private final List<Card> firstEraCards;
    private final List<Card> secondEraCards;
    private final List<Card> thirdEraCards;
    private final List<Wonder> wonders;
    private final List<ProgressToken> progressTokens;

    private static final long serialVersionUID = 1L;

    private static Logger LOG = new Logger("BoxContent", LOG_LEVEL);
    public static BoxContent build(){
        try {
            JsonObject json = new JsonParser().parse(new FileReader("box_content.json")).getAsJsonObject();
            JsonObject entitiesJson = json.getAsJsonObject("entities");
            JsonArray wondersJson = entitiesJson.getAsJsonArray("wonders");
            List<Wonder> wonders = new ArrayList();
            for (JsonElement w : wondersJson) {
                wonders.add(parseWonder((JsonObject) w));
            }
            JsonObject cardsJson = entitiesJson.getAsJsonObject("cards");
            JsonArray firstEraCardsJson = cardsJson.getAsJsonArray("firstEra");
            List<Card> firstEraCards = new ArrayList();
            for (JsonElement c : firstEraCardsJson) {
                firstEraCards.add(parseCard((JsonObject) c, Era.FIRST));
            }
            JsonArray secondEraCardsJson = cardsJson.getAsJsonArray("secondEra");
            List<Card> secondEraCards = new ArrayList();
            for (JsonElement c : secondEraCardsJson) {
                secondEraCards.add(parseCard((JsonObject) c, Era.SECOND));
            }
            JsonArray thirdEraCardsJson = cardsJson.getAsJsonArray("thirdEra");
            List<Card> thirdEraCards = new ArrayList();
            for (JsonElement c : thirdEraCardsJson) {
                thirdEraCards.add(parseCard((JsonObject) c, Era.THIRD));
            }
            JsonArray progressTokensJson = entitiesJson.getAsJsonArray("progressTokens");
            List<ProgressToken> progressTokens = new ArrayList();
            for (JsonElement p : progressTokensJson) {
                progressTokens.add(parseProgressToken((JsonObject) p));
            }
            return new BoxContent(firstEraCards, secondEraCards, thirdEraCards,
                    wonders, progressTokens);
        } catch (Exception e) {
            LOG.error("Error loading BoxContentFile", e);
            return null;
        }
    }
    private static ProgressToken parseProgressToken(JsonObject p) {
        int id = p.get("id").getAsInt();
        String name = p.get("name").getAsString();
        int points = p.has("points") ? p.get("points").getAsInt() : 0;
        int money = p.has("money") ? p.get("money").getAsInt() : 0;
        Resource resourceType = p.has("resourceProviderType") ?
                Resource.valueOf(p.get("resourceProviderType").getAsString()) : Resource.NO_RESOURCE;
        int resourceAmount = p.has("resourceProviderAmount") ? p.get("resourceProviderAmount").getAsInt() : 0;
        boolean economy = p.has("economy") ? p.get("economy").getAsBoolean() : false;
        boolean bonnusShield = p.has("bonnusShield") ? p.get("bonnusShield").getAsBoolean() : false;
        int bonnuschaining = p.has("bonnuschaining") ? p.get("bonnuschaining").getAsInt() : 0;
        ScientificSymbol scientificSymbol = p.has("scientificSymbol") ?
                ScientificSymbol.valueOf(p.get("scientificSymbol").getAsString()) : ScientificSymbol.NO_SCIENTIFIC_SYMBOL;
        PointsOrMoneyByType pointsByType = p.has("pointsByType") ?
                PointsOrMoneyByType.valueOf(p.get("pointsByType").getAsString()) : PointsOrMoneyByType.NOTHING;;
        int pointsByAmount = p.has("pointsByAmount") ? p.get("pointsByAmount").getAsInt() : 0;;
        boolean wonderPlayAgain = p.has("wonderPlayAgain") ? p.get("wonderPlayAgain").getAsBoolean() : false;;


        return new ProgressToken(id, name, points, money, resourceType, resourceAmount,
                economy, bonnusShield, bonnuschaining, scientificSymbol,
                pointsByType, pointsByAmount, wonderPlayAgain);
    }
    private static Card parseCard(JsonObject c, Era era) {
        int id = c.get("id").getAsInt();
        String name = c.get("name").getAsString();
        CardColor color = CardColor.valueOf(c.get("color").getAsString());
        HashMap<Resource, Integer> cost = getResourceMap(c, "cost");
        String chaining = c.has("chaining") ? c.get("chaining").getAsString() : "";
        int points = c.has("points") ? c.get("points").getAsInt() : 0;
        switch (color) {
            case BLUE:
                return new CardBlue(id, name, era, cost, chaining, points);
            case RED:
                return parseRedCard(id, name, era, cost, chaining, points, c);
            case YELLOW:
                return parseYellowCard(id, name, era, cost, chaining, points, c);
            case BROWN:
                return parseBrownCard(id, name, era, cost, chaining, points, c);
            case GREEN:
                return parseGreenCard(id, name, era, cost, chaining, points, c);
            case GREY:
                return parseGreyCard(id, name, era, cost, chaining, points, c);
            case PURPLE:
                return parsePurpleCard(id, name, era, cost, chaining, points, c);

        }
        LOG.error("Error parsing card " + c.toString());
        return null;
    }
    private static CardPurple parsePurpleCard(int id, String name, Era era, HashMap<Resource,Integer> cost, String chaining, int points, JsonObject c) {
        PointsOrMoneyByType pointsByType = PointsOrMoneyByType.valueOf(c.get("pointsByType").getAsString());
        int pointsByAmount = c.get("pointsByAmount").getAsInt();
        PointsOrMoneyByType moneyByType = c.has("moneyByType") ? PointsOrMoneyByType.valueOf(c.get("moneyByType").getAsString()) : PointsOrMoneyByType.NOTHING;
        int moneyByAmount = c.has("moneyByAmount") ? c.get("moneyByAmount").getAsInt() : 0;
        return new CardPurple(id, name, era, cost, pointsByType, pointsByAmount, moneyByType, moneyByAmount);
    }
    private static CardGreen parseGreenCard(int id, String name, Era era, HashMap<Resource,Integer> cost, String chaining, int points, JsonObject c) {
        ScientificSymbol scientificSymbol = c.has("scientificSymbol") ?
                ScientificSymbol.valueOf(c.get("scientificSymbol").getAsString()) : ScientificSymbol.NO_SCIENTIFIC_SYMBOL;
        return new CardGreen(id, name, era, cost, chaining, points, scientificSymbol);
    }
    private static CardBrown parseBrownCard(int id, String name, Era era, HashMap<Resource,Integer> cost, String chaining, int points, JsonObject c) {
        Resource resourceType = c.has("resourceType") ?
                Resource.valueOf(c.get("resourceType").getAsString()) : Resource.NO_RESOURCE;
        int resourceAmount = c.has("resourceAmount") ? c.get("resourceAmount").getAsInt() : 0;

        return new CardBrown(id, name, era, cost, resourceType, resourceAmount);
    }
    private static CardGrey parseGreyCard(int id, String name, Era era, HashMap<Resource,Integer> cost, String chaining, int points, JsonObject c) {
        Resource resourceType = c.has("resourceType") ?
                Resource.valueOf(c.get("resourceType").getAsString()) : Resource.NO_RESOURCE;
        int resourceAmount = c.has("resourceAmount") ? c.get("resourceAmount").getAsInt() : 0;

        return new CardGrey(id, name, era, cost, resourceType, resourceAmount);
    }
    private static CardYellow parseYellowCard(int id, String name, Era era, HashMap<Resource,Integer> cost, String chaining, int points, JsonObject c) {
        Resource resourceType = c.has("resourceType") ?
                Resource.valueOf(c.get("resourceType").getAsString()) : Resource.NO_RESOURCE;
        int resourceAmount = c.has("resourceAmount") ? c.get("resourceAmount").getAsInt() : 0;
        List<Resource> specialPrice = new ArrayList();
        if (c.has("specialPrice")) {
            for (JsonElement element : c.get("specialPrice").getAsJsonArray()) {
                specialPrice.add(Resource.valueOf(element.getAsString()));
            }
        }
        int money = c.has("money") ? c.get("money").getAsInt() : 0;
        return new CardYellow(id, name, era, cost, chaining, points, resourceType, resourceAmount, specialPrice, money);
    }
    private static CardRed parseRedCard(int id, String name, Era era, HashMap<Resource, Integer> cost, String chaining, int points, JsonObject c) {
        int shields = c.has("shields") ? c.get("shields").getAsInt() : 0;
        return new CardRed(id, name, era, cost, chaining, points, shields);
    }
    private static HashMap<Resource, Integer> getResourceMap(JsonObject entity, String key) {
        HashMap<Resource, Integer> cost = new HashMap();
        if (entity.has(key)) {
            for (JsonElement c : entity.get(key).getAsJsonArray()) {
                JsonObject object = (JsonObject) c;
                for (Resource r : Resource.values()) {
                    if (object.has(r.name())) cost.put(r, object.get(r.name()).getAsInt());
                }
            }
        }
        return cost;
    }
    private static Wonder parseWonder(JsonObject w) {
        int id = w.get("id").getAsInt();
        String name = w.get("name").getAsString();
        HashMap<Resource, Integer> cost = getResourceMap(w, "cost");
        int points = w.has("points") ? w.get("points").getAsInt() : 0;
        Resource resourceType = w.has("resourceProviderType") ?
                Resource.valueOf(w.get("resourceProviderType").getAsString()) : Resource.NO_RESOURCE;
        int resourceAmount = w.has("resourceProviderAmount") ? w.get("resourceProviderAmount").getAsInt() : 0;
        int shields = w.has("shields") ? w.get("shields").getAsInt() : 0;
        int money = w.has("money") ? w.get("money").getAsInt() : 0;
        int debt = w.has("debt") ? w.get("debt").getAsInt() : 0;
        boolean playAgain = w.has("playAgain") ? w.get("playAgain").getAsBoolean() : false;
        boolean destroyBrownCard = w.has("destroyBrownCard") ? w.get("destroyBrownCard").getAsBoolean() : false;
        boolean destroyGreyCard = w.has("destroyGreyCard") ? w.get("destroyGreyCard").getAsBoolean() : false;
        boolean getProgressToken = w.has("getProgressToken") ? w.get("getProgressToken").getAsBoolean() : false;
        boolean getFromGraveyard = w.has("getFromGraveyard") ? w.get("getFromGraveyard").getAsBoolean() : false;
        boolean lastRowBonnus = w.has("lastRowBonnus") ? w.get("lastRowBonnus").getAsBoolean() : false;
        return new Wonder(id, name, cost, points, resourceType, resourceAmount, shields, money,
                debt, playAgain, destroyBrownCard, destroyGreyCard, getProgressToken, getFromGraveyard, lastRowBonnus);
    }
    private BoxContent(List<Card> firstEraCards, List<Card> secondEraCards, List<Card> thirdEraCards,
                       List<Wonder> wonders, List<ProgressToken> progressTokens) {
        this.firstEraCards = firstEraCards;
        this.secondEraCards = secondEraCards;
        this.thirdEraCards = thirdEraCards;
        this.wonders = wonders;
        this.progressTokens = progressTokens;
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BoxContent:").append("\n");
        sb.append("First Era Cards:").append("\n");
        for (Card card : firstEraCards) {
            sb.append("\t").append(card).append("\n");
        }
        sb.append("Second Era Cards:").append("\n");
        for (Entity entity : secondEraCards) {
            sb.append("\t").append(entity).append("\n");
        }
        sb.append("Third Era Cards:").append("\n");
        for (Entity entity : thirdEraCards) {
            sb.append("\t").append(entity).append("\n");
        }
        sb.append("Wonders:").append("\n");
        for (Entity entity : wonders) {
            sb.append("\t").append(entity).append("\n");
        }
        sb.append("ProgressTokens:").append("\n");
        for (Entity entity : progressTokens) {
            sb.append("\t").append(entity).append("\n");
        }
        return sb.toString();
    }
    public List<Card> getFirstEraCards() {
        return firstEraCards;
    }
    public List<Card> getSecondEraCards() {
        return secondEraCards;
    }
    public List<Card> getThirdEraCards(boolean purples) {
        List<Card> output = new ArrayList();
        for (Card card : thirdEraCards) {
            if ((card.getColor() == CardColor.PURPLE) == purples) {
                output.add(card);
            }
        }
        return output;
    }
    public List<Wonder> getWonders() {
        return wonders;
    }
    public List<ProgressToken> getProgressTokens() {
        return progressTokens;
    }

    public Entity getEntityByName(String name) {
        for (Wonder wonder : wonders) {
            if (wonder.getName().equalsIgnoreCase(name)) return wonder;
        }
        for (Card card : firstEraCards) {
            if (card.getName().equalsIgnoreCase(name)) return card;
        }
        for (Card card : secondEraCards) {
            if (card.getName().equalsIgnoreCase(name)) return card;
        }
        for (Card card : thirdEraCards) {
            if (card.getName().equalsIgnoreCase(name)) return card;
        }
        for (ProgressToken token : progressTokens) {
            if (token.getName().equalsIgnoreCase(name)) return token;
        }
        throw new IllegalArgumentException("Entity not found in box: " + name);
    }
}
