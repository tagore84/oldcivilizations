package es.pozoesteban.alberto.model.enums;

import java.io.Serializable;

public enum PointsOrMoneyByType implements Serializable {

    NOTHING(0), BROWN(1), GREY(2), YELLOW(3), RED(4), WONDERS(5), PROGRESS_TOKENS(6),
    MAX_YELLOW(7), MAX_BROWN_AND_GREY(8), MAX_WONDERS(9), MAX_BLUE(10), MAX_GREEN(11), MAX_MONEY(12), MAX_RED(13);

    private static final long serialVersionUID = 1L;

    private final int value;

    PointsOrMoneyByType(int value) {
        this.value = value;
    }
}
