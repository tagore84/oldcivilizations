package es.pozoesteban.alberto.model.enums;

import java.io.Serializable;

public enum WinType implements Serializable {
    BY_POINTS(0),
    MILITARY(1),
    SCIENTIFIC(2);

    private final int value;

    WinType(int value) {
        this.value = value;
    }

    public int getValue(){
        return value;
    }

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        switch (this) {
            case BY_POINTS:
                return "by points";
            case MILITARY:
                return "military";
            case SCIENTIFIC:
                return "scientific";
        }
        return "UNKNOWN WIN_TYPE";
    }
}
