package es.pozoesteban.alberto.model.enums;

import java.io.Serializable;

public enum WonderState implements EntityState, Serializable {
    OUT(-1), SELECTABLE(0), SELECTED_BY_DOWN(1), SELECTED_BY_UP(2), PLAYED_BY_DOWN(3), PLAYED_BY_UP(4);

    private final int value;

    WonderState(int value) {
        this.value = value;
    }

    private static final long serialVersionUID = 1L;

}
