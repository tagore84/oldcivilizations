package es.pozoesteban.alberto.model.enums;

import java.io.Serializable;

public enum Action implements Serializable {
    INIT_SELECT_WONDER(0),
    SELECT_PROGRESS_TOKEN(1),
    BUILD_CARD(2),
    BUILD_WONDER(3),
    SELL(4),
    DESTROY_CARD(5),
    BUILT_CARD_FOR_FREE(6);

    private final int value;

    private static final long serialVersionUID = 1L;

    Action(int value) {
        this.value = value;
    }

    public int getValue(){
        return value;
    }

    @Override
    public String toString() {
        switch (this) {
            case INIT_SELECT_WONDER: case SELECT_PROGRESS_TOKEN:
                return "SELECT";
            case BUILD_CARD:
                return "BUILD";
            case BUILD_WONDER:
                return "BUILD WONDER WITH";
            case SELL:
                return "SELL";
            case BUILT_CARD_FOR_FREE:
                return "BUILD FOR FREE";
            case DESTROY_CARD:
                return "DESTROY CARD";
        }
        return "UNKNOWN ACTION";
    }


}

