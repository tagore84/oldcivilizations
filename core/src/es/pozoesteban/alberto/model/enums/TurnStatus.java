package es.pozoesteban.alberto.model.enums;

import java.io.Serializable;

public enum TurnStatus implements Serializable {

    INIT_CHOOSING_WONDER_1_8(0), INIT_CHOOSING_WONDER_2_8(1), INIT_CHOOSING_WONDER_3_8(2), INIT_CHOOSING_WONDER_4_8(3),
    INIT_CHOOSING_WONDER_5_8(4), INIT_CHOOSING_WONDER_6_8(5), INIT_CHOOSING_WONDER_7_8(6), INIT_CHOOSING_WONDER_8_8(7),
    NORMAL_TURN(8),
    CHOOSING_PROGRESS_TOKEN(9),
    CHOOSING_CARD_TO_DESTROY(10),
    CHOOSING_CARD_TO_GET_FOR_FREE(11);

    public int getValue(){
        return value;
    }

    private static final long serialVersionUID = 1L;

    private final int value;

    TurnStatus(int value) {
        this.value = value;
    }

    public static TurnStatus parse(int value)
    {
        for (TurnStatus position : TurnStatus.values()) {
            if (position.value == value) return position;
        }
        throw new IllegalArgumentException("Unknown TurnStatus for int " + value);
    }
}
