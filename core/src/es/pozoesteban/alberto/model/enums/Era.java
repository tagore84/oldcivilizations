package es.pozoesteban.alberto.model.enums;

import java.io.Serializable;

public enum Era implements Serializable {
    WONDER_SELECTION(0), FIRST(1), SECOND(2), THIRD(3), FINISH(4);

    private final int value;

    Era(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static Era parse(int value)
    {
        for (Era era : Era.values()) {
            if (era.value == value) return era;
        }
        throw new IllegalArgumentException("Unknown Era for int " + value);
    }

    private static final long serialVersionUID = 1L;
}