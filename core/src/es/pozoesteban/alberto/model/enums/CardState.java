package es.pozoesteban.alberto.model.enums;

import java.io.Serializable;

public enum CardState implements EntityState, Serializable {

    OUT(-1),
    ON_FIGURE(0),
    PLAYED_BY_DOWN(1), PLAYED_BY_UP(2), WONDERED_BY_DOWN(3), WONDERED_BY_UP(4),
    SELLED(5),
    SELECTABLE(6);

    private final int value;

    CardState(int value) {
        this.value = value;
    }

    private static final long serialVersionUID = 1L;
}
