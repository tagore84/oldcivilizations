package es.pozoesteban.alberto.model.enums;

import es.pozoesteban.alberto.model.exceptions.UnknownOrInvalidPosition;
import es.pozoesteban.alberto.screens.BoardScreenPosition;

import java.io.Serializable;

public enum ProgressTokenPosition implements BoardScreenPosition, Serializable {
    INITIAL(-1),
    ON_BOARD_0(0), ON_BOARD_1(1), ON_BOARD_2(2), ON_BOARD_3(3), ON_BOARD_4(4),
    PLAYED_DOWN_0(5), PLAYED_DOWN_1(6), PLAYED_DOWN_2(7), PLAYED_DOWN_3(8), PLAYED_DOWN_4(9),
    PLAYED_UP_0(10), PLAYED_UP_1(11), PLAYED_UP_2(12), PLAYED_UP_3(13), PLAYED_UP_4(14),
    SELECTABLE_0(15), SELECTABLE_1(16), SELECTABLE_2(17), SELECTABLE_3(18), SELECTABLE_4(19);

    private int value;

    private static final long serialVersionUID = 1L;

    @Override
    public int getIndex() throws UnknownOrInvalidPosition {
        switch (this) {
            case ON_BOARD_0: case ON_BOARD_1: case ON_BOARD_2: case ON_BOARD_3: case ON_BOARD_4:
                return value;
            case PLAYED_DOWN_0: case PLAYED_DOWN_1: case PLAYED_DOWN_2: case PLAYED_DOWN_3: case PLAYED_DOWN_4:
                return value - 5;
            case PLAYED_UP_0: case PLAYED_UP_1: case PLAYED_UP_2: case PLAYED_UP_3: case PLAYED_UP_4:
                return value - 10;
            case SELECTABLE_0: case SELECTABLE_1: case SELECTABLE_2: case SELECTABLE_3: case SELECTABLE_4:
                return value - 15;
        }
        throw new UnknownOrInvalidPosition(this);
    }

    ProgressTokenPosition(int value) {
        this.value = value;
    }

    public static ProgressTokenPosition parse(int value)
    {
        for (ProgressTokenPosition position : ProgressTokenPosition.values()) {
            if (position.value == value) return position;
        }
        throw new IllegalArgumentException("Unknown PorgressTokenPosition for int " + value);
    }

    public int getValue() {
        return value;
    }
}
