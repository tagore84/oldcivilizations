package es.pozoesteban.alberto.model.enums;

import java.io.Serializable;

public enum ScientificSymbol implements Serializable {
    NO_SCIENTIFIC_SYMBOL(0), COMPASS(1), WHEEL(2), PEN(3), MORTAR(4), SOLAR_WATCH(5), GYROSCOPE(6), BALANCE(7);

    private final int value;

    ScientificSymbol(int value) {
        this.value = value;
    }

    private static final long serialVersionUID = 1L;
}
