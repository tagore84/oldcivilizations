package es.pozoesteban.alberto.model.enums;

import java.io.Serializable;

public enum CardColor implements Serializable {
    BROWN(0), GREY(1), YELLOW(2), BLUE(3), RED(4), GREEN(5), PURPLE(6);

    private static final long serialVersionUID = 1L;

    private final int value;

    CardColor(int value) {
        this.value = value;
    }
}
