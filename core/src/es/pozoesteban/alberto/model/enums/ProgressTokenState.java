package es.pozoesteban.alberto.model.enums;

import java.io.Serializable;

public enum ProgressTokenState implements EntityState, Serializable {
    OUT(-1), ON_BOARD(0), PLAYED_BY_DOWN(1), PLAYED_BY_UP(2), THREE_EXTRA(3), SELECTABLE(4);

    private final int value;

    ProgressTokenState(int value) {
        this.value = value;
    }

    private static final long serialVersionUID = 1L;
}
