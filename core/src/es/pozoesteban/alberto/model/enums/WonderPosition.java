package es.pozoesteban.alberto.model.enums;

import es.pozoesteban.alberto.model.exceptions.UnknownOrInvalidPosition;
import es.pozoesteban.alberto.screens.BoardScreenPosition;

import java.io.Serializable;

public enum WonderPosition implements BoardScreenPosition, Serializable {
    INITIAL(-1),
    PLAYER_DOWN_0(0), PLAYER_DOWN_1(1), PLAYER_DOWN_2(2), PLAYER_DOWN_3(3),
    PLAYER_UP_0(4), PLAYER_UP_1(5), PLAYER_UP_2(6), PLAYER_UP_3(7),
    SELECTABLE_0(8), SELECTABLE_1(9), SELECTABLE_2(10), SELECTABLE_3(11);

    private int value;
    private static final long serialVersionUID = 1L;

    WonderPosition(int value) {
        this.value = value;
    }

    public int getValue(){
        return value;
    }

    @Override
    public int getIndex() throws UnknownOrInvalidPosition {
        switch (this) {
            case PLAYER_DOWN_0: case PLAYER_DOWN_1: case PLAYER_DOWN_2: case PLAYER_DOWN_3:
                return value;
            case PLAYER_UP_0: case PLAYER_UP_1: case PLAYER_UP_2: case PLAYER_UP_3:
                return value - 4;
            case SELECTABLE_0: case SELECTABLE_1: case SELECTABLE_2: case SELECTABLE_3:
                return value - 8;
        }
        throw new UnknownOrInvalidPosition(this);
    }

    public static WonderPosition parse(int value)
    {
        for (WonderPosition position : WonderPosition.values()) {
            if (position.value == value) return position;
        }
        throw new IllegalArgumentException("Unknown WonderPosition for int " + value);
    }
}
