package es.pozoesteban.alberto.model.enums;

import java.io.Serializable;

public enum Resource implements Serializable {
    NO_RESOURCE(-1), MONEY(0),
    WOOD(1), CLAY(2), STONE(3), GLASS(4), PAPYRUS(5),
    BROWN_JOKER(6), GREY_JOKER(7),
    WONDER_JOKER(8),
    BLUE_JOKER(9);

    private final int value;

    Resource(int value) {
        this.value = value;
    }

    private static final long serialVersionUID = 1L;
}
