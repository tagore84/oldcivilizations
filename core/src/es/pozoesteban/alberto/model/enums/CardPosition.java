package es.pozoesteban.alberto.model.enums;

import es.pozoesteban.alberto.model.exceptions.UnknownOrInvalidPosition;
import es.pozoesteban.alberto.screens.BoardScreenPosition;

import java.io.Serializable;

public enum CardPosition implements BoardScreenPosition, Serializable {
    INITIAL(-1),
    ON_BOARD_0(0), ON_BOARD_1(1), ON_BOARD_2(2), ON_BOARD_3(3), ON_BOARD_4(4), ON_BOARD_5(5), ON_BOARD_6(6),
    ON_BOARD_7(7), ON_BOARD_8(8), ON_BOARD_9(9), ON_BOARD_10(10), ON_BOARD_11(11), ON_BOARD_12(12), ON_BOARD_13(13),
    ON_BOARD_14(14), ON_BOARD_15(15), ON_BOARD_16(16), ON_BOARD_17(17), ON_BOARD_18(18), ON_BOARD_19(19),
    GRAVEYARD(20),
    PLAYED_DOWN_0(21),PLAYED_DOWN_1(22),PLAYED_DOWN_2(23),PLAYED_DOWN_3(24),PLAYED_DOWN_4(25),PLAYED_DOWN_5(26),
    PLAYED_DOWN_6(27),PLAYED_DOWN_7(28),PLAYED_DOWN_8(29),PLAYED_DOWN_9(30),PLAYED_DOWN_10(31),PLAYED_DOWN_11(32),
    PLAYED_DOWN_12(33),PLAYED_DOWN_13(34),PLAYED_DOWN_14(35),PLAYED_DOWN_15(36),PLAYED_DOWN_16(37),PLAYED_DOWN_17(38),
    PLAYED_DOWN_18(39),PLAYED_DOWN_19(40),PLAYED_DOWN_20(41),PLAYED_DOWN_21(42),PLAYED_DOWN_22(43),PLAYED_DOWN_23(44),
    PLAYED_DOWN_24(45),PLAYED_DOWN_25(46),PLAYED_DOWN_26(47),PLAYED_DOWN_27(48),PLAYED_DOWN_28(49),PLAYED_DOWN_29(50),
    WONDER_DOWN_0(51), WONDER_DOWN_1(52), WONDER_DOWN_2(53), WONDER_DOWN_3(54),
    PLAYED_UP_0(55),PLAYED_UP_1(56),PLAYED_UP_2(57),PLAYED_UP_3(58),PLAYED_UP_4(59),PLAYED_UP_5(60),
    PLAYED_UP_6(61),PLAYED_UP_7(62),PLAYED_UP_8(63),PLAYED_UP_9(64),PLAYED_UP_10(65),PLAYED_UP_11(66),
    PLAYED_UP_12(67),PLAYED_UP_13(68),PLAYED_UP_14(69),PLAYED_UP_15(70),PLAYED_UP_16(71),PLAYED_UP_17(72),
    PLAYED_UP_18(73),PLAYED_UP_19(74),PLAYED_UP_20(75),PLAYED_UP_21(76),PLAYED_UP_22(77),PLAYED_UP_23(78),
    PLAYED_UP_24(79),PLAYED_UP_25(80),PLAYED_UP_26(81),PLAYED_UP_27(82),PLAYED_UP_28(83),PLAYED_UP_29(84),
    WONDER_UP_0(85), WONDER_UP_1(86), WONDER_UP_2(87), WONDER_UP_3(88),
    SELECTABLE_0(89), SELECTABLE_1(90), SELECTABLE_2(91), SELECTABLE_3(92), SELECTABLE_4(93), SELECTABLE_5(94),
    SELECTABLE_6(95), SELECTABLE_7(96), SELECTABLE_8(97), SELECTABLE_9(98), SELECTABLE_10(99);

    private int value;

    private static final long serialVersionUID = 1L;

    CardPosition(int value) {
        this.value = value;
    }

    @Override
    public int getIndex() throws UnknownOrInvalidPosition {
        switch (this) {
            case ON_BOARD_0: case ON_BOARD_1: case ON_BOARD_2: case ON_BOARD_3: case ON_BOARD_4:
            case ON_BOARD_5: case ON_BOARD_6: case ON_BOARD_7: case ON_BOARD_8: case ON_BOARD_9:
            case ON_BOARD_10: case ON_BOARD_11: case ON_BOARD_12: case ON_BOARD_13: case ON_BOARD_14:
            case ON_BOARD_15: case ON_BOARD_16: case ON_BOARD_17: case ON_BOARD_18: case ON_BOARD_19:
                return value;
            case PLAYED_DOWN_0: case PLAYED_DOWN_1: case PLAYED_DOWN_2: case PLAYED_DOWN_3: case PLAYED_DOWN_4:
            case PLAYED_DOWN_5: case PLAYED_DOWN_6: case PLAYED_DOWN_7: case PLAYED_DOWN_8: case PLAYED_DOWN_9:
            case PLAYED_DOWN_10: case PLAYED_DOWN_11: case PLAYED_DOWN_12: case PLAYED_DOWN_13: case PLAYED_DOWN_14:
            case PLAYED_DOWN_15: case PLAYED_DOWN_16: case PLAYED_DOWN_17: case PLAYED_DOWN_18: case PLAYED_DOWN_19:
            case PLAYED_DOWN_20: case PLAYED_DOWN_21: case PLAYED_DOWN_22: case PLAYED_DOWN_23: case PLAYED_DOWN_24:
            case PLAYED_DOWN_25: case PLAYED_DOWN_26: case PLAYED_DOWN_27: case PLAYED_DOWN_28: case PLAYED_DOWN_29:
                return value - 21;
            case WONDER_DOWN_0: case WONDER_DOWN_1: case WONDER_DOWN_2: case WONDER_DOWN_3:
                return value - 51;
            case PLAYED_UP_0: case PLAYED_UP_1: case PLAYED_UP_2: case PLAYED_UP_3: case PLAYED_UP_4:
            case PLAYED_UP_5: case PLAYED_UP_6: case PLAYED_UP_7: case PLAYED_UP_8: case PLAYED_UP_9:
            case PLAYED_UP_10: case PLAYED_UP_11: case PLAYED_UP_12: case PLAYED_UP_13: case PLAYED_UP_14:
            case PLAYED_UP_15: case PLAYED_UP_16: case PLAYED_UP_17: case PLAYED_UP_18: case PLAYED_UP_19:
            case PLAYED_UP_20: case PLAYED_UP_21: case PLAYED_UP_22: case PLAYED_UP_23: case PLAYED_UP_24:
            case PLAYED_UP_25: case PLAYED_UP_26: case PLAYED_UP_27: case PLAYED_UP_28: case PLAYED_UP_29:
                return value - 55;
            case WONDER_UP_0: case WONDER_UP_1: case WONDER_UP_2: case WONDER_UP_3:
                return value - 85;
            case SELECTABLE_0: case SELECTABLE_1: case SELECTABLE_2: case SELECTABLE_3: case SELECTABLE_4: case SELECTABLE_5:
            case SELECTABLE_6: case SELECTABLE_7: case SELECTABLE_8: case SELECTABLE_9: case SELECTABLE_10:
                return value - 89;
        }
        throw new UnknownOrInvalidPosition(this);
    }

    public static CardPosition parse(int value)
    {
        for (CardPosition position : CardPosition.values()) {
            if (position.value == value) return position;
        }
        throw new IllegalArgumentException("Unknown CardPosition for int " + value);
    }
    public int getValue() {
        return value;
    }

}
