package es.pozoesteban.alberto.model.interfaces;

public interface War {
    public int getShields();
}