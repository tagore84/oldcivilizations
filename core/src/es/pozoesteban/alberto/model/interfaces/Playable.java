package es.pozoesteban.alberto.model.interfaces;

import java.util.HashMap;

import es.pozoesteban.alberto.model.entities.Card;
import es.pozoesteban.alberto.model.enums.Resource;

public interface Playable {

    public HashMap<Resource, Integer> getCost();

    public String getChainingPre();
}
