package es.pozoesteban.alberto.model.interfaces;

import es.pozoesteban.alberto.model.enums.Resource;

public interface ResourceProvider {

    public Resource getType();
    public int getAmount();
}

