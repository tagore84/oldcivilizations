package es.pozoesteban.alberto.model.players;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.GameApplication;
import es.pozoesteban.alberto.ai.PedritoElAleatorio;
import es.pozoesteban.alberto.model.GameState;
import es.pozoesteban.alberto.model.entities.Card;
import es.pozoesteban.alberto.model.entities.ProgressToken;
import es.pozoesteban.alberto.model.entities.Wonder;
import es.pozoesteban.alberto.model.enums.Resource;
import es.pozoesteban.alberto.model.enums.ScientificSymbol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;

public class HumanPlayer extends Player {

    private static Logger LOG = new Logger("HumanPlayer", LOG_LEVEL);

    public HumanPlayer(String name, boolean down) {
        super(name, down);
    }

    @Override
    public void onBegingTurn(GameState gameState) {
        LOG.info("Human_onBegingTurn... do nothing...");
        // ToDo Do nothing, I think...
    }

    @Override
    public boolean isAIPlayer() {
        return false;
    }

    @Override
    public Player clone() throws CloneNotSupportedException {
        HumanPlayer clone = (HumanPlayer) super.clone();
        return clone;
    }
}
