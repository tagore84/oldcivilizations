package es.pozoesteban.alberto.model.players;

import es.pozoesteban.alberto.GameApplication;
import es.pozoesteban.alberto.ai.PedritoElAleatorio;
import es.pozoesteban.alberto.ai.MaximoMinguez;
import es.pozoesteban.alberto.ai.PoncioElPonderado;
import es.pozoesteban.alberto.ai.TrilloElExpertillo;
import es.pozoesteban.alberto.model.GameState;

public class PlayerFactory {

    public static final String HUMAN = "human";
    public static final String PEDRITO_EL_ALEATORIO = "Predrito_el_Aleatorio";
    public static final String MAXIMO_MINGUEZ = "Maximo_Minguez";
    public static final String TRILLO_EL_EXPERTILLO = "TrilloElExpertillo";
    public static final String PONCIO_EL_PONDERADO = "PoncioElPonderado";

    public static final String[] TYPES = new String[]{HUMAN, PEDRITO_EL_ALEATORIO, MAXIMO_MINGUEZ};

    public static Player buildHuman(String name, boolean down) {
        return new HumanPlayer(name, down);
    }
    public static Player buildAI(String type, GameState gameState, boolean down) {
        if (type.equalsIgnoreCase(PEDRITO_EL_ALEATORIO)) return new PedritoElAleatorio(down);
        if (type.equalsIgnoreCase(MAXIMO_MINGUEZ)) return new MaximoMinguez(down, new float[]{35f});
        if (type.equalsIgnoreCase(PONCIO_EL_PONDERADO)) return new PoncioElPonderado(down, new float[]{0.5f, 0.5f, 0.5f});
        if (type.equalsIgnoreCase(TRILLO_EL_EXPERTILLO)) return new TrilloElExpertillo(down, new float[]{});
        throw new IllegalArgumentException("Unknown player type in factory: " + type);
    }
}
