package es.pozoesteban.alberto.model.players;

import java.io.Serializable;
import java.util.*;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.GameApplication;
import es.pozoesteban.alberto.model.BoxContent;
import es.pozoesteban.alberto.model.GameState;
import es.pozoesteban.alberto.model.entities.*;
import es.pozoesteban.alberto.model.enums.*;
import es.pozoesteban.alberto.model.exceptions.NotFreeCard;
import es.pozoesteban.alberto.model.exceptions.UnknownOrInvalidPosition;
import es.pozoesteban.alberto.model.interfaces.Playable;
import es.pozoesteban.alberto.model.interfaces.ResourceProvider;
import es.pozoesteban.alberto.model.interfaces.War;
import es.pozoesteban.alberto.screens.BoardScreenPosition;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;

public abstract class Player implements Serializable, Cloneable {

    private static Logger LOG = new Logger("Player", LOG_LEVEL);

    private static final long serialVersionUID = 1L;

    protected static final int INITIAL_MONEY = 6;

    protected String name;
    protected List<Card> played;
    protected List<ProgressToken> progressTokens;
    protected HashMap<ScientificSymbol, Integer> scientificSymbols;
    protected List<Wonder> wonders;
    protected HashMap<Resource, Integer> resources;
    protected List<Resource> specialResourcePrices;
    protected int money;
    protected boolean economy;
    protected boolean bonnusShield;
    protected boolean lastRowBonnus;
    protected int bonnusChaining;
    private boolean downPlayer;
    private boolean dragging;

    private boolean isCloned;


    public Player(String name, boolean downPlayer) {
        this.isCloned = false;
        this.name = name;
        this.played = new ArrayList();
        this.progressTokens = new ArrayList();
        this.scientificSymbols = new HashMap();
        this.wonders = new ArrayList();
        this.resources = new HashMap();
        this.specialResourcePrices = new ArrayList();
        this.money = INITIAL_MONEY;
        this.economy = false;
        this.bonnusShield = false;
        this.lastRowBonnus = false;
        this.bonnusChaining = 0;
        this.downPlayer = downPlayer;
    }

    @Override
    public Player clone() throws CloneNotSupportedException {
        Player clone = (Player)super.clone();
        clone.isCloned = true;
        clone.name = new String(name);
        clone.played = new ArrayList();
        for (Card card : played) {
            clone.played.add(card.clone());
        }
        clone.progressTokens = new ArrayList();
        for (ProgressToken progressToken : progressTokens) {
            clone.progressTokens.add(progressToken.clone());
        }
        clone.scientificSymbols = new HashMap();
        for (Map.Entry<ScientificSymbol, Integer> entry : scientificSymbols.entrySet()) {
            clone.scientificSymbols.put(entry.getKey(), new Integer(entry.getValue()));
        }
        clone.wonders = new ArrayList();
        for (Wonder wonder : wonders) {
            clone.wonders.add(wonder.clone());
        }
        clone.resources = new HashMap();
        for (Map.Entry<Resource, Integer> entry : resources.entrySet()) {
            clone.resources.put(entry.getKey(), new Integer(entry.getValue()));
        }
        clone.specialResourcePrices = new ArrayList();
        for (Resource resource : specialResourcePrices) {
            clone.specialResourcePrices.add(resource);
        }
        clone.money = money;
        clone.economy = economy;
        clone.bonnusShield = bonnusShield;
        clone.lastRowBonnus = lastRowBonnus;
        clone.bonnusChaining = bonnusChaining;
        clone.downPlayer = downPlayer;
        clone.dragging = dragging;
        clone.bonnusChaining = bonnusChaining;
        return clone;
    }

    //<editor-fold desc="Public managment methods">
    public void debt(int amount) {
        money -= amount;
        if (money < 0) money = 0;
    }
    public float getResourcesValue(GameState gameState) {
        Player opponent = isDownPlayer() ? gameState.getUpPlayer() :
                gameState.getDownPlayer();
        BoxContent boxContent = gameState.getBoxContent();
        List<Entity> yetUsed = new ArrayList();
        yetUsed.addAll(played);
        yetUsed.addAll(opponent.getPlayed());
        yetUsed.addAll(gameState.getSelledCards());
        for (Wonder wonder : getWonders()) {
            if (wonder.isBuilt())yetUsed.add(wonder);
        }
        for (Wonder wonder : opponent.getWonders()) {
            if (wonder.isBuilt())yetUsed.add(wonder);
        }

        List<Playable> all = new ArrayList();
        all.addAll(boxContent.getFirstEraCards());
        all.addAll(boxContent.getSecondEraCards());
        all.addAll(boxContent.getThirdEraCards(true));
        all.addAll(boxContent.getThirdEraCards(false));
        all.addAll(boxContent.getWonders());
        all.removeAll(yetUsed);
        HashMap<Entity, Integer> playablesCosts = calcPlayables(gameState, all, false);
        int totalMoney = 0;
        for (Map.Entry<Entity, Integer> entry : playablesCosts.entrySet()) {
            if (!yetUsed.contains(entry.getKey())) {
                totalMoney += entry.getValue();
            } else {
                LOG.error(entry.getKey().toString() + " in yet used and in all, POSIBLE ERROR EN LA CACHE DE playablesCosts");
            }
        }
        return -1 * totalMoney;
    }
    public HashMap<Entity, Integer> calcPlayables(GameState gameState) {
        List<Playable> playables = new ArrayList();
        playables.addAll(gameState.getTableCars().getFreeCards());
        for (Wonder wonder : wonders) {
            if (!wonder.isBuilt()) playables.add(wonder);
        }
        return calcPlayables(gameState, playables, true);
    }
    public HashMap<Entity, Integer> calcPlayables(GameState gameState, List<Playable> playables, boolean filterNotPlayable) {
        HashMap<Entity, Integer> output = new HashMap();
        for (Playable playable : playables) {
            int[] moneyCost = calcMoneyCost(gameState, playable);
            int totalMoneyCost = moneyCost[2] == 1 ? 0 : moneyCost[0] + moneyCost[1];
            if (!filterNotPlayable || totalMoneyCost <= getMoney()) output.put((Entity) playable, totalMoneyCost);
        }
        return output;
    }

    /**
     * Calcula el coste desglosado de construir algo...
     * @param playable Lo que se construye (Carta o Maravilla)
     * @return int[]{moneyToBuyResources, moneyCost, chaining}
     */
    public int[] calcMoneyCost(GameState gameState, Playable playable) {
        int chaining = 0;
        String preName = playable.getChainingPre();
        for (Card card : played) {
            if (card.getName().equalsIgnoreCase(preName)) {
                chaining = 1;
                break;
            }
        }
        TreeMap<Resource, Integer> rest = new TreeMap(new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                Resource r1 = (Resource) o1;
                Resource r2 = (Resource) o2;
                int price1 = getResourcePrice(gameState, r1);
                int price2 = getResourcePrice(gameState, r2);
                if (price1 != price2) return Integer.compare(price2, price1);
                else return r1.compareTo(r2);
            }
        });
        TreeMap<Resource, Integer> cost = new TreeMap(new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                Resource r1 = (Resource) o1;
                Resource r2 = (Resource) o2;
                int price1 = getResourcePrice(gameState, r1);
                int price2 = getResourcePrice(gameState, r2);
                if (price1 != price2) return Integer.compare(price2, price1);
                else return r1.compareTo(r2);
            }
        });
        for (Map.Entry<Resource, Integer> entry : playable.getCost().entrySet()) {
            cost.put(entry.getKey(), entry.getValue());
        }
        int moneyCost = cost.containsKey(Resource.MONEY) ? cost.get(Resource.MONEY) : 0;
        cost.remove(Resource.MONEY);
        rest.putAll(cost);
        for (Map.Entry<Resource, Integer> entry : cost.entrySet()) {
            if (resources.containsKey(entry.getKey())) {
                if (entry.getValue() <= resources.get(entry.getKey())) {
                    rest.remove(entry.getKey());
                } else {
                    rest.put(entry.getKey(), entry.getValue() - resources.get(entry.getKey()));
                }
            }
        }
        if (rest.isEmpty()) return new int[]{0, moneyCost, chaining};
        cost.clear();
        cost.putAll(rest);

        int brownJoker = resources.containsKey(Resource.BROWN_JOKER) ? resources.get(Resource.BROWN_JOKER) : 0;
        int greyJoker = resources.containsKey(Resource.GREY_JOKER) ? resources.get(Resource.GREY_JOKER) : 0;
        for (Map.Entry<Resource, Integer> entry : cost.entrySet()) {
            switch(entry.getKey()) {
                case WOOD: case CLAY: case STONE:
                    if (brownJoker > 0) {
                        if (entry.getValue() <= brownJoker) {
                            rest.remove(entry.getKey());
                        } else {
                            rest.put(entry.getKey(), entry.getValue() - brownJoker);
                        }
                        brownJoker -= entry.getValue();
                    }
                    break;
                case GLASS: case PAPYRUS:
                    if (greyJoker > 0) {
                        if (entry.getValue() <= greyJoker) {
                            rest.remove(entry.getKey());
                        } else {
                            rest.put(entry.getKey(), entry.getValue() - greyJoker);
                        }
                        greyJoker -= entry.getValue();
                    }
                    break;
            }
        }
        if (rest.isEmpty()) return new int[]{0, moneyCost, chaining};

        if (playable instanceof CardBlue) {
            cost.clear();
            cost.putAll(rest);
            int blueJoker = resources.containsKey(Resource.BLUE_JOKER) ? resources.get(Resource.BLUE_JOKER) : 0;

            for (Map.Entry<Resource, Integer> entry : cost.entrySet()) {
                if (blueJoker > 0) {
                    int need = entry.getValue();
                    if (need <= blueJoker) {
                        rest.remove(entry.getKey());
                    } else {
                        rest.put(entry.getKey(), need - brownJoker);
                    }
                    blueJoker -= need;
                } else {
                    break;
                }
            }
        }

        if (playable instanceof Wonder) {
            cost.clear();
            cost.putAll(rest);
            int wonderJoker = resources.containsKey(Resource.WONDER_JOKER) ? resources.get(Resource.WONDER_JOKER) : 0;

            for (Map.Entry<Resource, Integer> entry : cost.entrySet()) {
                if (wonderJoker > 0) {
                    int need = entry.getValue();
                    if (need <= wonderJoker) {
                        rest.remove(entry.getKey());
                    } else {
                        rest.put(entry.getKey(), need - brownJoker);
                    }
                    wonderJoker -= need;
                } else {
                    break;
                }
            }
        }
        int moneyToBuyResources = 0;
        for (Map.Entry<Resource, Integer> entry : rest.entrySet()) {
            moneyToBuyResources += entry.getValue() * (getResourcePrice(gameState, entry.getKey()));
        }
        return new int[]{moneyToBuyResources, moneyCost, chaining};
    }

    public abstract void onBegingTurn(GameState gameState) throws UnknownOrInvalidPosition, NotFreeCard, CloneNotSupportedException;

    public int getFinalPoints(GameState gameState) {
        return sumPoints(getFinalPointsDisaggregated(gameState));
    }
    public int[] getFinalPointsDisaggregated(GameState gameState) {
        int[] points = new int[8]; // Blue, Yellow, Green, Purple, Wonders, Tokens, War, Money
        for (Card card : played) {
            switch (card.getColor()) {
                case BLUE:
                    points[0] += card.getPoints();
                    break;
                case YELLOW:
                    points[1] += card.getPoints();
                    break;
                case GREEN:
                    points[2] += card.getPoints();
                    break;
            }
            if (card.getColor() == CardColor.PURPLE) {
                CardPurple purple = (CardPurple) card;
                points[3] += calcPointsBy(gameState, purple.getPointsByType(), purple.getPointsByAmount());
            }
        }
        for (Wonder wonder : wonders) {
            if (wonder.isBuilt()) points[4] += wonder.getPoints();
        }
        for (ProgressToken token : progressTokens) {
            points[5] += token.getPoints();
            int count = 0;
            int amount = token.getPointsByAmount();
            PointsOrMoneyByType type = token.getPointsByType();
            switch (token.getPointsByType()) {
                case MAX_BLUE:
                case MAX_BROWN_AND_GREY:
                case MAX_GREEN:
                case MAX_RED:
                case MAX_MONEY:
                case MAX_WONDERS:
                case MAX_YELLOW:
                    count = gameState.countMax(type);
                    break;
                case BROWN:
                case GREY:
                case PROGRESS_TOKENS:
                case RED:
                case WONDERS:
                case YELLOW:
                    count = count(type);
                    break;
            }
            points[5] += amount * count;
        }
        points[6] += gameState.getWarPoints(isDownPlayer());
        points[7] += money / 3;
        LOG.debug(pointsToString(points));
        return points;
    }
    private String pointsToString(int[] points) {
        StringBuilder sb = new StringBuilder();
        sb.append(this).append(" points = ");
        sb.append("[Blue: ").append(points[0]).append("], ");
        sb.append("[Yellow: ").append(points[1]).append("], ");
        sb.append("[Green: ").append(points[2]).append("], ");
        sb.append("[Purple: ").append(points[3]).append("], ");
        sb.append("[Wonders: ").append(points[4]).append("], ");
        sb.append("[Token: ").append(points[5]).append("], ");
        sb.append("[War: ").append(points[6]).append("], ");
        sb.append("[Money: ").append(points[7]).append("] ");
        sb.append("Total: ").append(sumPoints(points));
        return sb.toString();
    }
    private int sumPoints(int[] points) {
        int sum = 0;
        for (int point : points) {
            sum += point;
        }
        return sum;
    }
    public boolean play(GameState gameState, ProgressToken token) {
        money += token.getMoney();
        Resource resourceType = token.getResourceType();
        if (resourceType != null && resourceType != Resource.NO_RESOURCE) {
            int amount = resources.get(resourceType);
            amount += token.getResourceAmount();
            resources.put(resourceType, amount);
        }
        if (token.isEconomy()) economy = true;
        if (token.isBonnusShield()) bonnusShield = true;
        if (token.getBonnusChaining() > 0) bonnusChaining = token.getBonnusChaining();
        ScientificSymbol symbol = token.getScientificSymbol();
        if (symbol != null && symbol != ScientificSymbol.NO_SCIENTIFIC_SYMBOL) {
            scientificSymbols.put(symbol, 1); // Solo hay una balanza :P
            if (scientificSymbols.size() >= 6) {
                gameState.setWinner(this, WinType.SCIENTIFIC);
                return true;
            }
        }
        if (token.isWonderPlayAgain()) {
            for (Wonder wonder : wonders) {
                if (!wonder.isBuilt()) wonder.isPlayAgain(true);
            }
        }
        return false;
    }
    public boolean playForFree(GameState gameState, Card entityChoosed) throws UnknownOrInvalidPosition {
        return play(gameState, entityChoosed, null, true);
    }
    public void destroyCard(GameState gameState, Card card) {
        played.remove(card);
        if (card instanceof ResourceProvider) {
            ResourceProvider provider = (ResourceProvider) card;
            int newValue = resources.get(provider.getType());
            newValue -= provider.getAmount();
            if (newValue > 0) resources.put(provider.getType(), newValue);
            else resources.remove(provider.getType());
        }
        switch (card.getColor()) {
            case GREEN:
                ScientificSymbol symbol = ((CardGreen) card).getScientificSymbol();
                if (scientificSymbols.get(symbol) == 1) scientificSymbols.remove(symbol);
                else scientificSymbols.put(symbol, 1);
                break;
            case YELLOW:
                CardYellow yellow = (CardYellow) card;
                if (yellow.getSpecialPrice() != null) {
                    this.specialResourcePrices.removeAll(yellow.getSpecialPrice());
                }
                break;
        }
    }
    public boolean play(GameState gameState, Playable playable) throws UnknownOrInvalidPosition {
        return play(gameState, playable, null);
    }
    public boolean play(GameState gameState, Playable playable, Card with) throws UnknownOrInvalidPosition {
        return play(gameState, playable, with, false);
    }
    public boolean play(GameState gameState, Playable playable, Card with, boolean free) throws UnknownOrInvalidPosition {
        if (!free) {
            int[] moneyCost = calcMoneyCost(gameState, playable);
            int totalMoneyCost = moneyCost[2] == 1 ? 0 : moneyCost[0] + moneyCost[1];
            money -= totalMoneyCost;
            Player opponent = isDownPlayer() ? gameState.getUpPlayer() : gameState.getDownPlayer();
            if (opponent.economy) opponent.money += moneyCost[0];
        }
        if (playable instanceof ResourceProvider) {
            ResourceProvider provider = (ResourceProvider) playable;
            int old = resources.containsKey(provider.getType()) ? resources.get(provider.getType()) : 0;
            resources.put(provider.getType(), old + provider.getAmount());
        }
        if (playable instanceof War) {
            War war = (War) playable;
            int shields = war.getShields();
            if (bonnusShield && playable instanceof CardRed) shields++;
            if (gameState.moveWar(this, shields)) {

                return true;
            }
        }
        if (playable instanceof Wonder) {
            return playWonder(gameState, (Wonder) playable, with);
        } else {
            return playCard(gameState, (Card) playable);
        }
    }
    public int getResourcePrice(GameState gameState, Resource resource) {
        if (specialResourcePrices.contains(resource)) return 1;
        if (resource == Resource.MONEY) return 1;
        return gameState.getResourcePrice(resource, this);
    }
    public int getSellerPrice() {
        int sum = 2;
        for (Card card : played) {
            if (card.getColor() == CardColor.YELLOW) sum++;
        }
        return sum;
    }
    public void sellCard() {
        money += getSellerPrice();
    }
    //</editor-fold>

    //<editor-fold desc="Getters & Setters">
    public String getName() {
        return name;
    }
    public boolean isBonnusShield() {
        return bonnusShield;
    }
    public boolean isCloned() {
        return isCloned;
    }
    public void addWonder(Wonder wonder) {
        wonders.add(wonder);
    }
    public List<ProgressToken> getProgressTokens() {
        return progressTokens;
    }
    public HashMap<ScientificSymbol, Integer> getScientificSymbols() {
        return scientificSymbols;
    }
    public HashMap<Resource, Integer> getResources() {
        return resources;
    }
    public List<Card> getPlayed() {
        return played;
    }
    public List<Wonder> getWonders() {
        return wonders;
    }
    public List<Wonder> getWonders(boolean justBuilts) {
        List<Wonder> output = new ArrayList();
        for (Wonder wonder : wonders) {
            if (wonder.isBuilt()) output.add(wonder);
        }
        return output;
    }
    public boolean isDownPlayer() {
        return downPlayer;
    }
    public int getMoney() {
        return money;
    }
    public List<Card> getPlayedCards() {
        return played;
    }
    public abstract boolean isAIPlayer();
    public BoardScreenPosition getNextProgressTokenSlot() {
        if (downPlayer) {
            switch (progressTokens.size()) {
                case 0:
                    return ProgressTokenPosition.PLAYED_DOWN_0;
                case 1:
                    return ProgressTokenPosition.PLAYED_DOWN_1;
                case 2:
                    return ProgressTokenPosition.PLAYED_DOWN_2;
                case 3:
                    return ProgressTokenPosition.PLAYED_DOWN_3;
                case 4:
                    return ProgressTokenPosition.PLAYED_DOWN_4;
            }
        } else {
            switch (progressTokens.size()) {
                case 0:
                    return ProgressTokenPosition.PLAYED_UP_0;
                case 1:
                    return ProgressTokenPosition.PLAYED_UP_1;
                case 2:
                    return ProgressTokenPosition.PLAYED_UP_2;
                case 3:
                    return ProgressTokenPosition.PLAYED_UP_3;
                case 4:
                    return ProgressTokenPosition.PLAYED_UP_4;
            }
        }
        throw new IllegalArgumentException("Error finding progress token slot on player " + this);
    }
    //</editor-fold>

    //<editor-fold desc="Private tools">
    private boolean playCard(GameState gameState, Card card) throws UnknownOrInvalidPosition {
        played.add(card);
        switch (card.getColor()) {
            case GREEN:
                CardGreen green = (CardGreen) card;
                ScientificSymbol symbol = green.getScientificSymbol();
                if (scientificSymbols.containsKey(symbol)) {
                    scientificSymbols.put(symbol, 2);
                    gameState.setChooseProgressToken(true);
                } else {
                    scientificSymbols.put(symbol, 1);
                    if (scientificSymbols.size() >= 6) {
                        gameState.setWinner(this, WinType.SCIENTIFIC);
                        return true;
                    }
                }
                break;
            case PURPLE:
                CardPurple purple = (CardPurple) card;
                money += calcMoneyBy(gameState, purple.getMoneyByType(), purple.getMoneyByAmount());
                break;
            case YELLOW:
                CardYellow yellow = (CardYellow) card;
                money += yellow.getMoney();
                if (yellow.getSpecialPrice() != null) {
                    this.specialResourcePrices.addAll(yellow.getSpecialPrice());
                }
                money += calcMoneyBy(gameState, yellow.getMoneyByType(), yellow.getMoneyByAmount());
                break;
        }
        return false;
    }
    protected int calcPointsBy(GameState gameState, PointsOrMoneyByType type, int amount) {
        int count = 0;
        switch (type) {
            case MAX_BLUE:
            case MAX_BROWN_AND_GREY:
            case MAX_GREEN:
            case MAX_RED:
            case MAX_MONEY:
            case MAX_WONDERS:
            case MAX_YELLOW:
                count = gameState.countMax(type);
                break;
            case BROWN:
            case GREY:
            case PROGRESS_TOKENS:
            case RED:
            case WONDERS:
            case YELLOW:
                count = count(type);
                break;
        }
        return count * amount;
    }
    protected int calcMoneyBy(GameState gameState, PointsOrMoneyByType type, int amount) {
        int count = 0;
        switch (type) {
            case MAX_BLUE: case MAX_BROWN_AND_GREY: case MAX_GREEN: case MAX_RED: case MAX_MONEY:
            case MAX_WONDERS: case MAX_YELLOW:
                count = gameState.countMax(type);
                break;
            case BROWN: case GREY: case PROGRESS_TOKENS: case RED: case WONDERS: case YELLOW:
                count = count(type);
                break;
        }
        return amount * count;
    }
    private boolean playWonder(GameState gameState, Wonder wonder, Card with) throws UnknownOrInvalidPosition {
        wonder.isBuilt(true);
        with.builtWonder(wonder.peekPosition());
        money += wonder.getMoney();
        if (wonder.isPlayAgain()) gameState.playAgain();
        if (wonder.isDestroyBrownCard()) gameState.setDestroyCard(CardColor.BROWN);
        if (wonder.isDestroyGreyCard()) gameState.setDestroyCard(CardColor.GREY);
        if (wonder.isGetProgressToken()) {
            gameState.setChooseProgressToken(false);
        }
        if (wonder.isGetFromGraveyard()) gameState.setChooseFromGraveyard();
        if (wonder.isGetLastRowCard()) gameState.setGetLastRowCard();
        return false;
    }

    public int count(PointsOrMoneyByType type) {
        int count = 0;
        switch (type) {
            case MAX_BLUE: case MAX_BROWN_AND_GREY: case MAX_GREEN: case MAX_RED:
            case MAX_YELLOW: case BROWN: case YELLOW: case RED: case GREY:
                for (Card card : played) {
                    switch (card.getColor()) {
                        case BROWN:
                            if (type == PointsOrMoneyByType.MAX_BROWN_AND_GREY || type == PointsOrMoneyByType.BROWN) count++;
                            break;
                        case GREY:
                            if (type == PointsOrMoneyByType.MAX_BROWN_AND_GREY || type == PointsOrMoneyByType.GREY) count++;
                            break;
                        case YELLOW:
                            if (type == PointsOrMoneyByType.MAX_YELLOW || type == PointsOrMoneyByType.YELLOW) count++;
                            break;
                        case BLUE:
                            if (type == PointsOrMoneyByType.MAX_BLUE) count++;
                            break;
                        case GREEN:
                            if (type == PointsOrMoneyByType.MAX_GREEN) count++;
                            break;
                        case RED:
                            if (type == PointsOrMoneyByType.MAX_RED || type == PointsOrMoneyByType.RED) count++;
                            break;
                    }
                }
                break;
            case MAX_MONEY:
                count = money / 3;
                break;
            case WONDERS: case MAX_WONDERS:
                for (Wonder wonder : wonders) {
                    if (wonder.isBuilt()) count++;
                }
                break;
            case PROGRESS_TOKENS:
                count = progressTokens.size();
                break;
        }
        return count;
    }

    public List<Wonder> getPlayableWonders() {
        List<Wonder> output = new ArrayList();
        for (Wonder wonder : wonders) {
            if (!wonder.isBuilt()) output.add(wonder);
        }
        return output;
    }
    public BoardScreenPosition getSlotWonder(Wonder wonderBuilt) {
        int index = wonders.indexOf(wonderBuilt);
        int base = isDownPlayer() ? CardPosition.WONDER_DOWN_0.getValue() : CardPosition.WONDER_UP_0.getValue();
        return CardPosition.parse(base + index);
    }
    public BoardScreenPosition getNextCardSlot() {
        int value = isDownPlayer() ? CardPosition.PLAYED_DOWN_0.getValue() :
                CardPosition.PLAYED_UP_0.getValue();
        value += played.size();
        return CardPosition.parse(value);
    }

    //</editor-fold>

    //<editor-fold desc="Overrides">
    @Override
    public String toString() {
        return name + (isDownPlayer() ? " [DOWN]" : " [UP]");
    }

    public boolean hasEconomy() {
        return economy;
    }

    public void isDragging(boolean b) {
        this.dragging  = b;
    }
    public boolean isDragging() {
        return this.dragging;
    }

    //</editor-fold>

    //<editor-fold desc="Tools">
    public List<Move> generateLegalMoves(GameState gameState, boolean downPlayer, boolean simulated) {
        List<Move> legalMoves = new ArrayList();
        Player player = downPlayer ? gameState.getDownPlayer() :
                gameState.getUpPlayer();
        switch (gameState.getTurnStatus()) {
            case NORMAL_TURN:
                HashMap<Entity, Integer> playables = player.calcPlayables(gameState);
                Set<Card> freeCards = gameState.getTableCars().getFreeCards();

                for (Entity entity : playables.keySet()) {
                    if (entity instanceof Card) {
                        legalMoves.add(new Move(entity, Action.BUILD_CARD, null, simulated));
                    } else if (entity instanceof Wonder){
                        for (Card freeCard : freeCards) {
                            legalMoves.add(new Move(freeCard, Action.BUILD_WONDER, (Wonder) entity, simulated));
                        }
                    }
                }
                for (Card freeCard : freeCards) {
                    legalMoves.add(new Move(freeCard, Action.SELL, null, simulated));
                }
                break;
            case CHOOSING_PROGRESS_TOKEN:
                for (ProgressToken token : gameState.getProgressTokensToSelect()) {
                    legalMoves.add(new Move(token, Action.SELECT_PROGRESS_TOKEN, null, simulated));
                }
                break;
            case CHOOSING_CARD_TO_GET_FOR_FREE:
                for (Card card : gameState.getCardsToGetForFreeToSelect()) {
                    legalMoves.add(new Move(card, Action.BUILT_CARD_FOR_FREE, null, simulated));
                }
                break;
            case CHOOSING_CARD_TO_DESTROY:
                for (Card card : gameState.getCardsToDestroyToSelect()) {
                    legalMoves.add(new Move(card, Action.DESTROY_CARD, null, simulated));
                }
                break;
        }
        return legalMoves;
    }
    //</editor-fold>

}

