package es.pozoesteban.alberto.model.players;

import es.pozoesteban.alberto.model.entities.Card;
import es.pozoesteban.alberto.model.entities.Entity;
import es.pozoesteban.alberto.model.entities.Wonder;
import es.pozoesteban.alberto.model.enums.Action;
import es.pozoesteban.alberto.utils.Utils;

import java.io.Serializable;
import java.util.List;

public class Move implements Comparable<Move>, Serializable {

    private Entity entityChoosed;
    private final Action action;
    private Wonder wonderBuilt;
    private int wonderBuiltIndex = -1;
    private boolean simulated;
    private float weight = -1;

    public Move clone() {
        return this;
        // ToDo clone!
    }

    public void setEntityChoosed(Entity entity) {
        this.entityChoosed = entity;
    }

    public Move(Entity entityChoosed, Action action, Wonder wonderBuilt, boolean simulated) {
        this.entityChoosed = entityChoosed;
        this.action = action;
        this.wonderBuilt = wonderBuilt;
        this.simulated = simulated;
        if ((action == es.pozoesteban.alberto.model.enums.Action.BUILD_WONDER) != (wonderBuilt != null)) {
            throw new IllegalArgumentException("Move " + action + " and wonder " + wonderBuilt);
        }
    }
    public Move(Entity entityChoosed, Action action, Wonder wonderBuilt, boolean simulated, float weight) {
        this(entityChoosed, action, wonderBuilt, simulated);
        this.weight = weight;
    }
    public Move(Entity entityChoosed, Action action, int wonderBuiltIndex, boolean simulated) {
        this.entityChoosed = entityChoosed;
        this.action = action;
        this.wonderBuiltIndex = wonderBuiltIndex;
        this.simulated = simulated;
        if (action == es.pozoesteban.alberto.model.enums.Action.BUILD_WONDER) {
            throw new IllegalArgumentException("Move " + action + " and wonderBuiltIndex provided " + wonderBuiltIndex);
        }
    }

    public static Move parse(Move move, List<Wonder> wonders, boolean simulated) {
        if (move.wonderBuiltIndex >= 0) {
            return new Move(move.entityChoosed, move.action, wonders.get(move.wonderBuiltIndex), simulated);
        } else {
            return move;
        }
    }

    public Entity getEntityChoosed() {
        return entityChoosed;
    }

    public Action getAction() {
        return action;
    }

    public Wonder getWonderBuilt() {
        return wonderBuilt;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }
    public float getWeight() {
        return this.weight;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(action).append(" ").append(entityChoosed);
        if (wonderBuilt != null) sb.append(" -> ").append(wonderBuilt);
        sb.append(" [").append(Utils.doubleToPercentage(weight)).append("]");
        return sb.toString();
    }

    @Override
    public int compareTo(Move o) {
        int byWeght = Float.compare(o.getWeight(), weight);
        if (byWeght != 0) return byWeght;
        int a1 = getAction().getValue();
        int a2 = o.getAction().getValue();
        if (a1 == a2) {
            return getEntityChoosed().compareTo(o.getEntityChoosed());
        } else if (a1 < a2)return -1;
        else return 1;
    }

    public boolean isSimulated() {
        return simulated;
    }

    public void isSimulated(boolean b) {
        this.simulated = b;
    }
}