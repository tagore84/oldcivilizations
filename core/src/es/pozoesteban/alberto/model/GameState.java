package es.pozoesteban.alberto.model;

import java.io.Serializable;
import java.util.*;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.logging.Match;
import es.pozoesteban.alberto.model.entities.*;
import es.pozoesteban.alberto.model.enums.*;
import es.pozoesteban.alberto.model.exceptions.UnknownOrInvalidPosition;
import es.pozoesteban.alberto.model.players.Move;
import es.pozoesteban.alberto.model.exceptions.NotFreeCard;
import es.pozoesteban.alberto.model.players.Player;
import es.pozoesteban.alberto.model.interfaces.ResourceProvider;
import es.pozoesteban.alberto.utils.Utils;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;

public class GameState implements Serializable {

    private static final long serialVersionUID = 1L;

    private static Logger LOG = new Logger("GameState", LOG_LEVEL);

    public final static int WAR_SIZE = 9;

    private final boolean isClone;
    private BoxContent content;
    private Player downPlayer;
    private Player upPlayer;
    private List<Card> firstEraCards;
    private List<Card> secondEraCards;
    private List<Card> thirdEraCards;
    private BoardCardsSimple tableCars;
    private List<ProgressToken> progressTokens;
    private List<Card> selledCards;
    private Player currentPlayer;
    private Player winner;
    private WinType winType;
    private int turn;
    private TurnStatus turnStatus;
    private int warState;
    private Era currentEra;
    private List<Wonder> wonders;
    private List<Wonder> wondersToSelect;
    private List<ProgressToken> progressTokensToSelect;
    private List<Card> cardsToGetForFreeToSelect;
    private List<Card> cardsToDestroyToSelect;
    private boolean playAgain;
    private boolean[] debtWar;
    private Match matchLogging;

    private GameState undo;

    public GameState(Player downPlayer, Player upPlayer, List<Card> firstEraCards, List<Card> secondEraCards,
                     List<Card> thirdEraCards, BoardCardsSimple tableCars, List<ProgressToken> progressTokens,
                     List<Card> selledCards, Player currentPlayer, Player winner, WinType winType, int turn, TurnStatus turnStatus,
                     int warState, Era currentEra, List<Wonder> wonders, List<Wonder> wondersToSelect,
                     boolean playAgain, boolean[] debtWar, BoxContent content, GameState undo) {
        this.isClone = true;
        this.downPlayer = downPlayer;
        this.upPlayer = upPlayer;
        this.firstEraCards = firstEraCards;
        this.secondEraCards = secondEraCards;
        this.thirdEraCards = thirdEraCards;
        this.tableCars = tableCars;
        this.progressTokens = progressTokens;
        this.selledCards = selledCards;
        this.currentPlayer = currentPlayer;
        this.winner = winner;
        this.winType = winType;
        this.turn = turn;
        this.turnStatus = turnStatus;
        this.warState = warState;
        this.currentEra = currentEra;
        this.wonders = wonders;
        this.wondersToSelect = wondersToSelect;
        this.playAgain = playAgain;
        this.debtWar = debtWar;
        this.content = content;
        this.undo = undo;
    }


    public GameState(Player downPlayer, Player upPlayer) throws UnknownOrInvalidPosition {
        this.isClone = false;
        debtWar = new boolean[]{true, true, true, true};
        content = BoxContent.build();
        this.downPlayer = downPlayer;
        this.upPlayer = upPlayer;
        if (Utils.DICE.nextBoolean()) {
            this.matchLogging = new Match(this, downPlayer, upPlayer, true);
            currentPlayer =  downPlayer;
        } else {
            this.matchLogging = new Match(this, downPlayer, upPlayer, false);
            currentPlayer = upPlayer;
        }
        currentEra = Era.WONDER_SELECTION;
        progressTokens = (List<ProgressToken>) randomSelection(content.getProgressTokens(), 8);
        for (int i = 0; i < progressTokens.size(); i++) {
            if (i < 5 ) {
                progressTokens.get(i).pushPosition(ProgressTokenPosition.parse(i));
                progressTokens.get(i).setVisible(true);
                progressTokens.get(i).pushState(ProgressTokenState.ON_BOARD);
                this.matchLogging.saveProgressToken(true, progressTokens.get(i));
            }
            else {
                progressTokens.get(i).pushPosition(ProgressTokenPosition.INITIAL);
                progressTokens.get(i).pushState(ProgressTokenState.THREE_EXTRA);
                this.matchLogging.saveProgressToken(false, progressTokens.get(i));
            }
        }
        selledCards = new ArrayList();
        firstEraCards = (List<Card>) randomSelection(content.getFirstEraCards(), 20);
        for (int i = 0; i < firstEraCards.size(); i++) {
            firstEraCards.get(i).pushPosition(CardPosition.parse(i));
            firstEraCards.get(i).setVisible(false);
            firstEraCards.get(i).pushState(CardState.OUT);
        }
        secondEraCards = (List<Card>) randomSelection(content.getSecondEraCards(), 20);
        for (int i = 0; i < secondEraCards.size(); i++) {
            secondEraCards.get(i).pushPosition(CardPosition.parse(i));
            secondEraCards.get(i).setVisible(false);
            firstEraCards.get(i).pushState(CardState.OUT);
        }
        thirdEraCards = (List<Card>) randomSelection(content.getThirdEraCards(false), 17);
        thirdEraCards.addAll((List<Card>) randomSelection(content.getThirdEraCards(true), 3));
        Collections.shuffle(thirdEraCards);
        for (int i = 0; i < thirdEraCards.size(); i++) {
            thirdEraCards.get(i).pushPosition(CardPosition.parse(i));
            thirdEraCards.get(i).setVisible(false);
            firstEraCards.get(i).pushState(CardState.OUT);
        }
        wondersToSelect = new ArrayList();
        wonders = (List<Wonder>) randomSelection(content.getWonders(), 8);
        setWondersToSelect(true);
        turn = 1;
        turnStatus = TurnStatus.INIT_CHOOSING_WONDER_1_8;
        warState = 0;
    }

    private void setWondersToSelect(boolean first) throws UnknownOrInvalidPosition {
        if (first) {
            for (int i = 0; i < 8; i++) {
                Wonder wonder = wonders.get(i);
                if (i < 4) {
                    this.matchLogging.saveInititalWonder(true, wonder);
                    wondersToSelect.add(wonder);
                    wonder.pushPosition(WonderPosition.parse(8 + i)); // Para elegir (entre cuatro opciones)
                    wonder.pushState(WonderState.SELECTABLE);
                    wonder.setVisible(true);
                } else {
                    this.matchLogging.saveInititalWonder(false, wonder);
                    wonder.pushPosition(WonderPosition.INITIAL); // Para elegir (entre cuatro opciones)
                    wonder.pushState(WonderState.OUT);
                    wonder.setVisible(false);
                }
            }
        } else {
            for (int i = 4; i < 8; i++) {
                Wonder wonder = wonders.get(i);
                wondersToSelect.add(wonder);
                wonder.pushPosition(WonderPosition.parse(4 + i)); // Para elegir (entre cuatro opciones)
                wonder.setVisible(true);
                wonder.pushState(WonderState.SELECTABLE);
            }
        }
    }

    //<editor-fold desc="Public management methods">
    public int potencialShields(boolean isDownPlayer) {
        Player player = isDownPlayer ? downPlayer : upPlayer;
        float potencialShields = 0;
        for (Wonder wonder : player.getWonders()) {
            if (!wonder.isBuilt()) potencialShields += wonder.getShields();
        }
        boolean bonnusShields = player.isBonnusShield();
        if (!bonnusShields) {
            for (ProgressToken token : progressTokens) {
                if (token.peekState() == ProgressTokenState.ON_BOARD || token.peekState() == ProgressTokenState.SELECTABLE) {
                    if (token.isBonnusShield()) {
                        bonnusShields = true;
                        break;
                    }
                }
            }
        }
        List<Card> played = new ArrayList();
        played.addAll(downPlayer.getPlayed());
        played.addAll(upPlayer.getPlayed());
        float[] redCardsPlayed = new float[3];
        for (Card card : played) {
            if (card instanceof CardRed) {
                redCardsPlayed[card.getEra().getValue()-1]++;
            }
        }
        float cardsPlayable = 0;
        switch (currentEra) {
            case FIRST:
                cardsPlayable = 4 - redCardsPlayed[0];
                potencialShields += cardsPlayable * (bonnusShields ? 2 : 1);
                break;
            case SECOND:
                cardsPlayable = 5 - redCardsPlayed[1];
                potencialShields += cardsPlayable * (bonnusShields ? 2.6f : 1.6f);
                break;
            case THIRD:
                cardsPlayable = 5 - redCardsPlayed[2];
                potencialShields += cardsPlayable * (bonnusShields ? 3.4f : 2.4f);
                break;
        }
        return Math.round(potencialShields);
    }
    public Map<ScientificSymbol, Integer> potencialScientificSymbols() {
        Map<ScientificSymbol, Integer> potencial = new HashMap();
        potencial.put(ScientificSymbol.COMPASS, 2);
        potencial.put(ScientificSymbol.WHEEL, 2);
        potencial.put(ScientificSymbol.PEN, 2);
        potencial.put(ScientificSymbol.MORTAR, 2);
        potencial.put(ScientificSymbol.SOLAR_WATCH, 2);
        potencial.put(ScientificSymbol.GYROSCOPE, 2);
        potencial.put(ScientificSymbol.BALANCE, 1);
        for (ProgressToken token : getProgressTokens()) {
            if (token.peekState() == ProgressTokenState.ON_BOARD ||
                    token.peekState() == ProgressTokenState.SELECTABLE) {
                if (token.getScientificSymbol() != null && token.getScientificSymbol() == ScientificSymbol.BALANCE) {
                    potencial.put(ScientificSymbol.BALANCE, 0);
                }
            }
        }
        switch (getCurrentEra()) {
            case FIRST:
                break;
            case SECOND:
            case THIRD:
                List<Card> playedSecond = new ArrayList();
                playedSecond.addAll(getDownPlayer().getPlayed());
                playedSecond.addAll(getUpPlayer().getPlayed());
                for (Card card : playedSecond) {
                    if (card instanceof CardGreen) {
                        ScientificSymbol symbol = ((CardGreen)card).getScientificSymbol();
                        if (symbol != null && symbol != ScientificSymbol.NO_SCIENTIFIC_SYMBOL && potencial.containsKey(symbol)) {
                            if (potencial.get(symbol) == 2) potencial.put(symbol, 1);
                            if (potencial.get(symbol) == 1) potencial.remove(symbol);
                        }
                    }
                }
        }
        return potencial;
    }
    public void undoLastMove() {
        this.downPlayer = undo.downPlayer;
        this.upPlayer = undo.upPlayer;
        this.firstEraCards = undo.firstEraCards;
        this.secondEraCards = undo.secondEraCards;
        this.thirdEraCards = undo.thirdEraCards;
        this.tableCars = undo.tableCars;
        this.progressTokens = undo.progressTokens;
        this.selledCards = undo.selledCards;
        this.currentPlayer = undo.currentPlayer;
        this.winner = undo.winner;
        this.winType = undo.winType;
        this.turn = undo.turn;
        this.turnStatus = undo.turnStatus;
        this.warState = undo.warState;
        this.currentEra = undo.currentEra;
        this.wonders = undo.wonders;
        this.wondersToSelect = undo.wondersToSelect;
        this.playAgain = undo.playAgain;
        this.content = undo.content;
        this.undo = undo.undo;
    }
    public boolean play (Move move) throws UnknownOrInvalidPosition, NotFreeCard, CloneNotSupportedException {
        playAgain = false;
        undo = this.clone();
        if (!move.isSimulated()) {
            matchLogging.saveMove(currentPlayer, move, this);
        }
        Entity entityChoosed = move.getEntityChoosed();
        boolean suddenWin = false;
        switch (move.getAction()) {
            case INIT_SELECT_WONDER:
                suddenWin = initWonderSelection((Wonder) move.getEntityChoosed());
                break;
            case SELECT_PROGRESS_TOKEN:
                suddenWin = selectProgressToken((ProgressToken)entityChoosed);
                break;
            case SELL:
                takeCardFromBoard((Card) entityChoosed, move.isSimulated());
                suddenWin = sellCard((Card) entityChoosed);
                break;
            case BUILD_CARD:
                takeCardFromBoard((Card) entityChoosed, move.isSimulated());
                suddenWin = buildCard((Card) entityChoosed, false);
                break;
            case BUILD_WONDER:
                takeCardFromBoard((Card) entityChoosed, move.isSimulated());
                suddenWin = buildWonder((Card) entityChoosed, move.getWonderBuilt());
                break;
            case BUILT_CARD_FOR_FREE:
                suddenWin = buildCard((Card) entityChoosed, true);
                break;
            case DESTROY_CARD:
                suddenWin = destroyCard((Card) entityChoosed);
                break;
        }
        if (turnStatus == TurnStatus.NORMAL_TURN) {
            turn++;
            if (currentEra == Era.WONDER_SELECTION) nextEra();
        }
        if (tableCars != null && tableCars.isEmpty() && turnStatus == TurnStatus.NORMAL_TURN) {
            nextEra();
        }
        return suddenWin;
    }
    private boolean destroyCard(Card card) throws UnknownOrInvalidPosition {
        Player opponent = currentPlayer.isDownPlayer() ? getUpPlayer() : getDownPlayer();
        card.pushState(CardState.OUT);
        card.pushPosition(CardPosition.INITIAL);
        opponent.destroyCard(this, card);
        turnStatus = TurnStatus.NORMAL_TURN;
        return false;
    }
    private boolean selectProgressToken(ProgressToken token) throws UnknownOrInvalidPosition {
        token.pushPosition(currentPlayer.getNextProgressTokenSlot());
        token.pushState(currentPlayer.isDownPlayer() ? ProgressTokenState.PLAYED_BY_DOWN : ProgressTokenState.PLAYED_BY_UP);
        progressTokensToSelect.remove(token);
        turnStatus = TurnStatus.NORMAL_TURN;
        for (ProgressToken other : progressTokensToSelect) {
            other.popPosition();
            other.popState();
        }
        progressTokensToSelect.clear();
        return currentPlayer.play(this, token);
    }
    private void selectWonder(Wonder wonder, int index) throws UnknownOrInvalidPosition {
        int newPosValue = currentPlayer.isDownPlayer() ? (WonderPosition.PLAYER_DOWN_0.getValue() + index) :
                (WonderPosition.PLAYER_UP_0.getValue() + index);
        wonder.pushPosition(WonderPosition.parse(newPosValue));
        wonder.pushState(currentPlayer.isDownPlayer() ? WonderState.SELECTED_BY_DOWN : WonderState.SELECTED_BY_UP);
    }
    private boolean initWonderSelection(Wonder wonder) throws UnknownOrInvalidPosition {
        currentPlayer.addWonder(wonder);
        wondersToSelect.remove(wonder);
        switch (turnStatus) {
            case INIT_CHOOSING_WONDER_1_8:
                selectWonder(wonder, 0);
                break;
            case INIT_CHOOSING_WONDER_2_8:
                selectWonder(wonder, 1);
                playAgain();
                break;
            case INIT_CHOOSING_WONDER_3_8:
                selectWonder(wonder, 2);
                break;
            case INIT_CHOOSING_WONDER_4_8:
                selectWonder(wonder, 3);
                setWondersToSelect(false);
                break;
            case INIT_CHOOSING_WONDER_5_8:
                selectWonder(wonder, 0);
                break;
            case INIT_CHOOSING_WONDER_6_8:
                selectWonder(wonder, 1);
                playAgain();
                break;
            case INIT_CHOOSING_WONDER_7_8:
                selectWonder(wonder, 2);
                break;
            case INIT_CHOOSING_WONDER_8_8:
                selectWonder(wonder, 3);
                break;
        }
        turnStatus = TurnStatus.parse(turnStatus.getValue() + 1);
        return false;
    }
    private boolean buildCard(Card card, boolean forFree) throws UnknownOrInvalidPosition {
        card.pushState(currentPlayer == downPlayer ? CardState.PLAYED_BY_DOWN : CardState.PLAYED_BY_UP);
        card.pushPosition(currentPlayer.getNextCardSlot());
        if (forFree) {
            cardsToGetForFreeToSelect.remove(card);
            for (Card freeCard : cardsToGetForFreeToSelect) {
                freeCard.popPosition();
                freeCard.popState();
            }
            cardsToGetForFreeToSelect.clear();
            turnStatus = TurnStatus.NORMAL_TURN;
            return currentPlayer.playForFree(this, card);
        } else {
            return currentPlayer.play(this, card);
        }

    }
    private boolean sellCard(Card card) throws UnknownOrInvalidPosition {
        card.pushPosition(CardPosition.GRAVEYARD);
        card.pushState(CardState.SELLED);
        card.setVisible(false);
        currentPlayer.sellCard();
        return false;
    }
    private boolean buildWonder(Card with, Wonder wonder) throws UnknownOrInvalidPosition {
        with.pushState(currentPlayer == downPlayer ? CardState.WONDERED_BY_DOWN : CardState.WONDERED_BY_UP);
        with.pushPosition(currentPlayer.getSlotWonder(wonder));
        wonder.pushState(currentPlayer == downPlayer ? WonderState.PLAYED_BY_DOWN : WonderState.PLAYED_BY_UP);
        return currentPlayer.play(this, wonder, with);

    }
    private void takeCardFromBoard(Card card, boolean simulate) throws NotFreeCard, UnknownOrInvalidPosition {
        if (!tableCars.getFreeCards().contains(card)) {
            if (simulate) {
            } else {
                throw new NotFreeCard(card);
            }
        }
        tableCars.removeCard(card);
    }
    public int countMax(PointsOrMoneyByType type) {
        int up = upPlayer.count(type);
        int down = upPlayer.count(type);
        if (up > down) return up;
        return down;
    }
    public void playAgain() {
        playAgain = true;
    }
    public void nextPlayer() {
        if (playAgain) {
            playAgain = false;
        } else {
            currentPlayer = currentPlayer == downPlayer ? upPlayer : downPlayer;
        }
    }
    public void nextEra() throws UnknownOrInvalidPosition, CloneNotSupportedException {
        if (downPlayer.isAIPlayer() && upPlayer.isAIPlayer()) {
            LOG.debug("Changing ERA to " + Era.parse(currentEra.getValue()+1) + "\n\n");
        } else {
            LOG.info("Changing ERA to " + Era.parse(currentEra.getValue()+1) + "\n\n");
        }
        switch (currentEra) {
            case WONDER_SELECTION:
                currentEra = Era.FIRST;
                tableCars = new BoardCardsSimple(firstEraCards, Era.FIRST);
                if (matchLogging != null) {
                    matchLogging.saveFigure(Era.FIRST, tableCars);
                }
                for (int i  = 0; i < firstEraCards.size(); i++) {
                    Card card = firstEraCards.get(i);
                    card.pushPosition(CardPosition.parse(i));
                    card.isVisible(true);
                    card.pushState(CardState.ON_FIGURE);
                }
                turn = 1;
                break;
            case FIRST:
                currentEra = Era.SECOND;
                tableCars = new BoardCardsSimple(secondEraCards, Era.SECOND);
                if (matchLogging != null) {
                    matchLogging.saveFigure(Era.SECOND, tableCars);
                }
                for (int i  = 0; i < secondEraCards.size(); i++) {
                    Card card = secondEraCards.get(i);
                    card.pushPosition(CardPosition.parse(i));
                    card.isVisible(true);
                    card.pushState(CardState.ON_FIGURE);
                }
                turn = 1;
                if (warState > 0) currentPlayer = upPlayer;
                if (warState < 0) currentPlayer = downPlayer;
                break;
            case SECOND:
                currentEra = Era.THIRD;
                tableCars = new BoardCardsSimple(thirdEraCards, Era.THIRD);
                if (matchLogging != null) {
                    matchLogging.saveFigure(Era.THIRD, tableCars);
                }
                for (int i  = 0; i < thirdEraCards.size(); i++) {
                    Card card = thirdEraCards.get(i);
                    card.pushPosition(CardPosition.parse(i));
                    card.isVisible(true);
                    card.pushState(CardState.ON_FIGURE);
                }
                turn = 1;
                if (warState > 0) currentPlayer = upPlayer;
                if (warState < 0) currentPlayer = downPlayer;
                break;
            case THIRD:
                currentEra = Era.FINISH;
                int downPoints = downPlayer.getFinalPoints(this);
                int upPoints = upPlayer.getFinalPoints(this);
                if (downPoints > upPoints) {
                    setWinner(downPlayer, WinType.BY_POINTS);
                } else if (upPoints > downPoints) {
                    setWinner(upPlayer, WinType.BY_POINTS);
                } else if (downPlayer.getMoney() > upPlayer.getMoney()){
                    setWinner(downPlayer, WinType.BY_POINTS);
                } else if (upPlayer.getMoney() > downPlayer.getMoney()) {
                    setWinner(upPlayer, WinType.BY_POINTS);
                } else {
                    // ToDo check tiebreaker!
                    setWinner(Utils.DICE.nextBoolean() ? downPlayer : upPlayer, WinType.BY_POINTS);
                    if (!isClone) LOG.info("UNRESOLVE TIE!!!");
                }
                break;
        }
    }
    public void setWinner(Player player, WinType winType) {
        if (winType != WinType.BY_POINTS) {
            downPlayer.getFinalPoints(this);
            upPlayer.getFinalPoints(this);
        }
        this.winner = player;
        this.winType = winType;
        if (!isClone) matchLogging.saveWinner(player.isDownPlayer(), winType);
    }
    public int getResourcePrice(Resource resource, Player player) {
        int sum = 2;
        List<Card> opponentCards = player == downPlayer ? upPlayer.getPlayedCards() : downPlayer.getPlayedCards();
        for (Card card : opponentCards) {
            if (card.getColor() == CardColor.BROWN || card.getColor() == CardColor.GREY) {
                ResourceProvider provider = (ResourceProvider) card;
                if (provider.getType() == resource) sum += provider.getAmount();
            }
        }
        return sum;
    }
    public void setChooseFromGraveyard() throws UnknownOrInvalidPosition {
        hide(tableCars.getAllCards());
        cardsToGetForFreeToSelect = new ArrayList();
        for (int i = 0; i < selledCards.size(); i++) {
            Card card = selledCards.get(i);
            card.pushPosition(CardPosition.parse(i + 89));
            card.pushState(CardState.SELECTABLE);
            card.setVisible(true);
            cardsToGetForFreeToSelect.add(card);
            turnStatus = TurnStatus.CHOOSING_CARD_TO_GET_FOR_FREE;
            playAgain();
        }
    }
    public void setGetLastRowCard() throws UnknownOrInvalidPosition {
        hide(tableCars.getAllCards());
        cardsToGetForFreeToSelect = new ArrayList();
        for (int i = 0; i < tableCars.getLastRowCard().size(); i++) {
            Card card = tableCars.getLastRowCard().get(i);
            card.pushPosition(CardPosition.parse(i + CardPosition.SELECTABLE_0.getValue()));
            card.setVisible(true);
            card.pushState(CardState.SELECTABLE);
            cardsToGetForFreeToSelect.add(card);
            turnStatus = TurnStatus.CHOOSING_CARD_TO_GET_FOR_FREE;
            playAgain();
        }
    }
    public void setChooseProgressToken(boolean fromBoard) throws UnknownOrInvalidPosition {
        hide(tableCars.getAllCards());
        int valueNewPosition = ProgressTokenPosition.SELECTABLE_0.getValue();
        progressTokensToSelect = new ArrayList();
        for (ProgressToken token : progressTokens) {
            switch ((ProgressTokenPosition)token.peekPosition()) {
                case ON_BOARD_0: case ON_BOARD_1: case ON_BOARD_2: case ON_BOARD_3: case ON_BOARD_4:
                    if (fromBoard) {
                        token.pushPosition(ProgressTokenPosition.parse(valueNewPosition++));
                        token.setVisible(true);
                        token.pushState(ProgressTokenState.SELECTABLE);
                        progressTokensToSelect.add(token);
                    }
                    break;
                case INITIAL:
                    if (!fromBoard) {
                        token.pushPosition(ProgressTokenPosition.parse(valueNewPosition++));
                        token.setVisible(true);
                        token.pushState(ProgressTokenState.SELECTABLE);
                        progressTokensToSelect.add(token);
                    }
            }
        }
        if (!progressTokensToSelect.isEmpty()) {
            turnStatus = TurnStatus.CHOOSING_PROGRESS_TOKEN;
            playAgain();
        }
    }
    public boolean moveWar(Player player, int shields) {
        warState = player == downPlayer ? (warState += shields) : (warState -= shields);
        if (debtWar[2] && warState > 2) {
            debtWar(false, 2);
            debtWar[2] = false;
        }
        if (debtWar[3] && warState > 5) {
            debtWar(false, 5);
            debtWar[3] = false;
        }
        if (debtWar[1] && warState < -2) {
            debtWar(true, 2);
            debtWar[1] = false;
        }
        if (debtWar[0] && warState < -5) {
            debtWar(true, 5);
            debtWar[0] = false;
        }
        if (Math.abs(warState) >= WAR_SIZE) {
            setWinner(currentPlayer, WinType.MILITARY);
            return true;
        } else {
            return false;
        }
    }
    public int getWarPoints(boolean isDown) {
        if (isDown && warState > 0) {
            if (warState > 5) return 10;
            if (warState > 2) return 5;
            return 2;
        }
        if (!isDown && warState < 0) {
            if (warState < -5) return 10;
            if (warState < -2) return 5;
            return 2;
        }
        return 0;
    }
    private void debtWar(boolean isDown, int amount) {
        Player player = isDown ? downPlayer : upPlayer;
        player.debt(amount);
        // ToDo animation with the debt token!
    }

    public void setDestroyCard(CardColor colorToDestroy) throws UnknownOrInvalidPosition {
        Player other = currentPlayer == downPlayer ? upPlayer : downPlayer;
        cardsToDestroyToSelect = new ArrayList();
        for (Card card : other.getPlayedCards()) {
            if (card.getColor() == colorToDestroy) {
                cardsToDestroyToSelect.add(card);
            }
        }
        if (cardsToDestroyToSelect.isEmpty()) return;
        hide(tableCars.getAllCards());
        for (int i = 0; i < cardsToDestroyToSelect.size(); i++) {
            Card card = cardsToDestroyToSelect.get(i);
            card.pushPosition(CardPosition.parse(90 + i));
            card.pushState(CardState.SELECTABLE);
            card.setVisible(true);
        }
        turnStatus = TurnStatus.CHOOSING_CARD_TO_DESTROY;
        playAgain();
    }
    //</editor-fold>

    //<editor-fold desc="Private tools">
    private void hide(Set<? extends Entity> entities) {
        for (Entity entity : entities) {
            entity.isVisible(false);
        }
    }
    private List<? extends Entity> randomSelection(List<? extends Entity> list, int size) {
        List<Entity> output = new ArrayList();
        Set<Integer> indexes = new HashSet();
        while (indexes.size() < size) {
            indexes.add(Utils.DICE.nextInt(list.size()));
        }
        for (int index : indexes) {
            output.add(list.get(index));
        }
        Collections.shuffle(output);
        return output;
    }
    //</editor-fold>

    //<editor-fold desc="Getters & Setters">
    public boolean isPlayAgain() {
        return playAgain;
    }
    public boolean isClone() {
        return isClone;
    }
    public WinType getWinType() {
        return winType;
    }
    public List<ProgressToken> getProgressTokensToSelect() {
        return progressTokensToSelect;
    }
    public List<Card> getCardsToDestroyToSelect() {
        return cardsToDestroyToSelect;
    }
    public List<Card> getCardsToGetForFreeToSelect() {
        return cardsToGetForFreeToSelect;
    }
    public Match getMatchLogging() {
        return matchLogging;
    }
    public TurnStatus getTurnStatus() {
        return turnStatus;
    }
    public Player getDownPlayer() {
        return downPlayer;
    }
    public Player getUpPlayer() {
        return upPlayer;
    }
    public List<Wonder> getWonders() {
        return wonders;
    }
    public List<Card> getFirstEraCards() {
        return this.firstEraCards;
    }
    public List<Card> getSecondEraCards() {
        return this.secondEraCards;
    }
    public List<Card> getThirdEraCards() {
        return this.thirdEraCards;
    }
    public BoardCardsSimple getTableCars() {
        return tableCars;
    }
    public List<ProgressToken> getProgressTokens() {
        return progressTokens;
    }
    public List<Card> getSelledCards() {
        return selledCards;
    }
    public Player getCurrentPlayer() {
        return currentPlayer;
    }
    public Player getWinner() {
        return winner;
    }
    public int getTurn() {
        return turn;
    }
    public int getWarState() {
        return warState;
    }
    public Era getCurrentEra() {
        return currentEra;
    }
    //</editor-fold>

    //<editor-fold desc="Overrides">
    @Override
    public GameState clone() throws CloneNotSupportedException {
        Player downPlayer = this.downPlayer.clone();
        Player upPlayer = this.upPlayer.clone();
        List<Card> firstEraCards = new ArrayList();
        for (Card card : this.firstEraCards) {
            firstEraCards.add(card.clone());
        }
        List<Card> secondEraCards = new ArrayList();
        for (Card card : this.secondEraCards) {
            secondEraCards.add(card.clone());
        }
        List<Card> thirdEraCards = new ArrayList();
        for (Card card : this.thirdEraCards) {
            thirdEraCards.add(card.clone());
        }
        BoardCardsSimple tableCars = this.tableCars != null ? this.tableCars.clone() : null;
        List<ProgressToken> progressTokens = new ArrayList();
        for (ProgressToken token : this.progressTokens) {
            progressTokens.add(token.clone());
        }
        List<Card> selledCards = new ArrayList();
        for (Card card : this.selledCards) {
            selledCards.add(card.clone());
        }
        Player currentPlayer = this.currentPlayer.isDownPlayer() ? downPlayer : upPlayer;
        Player winner = this.winner == null ? null : (this.winner.isDownPlayer() ? downPlayer : upPlayer);
        WinType winType = this.winType;
        int turn = this.turn;
        TurnStatus turnStatus = this.turnStatus;
        int warState = this.warState;
        Era currentEra = this.currentEra;
        List<Wonder> wonders = new ArrayList();
        for (Wonder wonder : this.wonders) {
            wonders.add(wonder.clone());
        }
        List<Wonder> wondersToSelect = new ArrayList();
        for (Wonder wonder : this.wondersToSelect) {
            wondersToSelect.add(wonder.clone());
        }
        boolean playAgain = this.playAgain;
        boolean[] debtWar = new boolean[]{this.debtWar[0], this.debtWar[1], this.debtWar[2], this.debtWar[3]};
        BoxContent content = this.content;
        try {
            return new GameState(downPlayer, upPlayer, firstEraCards, secondEraCards, thirdEraCards, tableCars,
                    progressTokens, selledCards, currentPlayer, winner, winType, turn, turnStatus, warState, currentEra,
                    wonders, wondersToSelect, playAgain, debtWar, content, this.undo);
        } catch (Exception ex) {
            LOG.error("Error cloning the gameState" + this, ex);
            return null;
        }
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (isClone) {
            sb.append("CLONED ");
        }
        sb.append("GameState:").append("[").append(super.toString()).append("]");
        sb.append(downPlayer);
        if (currentPlayer == downPlayer) sb.append( " [your turn]");
        sb.append(" VS ");
        sb.append(upPlayer);
        if (currentPlayer == upPlayer) sb.append( " [your turn]");
        sb.append("\n");
//        sb.append("First Era Cards:\n");
//        for (Card card : firstEraCards) {
//            sb.append("\t").append(card.toString()).append("\n");
//        }
//        sb.append("Second Era Cards:\n");
//        for (Card card : secondEraCards) {
//            sb.append("\t").append(card.toString()).append("\n");
//        }
//        sb.append("Third Era Cards:\n");
//        for (Card card : thirdEraCards) {
//            sb.append("\t").append(card.toString()).append("\n");
//        }
//        sb.append("Wonders:\n");
//        for (Wonder wonder : wonders) {
//            sb.append("\t").append(wonder.toString()).append("\n");
//        }
        sb.append("ProgressTokens:\n");
        for (ProgressToken token: progressTokens) {
            sb.append("\t").append(token.toString()).append("\n");
        }

//        sb.append("Current Table Cards:\n");
//        if (tableCars != null) {
//            for (Entity entity : tableCars.getFreeCards()) {
//                sb.append("\t").append(entity).append("\n");
//            }
//        }
        sb.append("Current Table Progress Tokens:\n");
        for (Entity entity : progressTokens) {
            sb.append("\t").append(entity).append("\n");
        }
//        sb.append("War Status: ").append(Math.abs(warState));
//        if (warState > 0) sb.append(" for ").append(downPlayer);
//        if (warState < 0) sb.append(" for ").append(upPlayer);
        return sb.toString();
    }

    public List<Wonder> getWondersToSelect() {
        List<Wonder> output = new ArrayList();
        output.addAll(wondersToSelect);
        return output;
    }

    public BoxContent getBoxContent() {
        return this.content;
    }

    public void changeCurrentPlayer() {
        if (currentPlayer.isDownPlayer()) {
            currentPlayer = upPlayer;
        } else {
            currentPlayer = downPlayer;
        }
    }

    public Entity getRealEntity(Entity clone) {
        if (clone instanceof Card) {
            for (Card card : getBoxContent().getFirstEraCards()) {
                if (card.equals(clone)) return card;
            }
            for (Card card : getBoxContent().getSecondEraCards()) {
                if (card.equals(clone)) return card;
            }
            for (Card card : getBoxContent().getThirdEraCards(false)) {
                if (card.equals(clone)) return card;
            }
            for (Card card : getBoxContent().getThirdEraCards(true)) {
                if (card.equals(clone)) return card;
            }
        }
        if (clone instanceof Wonder) {
            for (Wonder wonder : getBoxContent().getWonders()) {
                if (wonder.equals(clone)) return wonder;
            }
        }
        if (clone instanceof ProgressToken) {
            for (ProgressToken token : getBoxContent().getProgressTokens()) {
                if (token.equals(clone)) return token;
            }
        }
        throw new IllegalArgumentException(clone + " not found");
    }

    public GameState getUndo() {
        return undo;
    }
    //</editor-fold>
}
