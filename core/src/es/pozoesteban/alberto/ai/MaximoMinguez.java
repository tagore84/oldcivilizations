package es.pozoesteban.alberto.ai;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.ai.minmax.NegamaxAlgorithm;
import es.pozoesteban.alberto.model.GameState;
import es.pozoesteban.alberto.model.players.Move;
import es.pozoesteban.alberto.model.players.Player;
import es.pozoesteban.alberto.utils.Utils;

import java.util.Date;
import java.util.List;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;

/**
 * 13/11/2018 50vsRandom (15 depth) -> P: 27-8, S: 0-4, M: 2-9
 */
public class MaximoMinguez extends AIPlayer {

    private static Logger LOG = new Logger("MaximoMinguez", LOG_LEVEL);

    private int maxDepth;
    private long timeToThink;



    public MaximoMinguez(boolean down, float[] config) {
        super("MaximoMinguez", down);
        maxDepth = (int) config[0];
        timeToThink = (int)config[1] * 1000L;
    }

    //<editor-fold desc="AI Methods (Experts Rules Implementation">
    @Override
    protected Move specificInitialSelectWonderMove(GameState gameState) {
        return null;
    }
    @Override
    protected Move specificNormalTurnMove(GameState gameState) throws CloneNotSupportedException {
        LOG.debug("Turn " + gameState.getTurn() + " cards: " + gameState.getTableCars().getAllCards().size());
        NegamaxAlgorithm algorithm = new NegamaxAlgorithm(gameState);
        long timeFinishThinking = new Date().getTime() + timeToThink;
        long now = new Date().getTime();
        List<Move> bestMoves =  algorithm.getBestMoves(maxDepth, now + timeToThink);
        LOG.debug("Thinking from " + Utils.timstampToStringDate(now) + " to " + Utils.timstampToStringDate(timeFinishThinking) + " real finish time " + Utils.timstampToStringDate(new Date().getTime()));
        if (bestMoves.isEmpty()) {
            LOG.info("No move to select, moving random...");
            return null;
        }
        Move bestMove = bestMoves.get(0);
        Move move = notClonedMove(gameState, bestMove);
        return move;
    }
    @Override
    protected Move specificSelectProgressTokenMove(GameState gameState) {
        return null;
    }

    @Override
    protected Move specificSelectCardToDestroyMove(GameState gameState) {
        return null;
    }

    @Override
    protected Move specificsSelectCardToGetForFree(GameState gameState) {
        return null;
    }

    @Override
    public Player clone() throws CloneNotSupportedException {
        MaximoMinguez clone = (MaximoMinguez)super.clone();
        clone.maxDepth = maxDepth;
        return clone;
    }
    //</editor-fold>

}
