package es.pozoesteban.alberto.ai;

import es.pozoesteban.alberto.model.BoxContent;
import es.pozoesteban.alberto.model.entities.Card;
import es.pozoesteban.alberto.model.entities.Entity;
import es.pozoesteban.alberto.model.enums.CardColor;
import es.pozoesteban.alberto.model.enums.Resource;
import es.pozoesteban.alberto.model.interfaces.Playable;

import java.util.*;

public class BoxContentAnalyzer {

    private final BoxContent boxContent;

    public static void main(String[] args) {
        BoxContentAnalyzer analyzer = new BoxContentAnalyzer();

        List<Entity> all = analyzer.allPlayables();
        Collections.sort(all);
        for (Entity entity : all) {
            System.out.println(entity);
        }
        System.out.println();

        analyzer.printHistogram(analyzer.costHistogramAllCard(), "AllCards");

        for (CardColor color : CardColor.values()) {
            analyzer.printHistogram(analyzer.costHistogramCardByColor(color), color.name());
        }

        analyzer.printHistogram(analyzer.costHistogramWonders(), "Wonders");
    }

    private List<Entity> allPlayables () {
        List<Entity> all = new ArrayList();
        all.addAll(boxContent.getFirstEraCards());
        all.addAll(boxContent.getSecondEraCards());
        all.addAll(boxContent.getThirdEraCards(true));
        all.addAll(boxContent.getThirdEraCards(false));
        all.addAll(boxContent.getProgressTokens());
        all.addAll(boxContent.getWonders());
        return all;
    }

    private void printHistogram(HashMap<Resource,int[]> histogram, String name) {
        StringBuilder sb = new StringBuilder(name);
        sb.append(":").append(System.getProperty("line.separator"));
        for (Resource resource : histogram.keySet()) {
            sb.append(resource);
            for (int value : histogram.get(resource)) {
                sb.append(";").append(value);
            }
            sb.append(System.getProperty("line.separator"));
        }
        System.out.println(sb.toString());
    }

    public BoxContentAnalyzer() {
        boxContent = BoxContent.build();
    }

    private HashMap<Resource, int[]> costHistogramAllCard() {
        List<Playable> allCards = new ArrayList();
        allCards.addAll(boxContent.getFirstEraCards());
        allCards.addAll(boxContent.getSecondEraCards());
        allCards.addAll(boxContent.getThirdEraCards(true));
        allCards.addAll(boxContent.getThirdEraCards(false));
        return costHistogram(allCards);
    }

    private HashMap<Resource, int[]> costHistogramWonders() {
        List<Playable> wonders = new ArrayList();
        wonders.addAll(boxContent.getWonders());
        return costHistogram(wonders);
    }
    private HashMap<Resource, int[]> costHistogramCardByColor(CardColor color) {
        List<Playable> cards = new ArrayList();
        for (Card card : boxContent.getFirstEraCards()) {
            if (card.getColor() == color) cards.add(card);
        }
        for (Card card : boxContent.getSecondEraCards()) {
            if (card.getColor() == color) cards.add(card);
        }
        for (Card card : boxContent.getThirdEraCards(true)) {
            if (card.getColor() == color) cards.add(card);
        }
        for (Card card : boxContent.getThirdEraCards(false)) {
            if (card.getColor() == color) cards.add(card);
        }
        return costHistogram(cards);
    }

    private HashMap<Resource, int[]> costHistogram(List<Playable> playables) {
        HashMap<Resource, int[]> histogramCost = new HashMap();
        for (Playable card : playables) {
            for (Map.Entry<Resource, Integer> entry : card.getCost().entrySet()) {
                Resource resource = entry.getKey();
                int amount = entry.getValue();
                int[] hist = histogramCost.containsKey(resource) ? histogramCost.get(resource) : new int[8];
                hist[amount-1] = hist[amount-1] + 1;
                histogramCost.put(resource, hist);
            }
        }
        return histogramCost;
    }


}
