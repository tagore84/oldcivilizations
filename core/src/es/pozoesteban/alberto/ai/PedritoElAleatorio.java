package es.pozoesteban.alberto.ai;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.GameApplication;
import es.pozoesteban.alberto.model.GameState;
import es.pozoesteban.alberto.model.entities.Card;
import es.pozoesteban.alberto.model.entities.Entity;
import es.pozoesteban.alberto.model.entities.ProgressToken;
import es.pozoesteban.alberto.model.entities.Wonder;
import es.pozoesteban.alberto.model.enums.Action;
import es.pozoesteban.alberto.model.players.Move;
import es.pozoesteban.alberto.model.players.Player;
import es.pozoesteban.alberto.utils.Utils;

import java.util.*;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;
import static es.pozoesteban.alberto.model.enums.Action.*;

public class PedritoElAleatorio extends AIPlayer {

    private static Logger LOG = new Logger("PedritoElAleatorio", LOG_LEVEL);

    public PedritoElAleatorio(boolean down) {
        super("Pedrito el Aleatorio", down);
    }

    //<editor-fold desc="AI Methods (Experts Rules Implementation">
    @Override
    protected Move specificInitialSelectWonderMove(GameState gameState) {
        return null;
    }
    @Override
    protected Move specificNormalTurnMove(GameState gameState) {
        return null;
    }
    @Override
    protected Move specificSelectProgressTokenMove(GameState gameState) {
        return null;
    }

    @Override
    protected Move specificSelectCardToDestroyMove(GameState gameState) {
        return null;
    }

    @Override
    protected Move specificsSelectCardToGetForFree(GameState gameState) {
        return null;
    }

    @Override
    public Player clone() throws CloneNotSupportedException {
       PedritoElAleatorio clone = (PedritoElAleatorio) super.clone();
       return clone;
    }
    //</editor-fold>
}
