package es.pozoesteban.alberto.ai;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.GameApplication;
import es.pozoesteban.alberto.model.BoxContent;
import es.pozoesteban.alberto.model.GameState;
import es.pozoesteban.alberto.model.entities.*;
import es.pozoesteban.alberto.model.enums.*;
import es.pozoesteban.alberto.model.exceptions.UnknownOrInvalidPosition;
import es.pozoesteban.alberto.model.interfaces.Playable;
import es.pozoesteban.alberto.model.interfaces.ResourceProvider;
import es.pozoesteban.alberto.model.interfaces.War;
import es.pozoesteban.alberto.model.players.Move;
import es.pozoesteban.alberto.model.players.Player;

import java.util.*;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;

public class PoncioElPonderado extends AIPlayer {

    private static Logger LOG = new Logger("PoncioElPonderado", LOG_LEVEL);

    //<editor-fold desc="INPUT_CONFIG: WRITE HERE!">
    private float resourceFactor;
    private float suddenVictoryFactor;
    private float militaryVictoryFactor;
    //</editor-fold>

    //<editor-fold desc="CALCULATED_CONFIG: DON'T TOUCH IT!">
    private float pointsFactor;
    private float finalVictoryFactor;
    private float scientificVictoryFactor;
    //</editor-fold>

    /**
     *
     * @param down
     * @param params
     */
    public PoncioElPonderado(boolean down, float[] params) {
        super("Trillo el Expertillo", down);
        resourceFactor = params[0]; //0.5f;
        suddenVictoryFactor = params[1]; //0.9f;
        militaryVictoryFactor = params[2] * suddenVictoryFactor; //0.1f
        pointsFactor = 1.0f - resourceFactor;
        finalVictoryFactor = 1.0f - suddenVictoryFactor;
        scientificVictoryFactor = 1 - finalVictoryFactor - militaryVictoryFactor;
    }

    //<editor-fold desc="AI Methods (Ponderated Sort Implementation">
    @Override
    protected Move specificInitialSelectWonderMove(GameState gameState) {
        return null;
    }
    @Override
    protected Move specificNormalTurnMove(GameState gameState) throws CloneNotSupportedException {
        List<Move> moves = generateLegalMoves(gameState, isDownPlayer(), true);
        weighMoves(gameState, moves);
        Collections.sort(moves);
        Move move = moves.get(0);
        move.isSimulated(false);
        return move;
    }
    @Override
    protected Move specificSelectProgressTokenMove(GameState gameState) throws CloneNotSupportedException {
        List<Move> moves = generateLegalMoves(gameState, isDownPlayer(), true);
        weighMoves(gameState, moves);
        Collections.sort(moves);
        Move move = moves.get(0);
        move.isSimulated(false);
        return move;
    }
    @Override
    protected Move specificSelectCardToDestroyMove(GameState gameState) throws CloneNotSupportedException {
        List<Move> moves = generateLegalMoves(gameState, isDownPlayer(), true);
        weighMoves(gameState, moves);
        Collections.sort(moves);
        Move move = moves.get(0);
        move.isSimulated(false);
        return move;
    }
    @Override
    protected Move specificsSelectCardToGetForFree(GameState gameState) throws CloneNotSupportedException {
        List<Move> moves = generateLegalMoves(gameState, isDownPlayer(), true);
        weighMoves(gameState, moves);
        Collections.sort(moves);
        Move move = moves.get(0);
        move.isSimulated(false);
        return move;
    }
    //</editor-fold>

    @Override
    public Player clone() throws CloneNotSupportedException {
        PoncioElPonderado clone = (PoncioElPonderado) super.clone();
        clone.resourceFactor = resourceFactor;
        clone.suddenVictoryFactor = suddenVictoryFactor;
        clone.militaryVictoryFactor = militaryVictoryFactor;
        clone.pointsFactor = pointsFactor;
        clone.finalVictoryFactor = finalVictoryFactor;
        clone.scientificVictoryFactor = scientificVictoryFactor;
        return clone;
    }

    private void weighMoves(GameState gameState, List<Move> moves) throws CloneNotSupportedException {
        for (Move move : moves) {
            switch (move.getAction()) {
                case INIT_SELECT_WONDER:
                    weighInitSelectWonder(gameState, move);
                    break;
                case BUILD_WONDER:
                    weighBuildWonder(gameState, move);
                    break;
                case BUILD_CARD:
                    weighBuildCard(gameState, move);
                    break;
                case SELL:
                    weighSellCard(gameState, move);
                    break;
                case SELECT_PROGRESS_TOKEN:
                    weighProgressToken(gameState, move);
                    break;
                case BUILT_CARD_FOR_FREE:
                    weighBuildCard(gameState, move);
                    break;
                case DESTROY_CARD:
                    weighDestroyCard(gameState, move);
                    break;
            }
        }
    }
    //<editor-fold desc="Weigh moves">
    private void weighInitSelectWonder(GameState gameState, Move move) throws CloneNotSupportedException {
        Wonder wonder = (Wonder) move.getEntityChoosed();
        Card tempCard = gameState.getBoxContent().getFirstEraCards().get(0);
        float resourceValue = resourceFactor * calcResourceValue(gameState, new Move(tempCard, Action.BUILD_WONDER, wonder, true));
        float pointValue = pointsFactor * (wonder.getPoints() / 9.0f);
        float suddenVictory = wonder.isPlayAgain() ? suddenVictoryFactor : 0.0f;
        float weight = resourceValue + pointValue + suddenVictory;
        move.setWeight(weight);
    }
    private void weighBuildWonder(GameState gameState, Move move) throws CloneNotSupportedException {
        Card with = (Card) move.getEntityChoosed();
        Wonder wonder = move.getWonderBuilt();
        float resourceValue = resourceFactor * calcResourceValue(gameState, move);
        float pointValue = pointsFactor * (wonder.getPoints() / 9.0f);
        float suddenMilitary = militaryVictoryFactor * (wonder instanceof War ? suddenMiliratyVictory(gameState, wonder) : 0);
        int[] cost = calcMoneyCost(gameState, wonder);
        float costFactor = (float)(getMoney() - cost[0] - cost[1]) / (float)getMoney();
        float weight = resourceValue + pointValue + suddenMilitary;
        weight = weight * costFactor;
        move.setWeight(weight);
    }
    private void weighBuildCard(GameState gameState, Move move) throws CloneNotSupportedException {
        Card card = (Card) move.getEntityChoosed();
        boolean forFree = move.getAction() == Action.BUILT_CARD_FOR_FREE;
        float resourceValue = resourceFactor * calcResourceValue(gameState, move);
        float pointValue = pointsFactor * (card.getPoints() / 7.0f);
        float suddenMilitary = militaryVictoryFactor * (card instanceof War ? suddenMiliratyVictory(gameState, (War) card) : 0);
        float suddenScientificVictory = scientificVictoryFactor * (card instanceof CardGreen ? suddenScientificVicory(gameState, ((CardGreen)card).getScientificSymbol()) : 0);
        float costFactor = 1;
        if (!forFree) {
            int[] cost = calcMoneyCost(gameState, card);
            costFactor = (float)(getMoney() - cost[0] - cost[1]) / (float)getMoney();
        }
        float weight = resourceValue + pointValue + suddenMilitary + suddenScientificVictory;
        LOG.debug(move.getEntityChoosed() + " RESOURCE = " + calcResourceValue(gameState, move) + " POINTS = " + (card.getPoints() / 7.0f) +
                " MILITARY = " + (card instanceof War ? suddenMiliratyVictory(gameState, (War) card) : 0) +
                " SCIENTIFIC = " + (card instanceof CardGreen ? suddenScientificVicory(gameState, ((CardGreen)card).getScientificSymbol()) : 0) +
                " COST = " + costFactor);
        weight = weight * costFactor;
        move.setWeight(weight);
    }
    private void weighSellCard(GameState gameState, Move move) {
        move.setWeight(0f);
    }
    private void weighProgressToken(GameState gameState, Move move) throws CloneNotSupportedException {
        ProgressToken token = (ProgressToken)move.getEntityChoosed();
        float resourceValue = resourceFactor * calcResourceValue(gameState, move);
        float pointValue = pointsFactor * (token.getPoints() / 7.0f);
        float suddenScientificVictory = token.getScientificSymbol() == ScientificSymbol.BALANCE ?
                scientificVictoryFactor * suddenScientificVicory(gameState, token.getScientificSymbol()) : 0f;
        float weight = resourceValue + pointValue + suddenScientificVictory;
        move.setWeight(weight);
    }
    private void weighDestroyCard(GameState gameState, Move move) {
        Card card = (Card)move.getEntityChoosed();
        // ToDo
        move.setWeight(0.5f);
    }
    //</editor-fold>
    //<editor-fold desc="Weigh moves tools">
    private float suddenMiliratyVictory(GameState gameState, War war) {
        int shields = isDownPlayer() ? war.getShields() : (-1 * war.getShields());
        int nextState = shields + gameState.getWarState();
        if (nextState == 19) return Float.MAX_VALUE; // Si se puede ganar así, el resto no importa.
        return shields / gameState.getCurrentEra().getValue();
    }
    private float suddenScientificVicory(GameState gameState, ScientificSymbol s) {
        boolean scientificProgressToken = false;
        for (ProgressToken token : gameState.getProgressTokens()) {
            if (token.getScientificSymbol() != null && token.isSelectable()) scientificProgressToken = true;
        }
        if (!couldScientificVictory(gameState, scientificProgressToken)) return 0;
        if (scientificSymbols.containsKey(s)) {
            return scientificProgressToken ? 1 : 0;
        } else {
            return 1;
        }
    }
    private boolean couldScientificVictory(GameState gameState, boolean scientificProgressToken) {
        int need = 6 - progressTokens.size();
        if (!progressTokens.contains(ScientificSymbol.BALANCE) && scientificProgressToken) need -= 1;
        Player opponent = isDownPlayer() ? gameState.getUpPlayer() : gameState.getDownPlayer();
        switch (gameState.getCurrentEra()) {
            case FIRST:
                return true;
            case SECOND:
                List<Card> playedSecond = new ArrayList();
                playedSecond.addAll(played);
                playedSecond.addAll(opponent.getPlayed());
                playedSecond.addAll(gameState.getSelledCards());
                int inSecondFigure = 4 - countScientificSymbols(Era.SECOND, playedSecond);
                return need <= (2 + inSecondFigure);
            case THIRD:
                List<Card> playedThird = new ArrayList();
                playedThird.addAll(played);
                playedThird.addAll(opponent.getPlayed());
                playedThird.addAll(gameState.getSelledCards());
                int inThirdFigure = 2 - countScientificSymbols(Era.SECOND, playedThird);
                return need <= inThirdFigure;
        }
        return false;
    }
    private int countScientificSymbols(Era era, List<Card> cards) {
        int sum = 0;
        for (Card card : cards) {
            if (card.getEra() == era && card.getColor() == CardColor.GREEN) sum++;
        }
        return sum;
    }
    //</editor-fold>


    //<editor-fold desc="getters">
    public float getResourceFactor() {
        return resourceFactor;
    }
    public float getSuddenVictoryFactor() {
        return suddenVictoryFactor;
    }
    public float getMilitaryVictoryFactor() {
        return militaryVictoryFactor;
    }
    //</editor-fold>

    private float calcResourceValue(GameState gameState, Move move) throws CloneNotSupportedException {
        Playable playable = null;
        if (move.getAction() == Action.BUILD_WONDER && move.getWonderBuilt().getType() != null) {
            playable = move.getWonderBuilt();
        }
        if ((move.getAction() == Action.BUILD_CARD || move.getAction() == Action.BUILT_CARD_FOR_FREE)
                && move.getEntityChoosed() instanceof ResourceProvider) {
            playable = (Playable) move.getEntityChoosed();
        }
        int moneyGets = 0;
        if (move.getAction() == Action.BUILD_WONDER) {
            moneyGets = move.getWonderBuilt().getMoney();
        }
        if ((move.getAction() == Action.BUILD_CARD || move.getAction() == Action.BUILT_CARD_FOR_FREE)
                && move.getEntityChoosed() instanceof CardYellow) {
            CardYellow yellow = (CardYellow) move.getEntityChoosed();
            moneyGets = yellow.getMoney();
            PointsOrMoneyByType moneyByType = yellow.getMoneyByType();
            if (moneyByType != null && moneyByType != PointsOrMoneyByType.NOTHING) {
                int amountYellow = yellow.getMoneyByAmount();
                if (yellow.getMoneyByType() != null && yellow.getMoneyByType() != PointsOrMoneyByType.NOTHING) {
                    int count = 0;
                    switch (moneyByType) {
                        case MAX_BLUE: case MAX_BROWN_AND_GREY: case MAX_GREEN: case MAX_RED: case MAX_MONEY:
                        case MAX_WONDERS: case MAX_YELLOW:
                            count = gameState.countMax(moneyByType);
                            break;
                        case BROWN: case GREY: case PROGRESS_TOKENS: case RED: case WONDERS: case YELLOW:
                            count = count(moneyByType);
                            break;
                    }
                    moneyGets += amountYellow * count;
                }
            }
        }
        if ((move.getAction() == Action.BUILD_CARD || move.getAction() == Action.BUILT_CARD_FOR_FREE)
                && move.getEntityChoosed() instanceof CardPurple) {
            CardPurple purple = (CardPurple) move.getEntityChoosed();
            PointsOrMoneyByType moneyByType = purple.getMoneyByType();
            if (moneyByType != null && moneyByType != PointsOrMoneyByType.NOTHING) {
                int amountYellow = purple.getMoneyByAmount();
                if (purple.getMoneyByType() != null && purple.getMoneyByType() != PointsOrMoneyByType.NOTHING) {
                    int count = 0;
                    switch (moneyByType) {
                        case MAX_BLUE: case MAX_BROWN_AND_GREY: case MAX_GREEN: case MAX_RED: case MAX_MONEY:
                        case MAX_WONDERS: case MAX_YELLOW:
                            count = gameState.countMax(moneyByType);
                            break;
                        case BROWN: case GREY: case PROGRESS_TOKENS: case RED: case WONDERS: case YELLOW:
                            count = count(moneyByType);
                            break;
                    }
                    moneyGets += amountYellow * count;
                }
            }
        }
        if (move.getAction() == Action.SELL) {
            moneyGets = getSellerPrice();
        }
        if (playable == null && moneyGets == 0) return 0f;
        float currentResourceValue = getResourcesValue(gameState);
        float newResourceValue;
        if (playable != null) {
            GameState cloneState = gameState.clone();
            Player clonePlayer = isDownPlayer() ? cloneState.getDownPlayer() : cloneState.getUpPlayer();
            try {
                clonePlayer.play(cloneState, playable);
                newResourceValue = clonePlayer.getResourcesValue(cloneState) + moneyGets;
            } catch (UnknownOrInvalidPosition ex) {
                newResourceValue = currentResourceValue + moneyGets;
                LOG.error("Error simulating in calcResourceValue...", ex);
            }
        } else {
            newResourceValue = currentResourceValue + moneyGets;
        }
        return Math.max(0, Math.min(1, (newResourceValue - currentResourceValue) / 100.0f));
    }

}
