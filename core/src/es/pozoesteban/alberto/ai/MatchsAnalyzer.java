package es.pozoesteban.alberto.ai;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.logging.Match;
import es.pozoesteban.alberto.model.players.Player;
import es.pozoesteban.alberto.utils.Utils;


import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;
import static es.pozoesteban.alberto.utils.Utils.MATCHS_FILE_PATH;

public class MatchsAnalyzer {

    private static Logger LOG = new Logger("MatchsAnalyzer", LOG_LEVEL);

    public static void main(String[] args) {
        HashMap<String, int[]> results = new HashMap();
        File folder = new File(MATCHS_FILE_PATH);
        for (final File fileEntry : folder.listFiles()) {
            StringBuilder sb = new StringBuilder();
            Set<Match> matchs = Utils.readMatchs(fileEntry);
            for (Match match : matchs) {
                Player winner = match.getWinner();
                if (winner instanceof PoncioElPonderado) {
                    float winR = ((PoncioElPonderado) winner).getResourceFactor();
                    float winS = ((PoncioElPonderado) winner).getSuddenVictoryFactor();
                    float winM = ((PoncioElPonderado) winner).getMilitaryVictoryFactor() / ((PoncioElPonderado) winner).getSuddenVictoryFactor();
                    String winKey = "[" + Float.toString(winR) + "|" + Float.toString(winS) + "|" + Float.toString(winM) + "]";
                    int w = results.containsKey(winKey) ? results.get(winKey)[0] : 0;
                    int l = results.containsKey(winKey) ? results.get(winKey)[1] : 0;
                    results.put(winKey, new int[]{w + 1, l});
                }
                Player losser = match.getLosser();
                if (losser instanceof PoncioElPonderado) {
                    float looseR = ((PoncioElPonderado) losser).getResourceFactor();
                    float looseS = ((PoncioElPonderado) losser).getSuddenVictoryFactor();
                    float looseM = ((PoncioElPonderado) losser).getMilitaryVictoryFactor() / ((PoncioElPonderado) losser).getSuddenVictoryFactor();
                    String looseKey = "[" + Float.toString(looseR) + "|" + Float.toString(looseS) + "|" + Float.toString(looseM) + "]";
                    int w = results.containsKey(looseKey) ? results.get(looseKey)[0] : 0;
                    int l = results.containsKey(looseKey) ? results.get(looseKey)[1] : 0;
                    results.put(looseKey, new int[]{w, l + 1});
                }
            }
        }

        for (Map.Entry<String, int[]> entry : results.entrySet()) {
            System.out.println(entry.getKey() + ";" + entry.getValue()[0] + ";" + entry.getValue()[1]);
        }

    }


}
