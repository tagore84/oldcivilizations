package es.pozoesteban.alberto.ai;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.GameApplication;
import es.pozoesteban.alberto.model.BoxContent;
import es.pozoesteban.alberto.model.GameState;
import es.pozoesteban.alberto.model.entities.*;
import es.pozoesteban.alberto.model.enums.Action;
import es.pozoesteban.alberto.model.enums.ProgressTokenState;
import es.pozoesteban.alberto.model.enums.Resource;
import es.pozoesteban.alberto.model.enums.ScientificSymbol;
import es.pozoesteban.alberto.model.interfaces.ResourceProvider;
import es.pozoesteban.alberto.model.interfaces.War;
import es.pozoesteban.alberto.model.players.Move;
import es.pozoesteban.alberto.model.players.Player;

import java.util.*;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;


/**
 * 13/11/2018 50vsRandom -> P: 21-9, S: 9-4, M: 6-1
 */
public class TrilloElExpertillo extends AIPlayer {

    private static Logger LOG = new Logger("TrilloElExpertillo", LOG_LEVEL);

    /**
     * 0  Points Victory
     * 1 Sudden Victory
     */
    private float pointsVsSudden;
    /**
     * 0  Scientific Victory
     * 1 Military Victory
     */
    private float scientificVsMilitary;

    public TrilloElExpertillo(boolean downPlayer, float[] config) {
        super("TrilloElExpertillo", downPlayer);

    }

    //<editor-fold desc="AI Methods (Experts Rules Implementation">
    @Override
    protected Move specificInitialSelectWonderMove(GameState gameState) {
        return null;
    }
    @Override
    protected Move specificNormalTurnMove(GameState gameState) {
        switch (gameState.getCurrentEra()) {
            case FIRST:
                return firstEraNormalTurnMove(gameState, isDownPlayer(), false);
            case SECOND:
                return secondEraNormalTurnMove(gameState, isDownPlayer(), false);
            case THIRD:
                return thirdEraNormalTurnMove(gameState, isDownPlayer(), false);
            default:
                throw new IllegalArgumentException("Normal turn move in " + gameState.getCurrentEra());
        }
    }
    @Override
    protected Move specificSelectProgressTokenMove(GameState gameState) {
        return null;
    }

    @Override
    protected Move specificSelectCardToDestroyMove(GameState gameState) {
        return null;
    }

    @Override
    protected Move specificsSelectCardToGetForFree(GameState gameState) {
        return null;
    }

    @Override
    public Player clone() throws CloneNotSupportedException {
        TrilloElExpertillo clone = (TrilloElExpertillo)super.clone();
        clone.pointsVsSudden = pointsVsSudden;
        clone.scientificSymbols = scientificSymbols;
        return clone;
    }
    //</editor-fold>


    //<editor-fold desc="Expert rule system">
    private Move firstEraNormalTurnMove(GameState gameState, boolean isDownPlayer, boolean simulated) {
        Move resourceProviderMove = getBestResourceProvideOption(gameState, isDownPlayer, simulated);
        LOG.debug("Resource: " + resourceProviderMove);
        Move specialPriceMove = getBestSpecialPriceOption(gameState, isDownPlayer, simulated);
        LOG.debug("SpecialPrice: " + specialPriceMove);
        Move best = bestMove(gameState, resourceProviderMove, specialPriceMove);
        if (best != null) return best;
        return null;
    }
    private Move secondEraNormalTurnMove(GameState gameState, boolean isDownPlayer, boolean simulated) {
        Move resourceProviderMove = getBestResourceProvideOption(gameState, isDownPlayer, simulated);
        LOG.debug("Resource: " + resourceProviderMove);
        Move specialPriceMove = getBestSpecialPriceOption(gameState, isDownPlayer, simulated);
        LOG.debug("SpecialPrice: " + specialPriceMove);
        Move pointsMove = getBestPointsOption(gameState, isDownPlayer, simulated);
        LOG.debug("Points: " + pointsMove);
        Move progressTokenMove = getBestScientificOption(gameState, isDownPlayer, simulated, true);
        LOG.debug("ProgressToken: " + progressTokenMove);
        Move scientificVictoryMove = getBestScientificOption(gameState, isDownPlayer, simulated, false);
        LOG.debug("ScientificVictory: " + scientificVictoryMove);
        Move militaryVictoryMove = getBestMilitaryOption(gameState, isDownPlayer, simulated);
        LOG.debug("MilitaryVictory: " + militaryVictoryMove);
        Move best = bestMove(gameState, resourceProviderMove, specialPriceMove, pointsMove,
                progressTokenMove, scientificVictoryMove, militaryVictoryMove);
        if (best != null) return best;
        return null;
    }

    private Move thirdEraNormalTurnMove(GameState gameState, boolean isDownPlayer, boolean simulated) {
        Move pointsMove = getBestPointsOption(gameState, isDownPlayer, simulated);
        LOG.debug("Points: " + pointsMove);
        Move progressTokenMove = getBestScientificOption(gameState, isDownPlayer, simulated, true);
        LOG.debug("ProgressToken: " + progressTokenMove);
        Move scientificVictoryMove = getBestScientificOption(gameState, isDownPlayer, simulated, false);
        LOG.debug("ScientificVictory: " + scientificVictoryMove);
        Move militaryVictoryMove = getBestMilitaryOption(gameState, isDownPlayer, simulated);
        LOG.debug("MilitaryVictory: " + militaryVictoryMove);
        Move best = bestMove(gameState, pointsMove, progressTokenMove, scientificVictoryMove, militaryVictoryMove);
        if (best != null) return best;
        return null;
    }

    private Move bestMove(GameState gameState, Move... moves) {
        float bestWeight = 0;
        Move bestMove = null;
        for (Move move : moves) {
            if (move != null && move.getWeight() > bestWeight) {
                bestWeight = move.getWeight();
                bestMove = move;
            }
        }
        if (bestMove != null && bestMove.getAction() == Action.BUILD_WONDER) {
            bestMove.setEntityChoosed(bestCardToBuildWonder(gameState));
        }
        return bestMove;
    }

    private Entity bestCardToBuildWonder(GameState gameState) {
        Move bestOpponentMove = null;
        switch (gameState.getCurrentEra()) {
            case FIRST:
                bestOpponentMove = firstEraNormalTurnMove(gameState, !isDownPlayer(), true);
            case SECOND:
                bestOpponentMove = firstEraNormalTurnMove(gameState, !isDownPlayer(), true);
            case THIRD:
                bestOpponentMove = firstEraNormalTurnMove(gameState, !isDownPlayer(), true);
        }
        if (bestOpponentMove != null && bestOpponentMove.getEntityChoosed() != null) return bestOpponentMove.getEntityChoosed();
        return gameState.getTableCars().getFreeCards().iterator().next();
    }

    private Move getBestScientificOption(GameState gameState, boolean isDownPlayer, boolean simulated, boolean progressToken) {
        List<Move> legalMoves = generateLegalMoves(gameState, isDownPlayer, simulated);
        HashMap<Entity, Integer> playables = isDownPlayer == isDownPlayer() ? calcPlayables(gameState) :
                (isDownPlayer ? gameState.getUpPlayer() : gameState.getDownPlayer())
                        .calcPlayables(gameState);
        float bestWeight = 0;
        Move bestMove = null;
        for (Move move : legalMoves) {
            Entity played = move.getWonderBuilt() == null ? move.getEntityChoosed() : move.getWonderBuilt();
            if (played instanceof CardGreen && playables.containsKey(played)) {
                CardGreen green = (CardGreen) played;
                ScientificSymbol symbol = green.getScientificSymbol();
                float weight = 0f;
                if (scientificSymbols.containsKey(symbol) && progressToken) {
                    //Progress Token Way...
                    weight = weightProgressTokens(gameState) - ((float) playables.get(played) / 2f);
                } else if (!progressToken) {
                    //Sudden Victory Way...
                    if (scientificSymbols.size() == 5) {
                        weight = Float.MAX_VALUE;
                    } else {
                        weight = 1f - (((float) playables.get(played) / 2f));
                    }
                }
                if (weight > bestWeight) {
                    bestWeight = weight;
                    bestMove = move.clone();
                    bestMove.setWeight(weight);
                }
            }
        }
        return null;
    }
    private float weightProgressTokens(GameState gameState) {
        List<ProgressToken> tokens = new ArrayList();
        for (ProgressToken token : gameState.getProgressTokens()) {
            if (token.peekState() == ProgressTokenState.ON_BOARD || token.peekState() == ProgressTokenState.SELECTABLE) {
                tokens.add(token);
            }
        }
        float bestWeight = 0;
        for (ProgressToken token : tokens) {
            float weight = 0;
            if (token.getPoints() > 0) {            // ids: 0 y 6
                weight = (token.getPoints() + ((float)token.getMoney() / 3f)) / 8f;
            }
            if (token.getPointsByType() != null) {  // ids: 5
                weight = ((progressTokens.size() + 1 + (tokens.size() * 0.1f)) * 3f) / 8f;
            }
            if (token.isWonderPlayAgain()) {        // ids: 8
                for (Wonder wonder : wonders) {
                    if (!wonder.isBuilt() && !wonder.isPlayAgain()) {
                        weight += 0.25f;
                    }
                }
            }
            // ToDo ids: 1, 2, 3, 4, 7, 9...
            weight = 0.5f;
            if (weight > bestWeight) {
                bestWeight = weight;
            }
        }
        return bestWeight;
    }
    private Move getBestMilitaryOption(GameState gameState, boolean isDownPlayer, boolean simulated) {
        List<Move> legalMoves = generateLegalMoves(gameState, isDownPlayer, simulated);
        HashMap<Entity, Integer> playables = isDownPlayer == isDownPlayer() ? calcPlayables(gameState) :
                (isDownPlayer ? gameState.getUpPlayer() : gameState.getDownPlayer())
                        .calcPlayables(gameState);
        float bestWeight = 0;
        Move bestMove = null;
        for (Move move : legalMoves) {
            Entity played = move.getWonderBuilt() == null ? move.getEntityChoosed() : move.getWonderBuilt();
            if (played instanceof War && playables.containsKey(played)) {
                War war = (War) played;
                float weight = (float) (war.getShields() - ((float) playables.get(played) / 3f)) / 3f;
                if (weight > bestWeight) {
                    bestMove = move.clone();
                    bestWeight = weight;
                    bestMove.setWeight(bestWeight);
                }
            }
        }
        if (bestMove != null) {
            return bestMove;
        }
        return null;
    }
    private Move getBestPointsOption(GameState gameState, boolean isDownPlayer, boolean simulated) {
        List<Move> legalMoves = generateLegalMoves(gameState, isDownPlayer, simulated);
        HashMap<Entity, Integer> playables = isDownPlayer == isDownPlayer() ? calcPlayables(gameState) :
                (isDownPlayer ? gameState.getUpPlayer() : gameState.getDownPlayer())
                        .calcPlayables(gameState);
        float bestWeight = 0;
        Move bestMove = null;
        for (Move move : legalMoves) {
            Entity played = move.getWonderBuilt() == null ? move.getEntityChoosed() : move.getWonderBuilt();
            if (playables.containsKey(played)) {
                float weight = (float) (played.getPoints() - ((float) playables.get(played) / 2f)) / 8f;
                if (played instanceof CardYellow) {
                    CardYellow yellow = (CardYellow) played;
                    int totalMoney = yellow.getMoney();
                    totalMoney += calcMoneyBy(gameState, yellow.getMoneyByType(), yellow.getMoneyByAmount());
                    weight = (float) (totalMoney - (float) playables.get(played) / 2.5f) / 8f;
                }
                if (played instanceof CardPurple) {
                    CardPurple purple = (CardPurple) played;
                    int totalMoney = calcMoneyBy(gameState, purple.getMoneyByType(), purple.getMoneyByAmount());
                    int points = calcPointsBy(gameState, purple.getMoneyByType(), purple.getMoneyByAmount());
                    weight = (float) (points + (totalMoney - (float) playables.get(played) / 2.5f)) / 8f;
                }
                if (weight > bestWeight) {
                    bestMove = move.clone();
                    bestWeight = weight;
                    bestMove.setWeight(bestWeight);
                }
            }
        }
        if (bestMove != null) {
            return bestMove;
        }
        return null;
    }
    private Move getBestResourceProvideOption(GameState gameState, boolean isDownPlayer, boolean simulated) {
        List<Move> legalMoves = generateLegalMoves(gameState, isDownPlayer, simulated);
        List<Resource> resourcesNeeded = Arrays.asList(Resource.values());
        Collections.sort(resourcesNeeded, (o1, o2) -> Integer.compare(calcValue(gameState, o2), calcValue(gameState, o1)));
        HashMap<Entity, Integer> playables = isDownPlayer == isDownPlayer() ? calcPlayables(gameState) :
                (isDownPlayer ? gameState.getUpPlayer() : gameState.getDownPlayer())
                        .calcPlayables(gameState);
        for (Resource resource : resourcesNeeded) {
            for (Move move : legalMoves) {
                Entity played = move.getWonderBuilt() == null ? move.getEntityChoosed() : move.getWonderBuilt();
                if (played instanceof ResourceProvider && playables.containsKey(played)) {
                    ResourceProvider provider = (ResourceProvider) played;
                    if (provider.getType() == resource) {
                        float weight = (float)(calcValue(gameState, resource)-playables.get(played)) / 15f;
                        Move output = move.clone();
                        output.setWeight(weight);
                        return output;
                    }
                }
            }
        }
        return null;
    }
    private Move getBestSpecialPriceOption(GameState gameState, boolean isDownPlayer, boolean simulated) {
        List<Move> legalMoves = generateLegalMoves(gameState, isDownPlayer, simulated);
        List<Resource> resourcesNeeded = Arrays.asList(Resource.values());
        final Player player = this;
        Collections.sort(resourcesNeeded, (o1, o2) -> Integer.compare(calcValue(gameState, o2), calcValue(gameState, o1)));
        HashMap<Entity, Integer> playables = isDownPlayer == isDownPlayer() ? calcPlayables(gameState) :
                (isDownPlayer ? gameState.getUpPlayer() : gameState.getDownPlayer())
                        .calcPlayables(gameState);
        for (Resource resource : resourcesNeeded) {
            for (Move move : legalMoves) {
                Entity played = move.getWonderBuilt() == null ? move.getEntityChoosed() : move.getWonderBuilt();
                if (played instanceof CardYellow && playables.containsKey(played)) {
                    List<Resource> specialPrices = ((CardYellow)played).getSpecialPrice();
                    for (Resource specialPriceResource : specialPrices) {
                        if (specialPriceResource == resource) {
                            float weight = (float)(calcValue(gameState, resource)-playables.get(played)) / 15f;
                            Move output = move.clone();
                            output.setWeight(weight);
                            return output;
                        }
                    }
                }
            }
        }
        return null;
    }
    private int calcValue(GameState gameState, Resource resource) {
        int amount = resources.containsKey(resource) ? resources.get(resource) : 0;
        int price = gameState.getResourcePrice(resource, this);
        int value = 0;
        for (int i = 0; i < 3; i++) {
            if (i >= amount) value += price * (3-i);
        }
        return value;
    }


    private Set<Wonder> sinergy(GameState gameState, Wonder selected) {
        Set<Wonder> output = new HashSet();
        BoxContent all = gameState.getBoxContent();
        switch (selected.getName()) {
            // ToDo
        }
        return output;
    }
    private Set<Wonder> antoSinergy(GameState gameState, Wonder selected) {
        Set<Wonder> output = new HashSet();
        BoxContent all = gameState.getBoxContent();
        switch (selected.getName()) {
            // ToDo
        }
        return output;
    }
    //</editor-fold>



}
