package es.pozoesteban.alberto.ai;

import com.badlogic.gdx.utils.Logger;

import es.pozoesteban.alberto.GameApplication;
import es.pozoesteban.alberto.model.GameState;
import es.pozoesteban.alberto.model.entities.*;
import es.pozoesteban.alberto.model.enums.*;
import es.pozoesteban.alberto.model.exceptions.NotFreeCard;
import es.pozoesteban.alberto.model.exceptions.UnknownOrInvalidPosition;
import es.pozoesteban.alberto.model.players.Move;
import es.pozoesteban.alberto.model.players.Player;
import es.pozoesteban.alberto.utils.Utils;


import java.util.*;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;
import static es.pozoesteban.alberto.model.enums.Action.BUILD_CARD;
import static es.pozoesteban.alberto.model.enums.Action.BUILD_WONDER;
import static es.pozoesteban.alberto.model.enums.Action.SELL;

public abstract class AIPlayer extends Player {

    private static Logger LOG = new Logger("AIPlayer", LOG_LEVEL);

    //<editor-fold desc="Constructs">
    public AIPlayer(String name, boolean down) {
        super(name, down);
    }
    //</editor-fold>


    protected Move notClonedMove(GameState gameState, Move simulatedMove) {
        Entity choose = gameState.getRealEntity(simulatedMove.getEntityChoosed());
        Wonder wonderBuild = simulatedMove.getWonderBuilt() == null ? null : (Wonder) gameState.getRealEntity(simulatedMove.getWonderBuilt());
        return new Move(choose, simulatedMove.getAction(), wonderBuild, false, simulatedMove.getWeight());
    }

    //<editor-fold desc="AI Methods (Sudden Win and Random Implementation">
    public Move chooseMove(GameState gameState) throws CloneNotSupportedException {
        Era era = gameState.getCurrentEra();
        if (era == Era.WONDER_SELECTION) return initialSelectWonderMove(gameState);
        TurnStatus turnStatus = gameState.getTurnStatus();
        switch (turnStatus) {
            case NORMAL_TURN:
                return normalTurnMove(gameState);
            case CHOOSING_PROGRESS_TOKEN:
                return selectProgressTokenMove(gameState);
            case CHOOSING_CARD_TO_DESTROY:
                return selectCardToDestroyMove(gameState);
            case CHOOSING_CARD_TO_GET_FOR_FREE:
                return selectCardToGetForFree(gameState);
        }
        throw new IllegalArgumentException("Unknow state Era = " + era + ", TurnStatus = " + turnStatus);
    }
    protected Move initialSelectWonderMove(GameState gameState) {
        Move specific = specificInitialSelectWonderMove(gameState);
        if (specific != null) return specific;
        List<Wonder> wonders = gameState.getWondersToSelect();
        return new Move(wonders.get(Utils.DICE.nextInt(wonders.size())),
                Action.INIT_SELECT_WONDER, null, false);
    }
    protected abstract Move specificInitialSelectWonderMove(GameState gameState);
    protected Move normalTurnMove(GameState gameState) throws CloneNotSupportedException {
        Move suddenWinMove = suddenWin(gameState);
        if (suddenWinMove != null) return suddenWinMove;
        Move specific = specificNormalTurnMove(gameState);
        if (specific != null) return specific;
        HashMap<Entity, Integer> playables = calcPlayables(gameState);
        if (playables.isEmpty()) {
            List<Card> cards = new ArrayList();
            cards.addAll(gameState.getTableCars().getFreeCards());
            Card toSell = cards.get(Utils.DICE.nextInt(cards.size()));
            return new Move(toSell, SELL, null, false);
        }
        Entity entity = (Entity) playables.keySet().toArray()[Utils.DICE.nextInt(playables.size())];
        if (entity instanceof Wonder) {
            List<Card> cards = new ArrayList();
            cards.addAll(gameState.getTableCars().getFreeCards());
            Card with = cards.get(Utils.DICE.nextInt(cards.size()));
            return new Move(with, BUILD_WONDER, (Wonder) entity, false);
        } else {
            return new Move(entity, BUILD_CARD, null, false);
        }
    }
    protected abstract Move specificNormalTurnMove(GameState gameState) throws CloneNotSupportedException;
    protected Move selectProgressTokenMove(GameState gameState) throws CloneNotSupportedException {
        Move specific = specificSelectProgressTokenMove(gameState);
        if (specific != null) return specific;
        List<ProgressToken> tokens = gameState.getProgressTokensToSelect();
        return new Move(tokens.get(Utils.DICE.nextInt(tokens.size())),
                Action.SELECT_PROGRESS_TOKEN, null, false);
    }
    protected abstract Move specificSelectProgressTokenMove(GameState gameState) throws CloneNotSupportedException;
    protected Move selectCardToDestroyMove(GameState gameState) throws CloneNotSupportedException {
        Move specific = specificSelectCardToDestroyMove(gameState);
        if (specific != null) return specific;
        List<Card> cards = gameState.getCardsToDestroyToSelect();
        return new Move(cards.get(Utils.DICE.nextInt(cards.size())),
                Action.DESTROY_CARD, null, false);
    }
    protected abstract Move specificSelectCardToDestroyMove(GameState gameState) throws CloneNotSupportedException;
    protected Move selectCardToGetForFree(GameState gameState) throws CloneNotSupportedException {
        Move specific = specificsSelectCardToGetForFree(gameState);
        if (specific != null) return specific;
        List<Card> freeCards = gameState.getCardsToGetForFreeToSelect();
        return new Move(freeCards.get(Utils.DICE.nextInt(freeCards.size())),
                Action.BUILT_CARD_FOR_FREE, null, false);
    }
    protected abstract Move specificsSelectCardToGetForFree(GameState gameState) throws CloneNotSupportedException;
    private Move suddenWin(GameState gameState) {
        // I could win...
        try {
            GameState cloneGameState = gameState.clone();
            List<Move> legalMoves = generateLegalMoves(cloneGameState, isDownPlayer(), true);
            for (Move move : legalMoves) {
                if(cloneGameState.play(move)) {
                    LOG.debug("Doing move to WIN!");
                    move.isSimulated(false);
                    return move;
                }
            }

            // Opponent could win...
            cloneGameState = gameState.clone();
            legalMoves = generateLegalMoves(gameState, !isDownPlayer(), true);
            for (Move move : legalMoves) {
                if (gameState.clone().play(move)) {
                    LOG.debug("Doing move to prevent opponent WIN!");
                    move.isSimulated(false);
                    return move;
                }
            }
        } catch (Exception ex) {
            LOG.error("Error checking sudden win", ex);
        }
        return null;
    }
    //</editor-fold>

    //<editor-fold desc="Overrides">
    @Override
    public boolean isAIPlayer() {
        return true;
    }
    @Override
    public void onBegingTurn(GameState gameState) throws UnknownOrInvalidPosition, NotFreeCard, CloneNotSupportedException {
        Move move = chooseMove(gameState);
        move.getEntityChoosed().isMovedByAIPlayer(true);

        LOG.info(name + " playing:" + move.toString() + System.getProperty("line.separator"));

        gameState.play(move);
    }
    @Override
    public Player clone() throws CloneNotSupportedException {
        AIPlayer clone = (AIPlayer)super.clone();
        return clone;
    }
    //</editor-fold>

}
