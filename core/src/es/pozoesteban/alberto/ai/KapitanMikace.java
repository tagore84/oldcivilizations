package es.pozoesteban.alberto.ai;

import es.pozoesteban.alberto.model.GameState;
import es.pozoesteban.alberto.model.enums.Action;
import es.pozoesteban.alberto.model.interfaces.ResourceProvider;
import es.pozoesteban.alberto.model.interfaces.War;
import es.pozoesteban.alberto.model.players.Move;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class KapitanMikace extends AIPlayer{
    public KapitanMikace(boolean down) {
        super("KamiKace", down);
    }

    @Override
    protected Move specificInitialSelectWonderMove(GameState gameState) {
        return null;
    }

    @Override
    protected Move specificNormalTurnMove(GameState gameState) throws CloneNotSupportedException {
        List<Move> moves = generateLegalMoves(gameState, isDownPlayer(), true);
        Collections.sort(moves, new Comparator<Move>() {
            @Override
            public int compare(Move o1, Move o2) {
                int s1 = 0;
                int r1 = 0;
                Action a1 = o1.getAction();
                if (a1 == Action.BUILD_CARD || a1 == Action.BUILT_CARD_FOR_FREE) {
                    if (o1.getEntityChoosed() instanceof War) {
                        s1 = ((War)o1.getEntityChoosed()).getShields();
                    }
                    if (o1.getEntityChoosed() instanceof ResourceProvider) {
                        r1 = 1;
                    }
                }
                if (a1 == Action.BUILD_WONDER) {
                    s1 = o1.getWonderBuilt().getShields();
                }
                int s2 = 0;
                int r2 = 0;
                Action a2 = o2.getAction();
                if (a2 == Action.BUILD_CARD || a2 == Action.BUILT_CARD_FOR_FREE) {
                    if (o2.getEntityChoosed() instanceof War) {
                        s2 = ((War)o2.getEntityChoosed()).getShields();
                    }
                    if (o2.getEntityChoosed() instanceof ResourceProvider) {
                        r2 = 1;
                    }
                }
                if (a2 == Action.BUILD_WONDER) {
                    s2 = o2.getWonderBuilt().getShields();
                }
                if (s1 == s2) {
                       return Integer.compare(r2, r1);
                } else {
                    return Integer.compare(s2, s1);
                }
            }
        });
        if (!moves.isEmpty()) return moves.get(0);
        return null;
    }

    @Override
    protected Move specificSelectProgressTokenMove(GameState gameState) throws CloneNotSupportedException {
        List<Move> moves = generateLegalMoves(gameState, isDownPlayer(), true);
        for (Move move : moves) {
            if (move.getEntityChoosed() instanceof War) return move;
        }
        return null;
    }

    @Override
    protected Move specificSelectCardToDestroyMove(GameState gameState) throws CloneNotSupportedException {
        List<Move> moves = generateLegalMoves(gameState, isDownPlayer(), true);
        for (Move move : moves) {
            if (move.getEntityChoosed() instanceof War) return move;
        }
        return null;
    }

    @Override
    protected Move specificsSelectCardToGetForFree(GameState gameState) throws CloneNotSupportedException {
        List<Move> moves = generateLegalMoves(gameState, isDownPlayer(), true);
        for (Move move : moves) {
            if (move.getEntityChoosed() instanceof War) return move;
        }
        return null;
    }
}
