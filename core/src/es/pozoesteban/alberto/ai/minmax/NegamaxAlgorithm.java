package es.pozoesteban.alberto.ai.minmax;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.ai.minmax.impl.Negamax;
import es.pozoesteban.alberto.model.GameState;
import es.pozoesteban.alberto.model.entities.Entity;
import es.pozoesteban.alberto.model.entities.Wonder;
import es.pozoesteban.alberto.model.enums.*;
import es.pozoesteban.alberto.model.exceptions.NotFreeCard;
import es.pozoesteban.alberto.model.exceptions.UnknownOrInvalidPosition;
import es.pozoesteban.alberto.model.interfaces.Playable;
import es.pozoesteban.alberto.model.players.Move;
import es.pozoesteban.alberto.model.players.Player;
import es.pozoesteban.alberto.utils.Utils;
import es.pozoesteban.alberto.model.entities.Card;

import java.util.*;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;
import static es.pozoesteban.alberto.model.GameState.WAR_SIZE;

public class NegamaxAlgorithm extends Negamax<Move> {

    private static Logger LOG = new Logger("NegamaxAlgorithm", LOG_LEVEL);

    private GameState cloneGameState;

    public NegamaxAlgorithm(GameState gameState) throws CloneNotSupportedException {
        cloneGameState = gameState.clone();
    }


    @Override
    public boolean isOver() {
        return cloneGameState.getCurrentEra() == Era.FINISH;
    }

    @Override
    public boolean makeMove(Move move) throws CloneNotSupportedException {
        LOG.info(cloneGameState.getCurrentPlayer() + " simuling move " + move);
        try {
            move.isSimulated(true);
            LOG.debug("\tSimulating " + move.toString());
            cloneGameState.play(move);
        } catch (Exception ex) {
            LOG.error("Error simulating the move " + move, ex);
        }
        return cloneGameState.isPlayAgain();
    }

    @Override
    public void unmakeMove(Move move) {
        cloneGameState.undoLastMove();
    }

    @Override
    public Iterable<Move> getPossibleMoves() {
        Player player = cloneGameState.getCurrentPlayer();
        List<Move> moves = player.generateLegalMoves(cloneGameState, player.isDownPlayer(), true);
        LOG.info("---" + player.getName() + " has " + moves.size() + " possible moves---");
        return moves;
        /* Poner solo los movimientos que no son vender inposibilita al jugador a quitar cartas utiles para el rival que no pueda sacar
        List<Move> notSellingMoves = new ArrayList();
        for (Move move : moves) {
            if (!cloneGameState.getTableCars().getFreeCards().contains(move.getEntityChoosed())) {
                player.generateLegalMoves(cloneGameState, player.isDownPlayer(), true);
            }
            if (move.getAction() != Action.SELL) {
                notSellingMoves.add(move);
            }
        }
        return notSellingMoves.isEmpty() ? moves : notSellingMoves;
        */
    }

    private float suddenMilitaryVictoryLikelihood() {
        float playerWarState = cloneGameState.getWarState();
        if (!cloneGameState.getCurrentPlayer().isDownPlayer()) playerWarState *= -1;
        int potencialShields = cloneGameState.potencialShields(cloneGameState.getCurrentPlayer().isDownPlayer());
        if ((potencialShields + playerWarState) < 9) return 0f;
        float warVictoryLikelihood = (playerWarState + 9f) / 18f;
        return warVictoryLikelihood;
    }
    private float suddenMilitaryDefeatLikelihood() {
        float playerWarState = cloneGameState.getWarState();
        if (cloneGameState.getCurrentPlayer().isDownPlayer()) playerWarState *= -1;
        int potencialShields = cloneGameState.potencialShields(cloneGameState.getCurrentPlayer().isDownPlayer());
        if ((potencialShields + playerWarState) < 9) return 0f;
        float warVictoryLikelihood = (playerWarState + 9f) / 18f;
        return warVictoryLikelihood;
    }
    private float suddenScientificVictoryLikelihood() {
        Map<ScientificSymbol, Integer> potencialSymbols = cloneGameState.potencialScientificSymbols();
        Map<ScientificSymbol, Integer> currenSymbols = cloneGameState.getCurrentPlayer().getScientificSymbols();
        Set<ScientificSymbol> current = currenSymbols.keySet();
        Set<ScientificSymbol> potencial = potencialSymbols.keySet();
        Set<ScientificSymbol> union = new HashSet();
        union.addAll(current);
        union.addAll(potencial);
        union.addAll(currenSymbols.keySet());

        if (union.size() < 6) return 0; // No way!
        return (float)current.size() / 6f + (potencial.contains(ScientificSymbol.BALANCE) ? (0.75f/6f) : 0f);
    }
    private float suddenScientificDefeatLikelihood() {
        Map<ScientificSymbol, Integer> potencialSymbols = cloneGameState.potencialScientificSymbols();
        Player opponent = cloneGameState.getCurrentPlayer().isDownPlayer() ? cloneGameState.getUpPlayer() : cloneGameState.getDownPlayer();
        Map<ScientificSymbol, Integer> currenSymbols = opponent.getScientificSymbols();
        Set<ScientificSymbol> current = currenSymbols.keySet();
        Set<ScientificSymbol> potencial = potencialSymbols.keySet();
        Set<ScientificSymbol> union = new HashSet();
        union.addAll(current);
        union.addAll(potencial);
        union.addAll(currenSymbols.keySet());

        if (union.size() < 6) return 0; // No way!
        return (float)current.size() / 6f + (potencial.contains(ScientificSymbol.BALANCE) ? (0.75f/6f) : 0f);
    }

    @Override
    public double evaluate() {
        float suddenMilitaryVictoryLikelihood = suddenMilitaryVictoryLikelihood();
        float suddenMilitaryDefeatLikelihood = suddenMilitaryDefeatLikelihood();
        float suddenScientificVictoryLikelihood = suddenScientificVictoryLikelihood();
        float suddenScientificDefeatLikelihood = suddenScientificDefeatLikelihood();

        float resourceAndMoneyPower = calcResourceAndMoneyPower();
        float pointsVictoryLikelihood = calcPointsVictoryLikelihood();
        float gameProgress = calcGameProgress();
        float normalVictoryLikelihood = resourceAndMoneyPower * (1 - gameProgress) + pointsVictoryLikelihood * gameProgress;

        float evaluate = normalVictoryLikelihood;
        float max = Math.max(suddenMilitaryVictoryLikelihood, suddenMilitaryDefeatLikelihood);
        max = Math.max(max, suddenScientificVictoryLikelihood);
        max = Math.max(max, suddenScientificDefeatLikelihood);
        if (suddenMilitaryVictoryLikelihood == max) evaluate = suddenMilitaryVictoryLikelihood;
        if (suddenMilitaryDefeatLikelihood == max) evaluate = 1 - suddenMilitaryDefeatLikelihood;
        if (suddenScientificVictoryLikelihood == max) evaluate = suddenScientificVictoryLikelihood;
        if (suddenScientificDefeatLikelihood == max) evaluate = 1 - suddenScientificDefeatLikelihood;
        LOG.info("Evaluate: " + Utils.doubleToPercentage(evaluate) + " [" +
                "M_V: " + Utils.doubleToPercentage(suddenMilitaryVictoryLikelihood) + ", " +
                "M_D: " + Utils.doubleToPercentage(suddenMilitaryDefeatLikelihood) + ", " +
                "S_V: " + Utils.doubleToPercentage(suddenScientificVictoryLikelihood) + ", " +
                "S_D: " + Utils.doubleToPercentage(suddenScientificDefeatLikelihood) + ", " +
                "R_P: " + Utils.doubleToPercentage(suddenScientificDefeatLikelihood) + ", " +
                "P_V: " + Utils.doubleToPercentage(suddenScientificDefeatLikelihood) + ", " +
                "progress: " + Utils.doubleToPercentage(gameProgress) + ", " +
                "N_V: " + Utils.doubleToPercentage(normalVictoryLikelihood) + "]");
        return evaluate;
    }

    private float calcGameProgress() {
        float turn = cloneGameState.getTurn();
        if (cloneGameState.getCurrentEra() == Era.SECOND) turn += 20;
        if (cloneGameState.getCurrentEra() == Era.THIRD) turn += 40;
        return turn / 60;
    }

    private float calcPointsVictoryLikelihood() {
        Player player = cloneGameState.getCurrentPlayer();
        Player opponent = cloneGameState.getCurrentPlayer().isDownPlayer() ? cloneGameState.getUpPlayer() : cloneGameState.getDownPlayer();
        int ownPoints = player.getFinalPoints(cloneGameState);
        int opponentPoints = opponent.getFinalPoints(cloneGameState);
        return Math.min(1f, Math.max(0f, ((ownPoints - opponentPoints) + 10f) / 20f));
    }
    private float calcResourceAndMoneyPower() {
        Player player = cloneGameState.getCurrentPlayer();
        Player opponent = cloneGameState.getCurrentPlayer().isDownPlayer() ? cloneGameState.getUpPlayer() : cloneGameState.getDownPlayer();

        int resourceValue = 0;
        Resource[] normalResources = new Resource[]{Resource.WOOD, Resource.CLAY, Resource.STONE, Resource.GLASS, Resource.PAPYRUS};
        for (Resource r : normalResources) {
            int ownAmount = Math.min(3, player.getResources().containsKey(r) ? player.getResources().get(r) : 0);
            int opponentAmount = Math.min(3, opponent.getResources().containsKey(r) ? opponent.getResources().get(r) : 0);
            int ownPrice = player.getResourcePrice(cloneGameState, r);
            int opponentPrice = opponent.getResourcePrice(cloneGameState, r);
            resourceValue += (opponentPrice * opponentPrice) * (3 - opponentAmount);
            resourceValue -= (ownPrice * ownPrice) * (3 - ownAmount);
        }
        Resource[] jokers = new Resource[]{Resource.BLUE_JOKER, Resource.BROWN_JOKER, Resource.GREY_JOKER, Resource.GREY_JOKER};
        for (Resource r : jokers) {
            if (player.getResources().containsKey(r)) resourceValue += 5;
            if (opponent.getResources().containsKey(r)) resourceValue -= 5;
        }
        if (player.getResources().containsKey(Resource.MONEY)) resourceValue += player.getResources().get(Resource.MONEY) * player.getResources().get(Resource.MONEY);
        if (opponent.getResources().containsKey(Resource.MONEY)) resourceValue -= opponent.getResources().get(Resource.MONEY) * opponent.getResources().get(Resource.MONEY);

        return Math.min(1f, Math.max(0f, (resourceValue + 10f) / 75f));
    }

    // Usa pre y now... eso no está bien...
    @Deprecated
    public double evaluateOLD() {
        Era era = cloneGameState.getCurrentEra();
        float resourceValue = 0f;
        float resourceDenominator = (era == Era.FIRST ? 6f : 8f);
        float pointsValue = 0f;
        float pointsDenominator = (era == Era.FIRST ? 3f : (era == Era.SECOND ? 5f : 7f));
        float warValue = 0f;
        float warShieldsDenominator = (era == Era.FIRST ? 2f : (era == Era.SECOND ? 4f : 6f));
        float scientificValue = 0f;
        float costAndMoneyValue = 0f;

        if (cloneGameState.getWinner() != null) {
            return 1;
        }

        GameState now = cloneGameState;
        GameState pre = cloneGameState.getUndo();
        if (pre == null) return 0;
        Player nowPlayer = now.getCurrentPlayer();
        Player prePlayer = pre.getCurrentPlayer();

        float prePoints = 0;
        float nowPoints = 0;
        int[] prePointsDisaggregated = prePlayer.getFinalPointsDisaggregated(pre);
        int[] nowPointsDisaggregated = nowPlayer.getFinalPointsDisaggregated(now);
        for (int i = 0; i < 6; i++) { // Blue, Yellow, Green, Purple, Wonders, Tokens, [W-a-r], [M-o-n-e-y]
            prePoints += prePointsDisaggregated[i];
            nowPoints += nowPointsDisaggregated[i];
        }
        pointsValue = (nowPoints - prePoints) / pointsDenominator;

        float preResources = 0;
        float nowResources = 0;
        for (Resource r : Resource.values()) {
            int preAmount = Math.min(3,
                    prePlayer.getResources().containsKey(r) ? prePlayer.getResources().get(r) : 0);
            int prePrice = prePlayer.getResourcePrice(pre, r);
            preResources += (3 - preAmount) * prePrice;

            int nowAmount = Math.min(3,
                    nowPlayer.getResources().containsKey(r) ? nowPlayer.getResources().get(r) : 0);
            int nowPrice = nowPlayer.getResourcePrice(now, r);
            nowResources += (3 - nowAmount) * nowPrice;
        }
        resourceValue = (preResources-nowResources) / resourceDenominator;

        float preWarShields = nowPlayer.isDownPlayer() ?  pre.getWarState() : (-1*pre.getWarState());
        float nowWarShields = nowPlayer.isDownPlayer() ?  now.getWarState() : (-1*now.getWarState());
        float warValueShields = (nowWarShields-preWarShields) / warShieldsDenominator; // 0 - 0.5 -> (0 - 3 shields)
        float preWarPoints = pre.getWarPoints(prePlayer.isDownPlayer());
        float nowWarPoints = now.getWarPoints(nowPlayer.isDownPlayer());
        float warValuePoints = (nowWarPoints-preWarPoints) / 10f; // 0 - 0.5 -> (5 - 10 points)
        warValue = warValueShields + warValuePoints;

        scientificValue = nowPlayer.getScientificSymbols().size() > prePlayer.getScientificSymbols().size() ?
                1 : 0;

        // 0 Muy caro, 1 ni da ni quita, 2 da oro!
        costAndMoneyValue = ((nowPlayer.getMoney() - prePlayer.getMoney()) + 5f) / 5f;

        float finalEvaluate = (resourceValue + pointsValue + warValue + scientificValue + costAndMoneyValue) / 6f;

        Set<Entity> playedList = new HashSet();
        playedList.addAll(nowPlayer.getProgressTokens());
        playedList.removeAll(prePlayer.getProgressTokens());
        playedList.addAll(nowPlayer.getPlayed());
        playedList.removeAll(prePlayer.getPlayed());
        playedList.addAll(nowPlayer.getWonders(true));
        playedList.removeAll(prePlayer.getWonders(true));

        if (!playedList.isEmpty()) {
            Entity played = playedList.iterator().next();
            LOG.debug("Evaluate " + played + "> [" + Utils.doubleToPercentage(finalEvaluate) + "] resource: " +
                    Utils.doubleToPercentage(resourceValue ) + ", points: " + Utils.doubleToPercentage(pointsValue) +
                    ", war: " + Utils.doubleToPercentage(warValue) + ", scientific: " + Utils.doubleToPercentage(scientificValue) +
                    ", costAndMoneyValue: " + Utils.doubleToPercentage(costAndMoneyValue));
        } else {
//            LOG.error("No hay difencia entre pre y now");
            Set<Entity> playedListDEBUG = new HashSet();
            playedListDEBUG.addAll(nowPlayer.getProgressTokens());
            playedListDEBUG.removeAll(prePlayer.getProgressTokens());
            playedListDEBUG.addAll(nowPlayer.getPlayed());
            playedListDEBUG.removeAll(prePlayer.getPlayed());
            playedListDEBUG.addAll(nowPlayer.getWonders(true));
            playedListDEBUG.removeAll(prePlayer.getWonders(true));
        }
        return finalEvaluate;
    }

    @Override
    public double maxEvaluateValue() {
        return 1;
    }

    @Override
    public void next() {
        cloneGameState.changeCurrentPlayer();
    }

    @Override
    public void previous() {
        cloneGameState.changeCurrentPlayer();
    }
}
