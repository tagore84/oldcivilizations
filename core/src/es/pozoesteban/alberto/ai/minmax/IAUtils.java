package es.pozoesteban.alberto.ai.minmax;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class IAUtils {

    private IAUtils() {}

    public static <Move> List<Move> iterableToSortedList(Iterable<Move> iterable) {
        if (iterable instanceof Collection) {
            return new ArrayList((Collection<Move>) iterable);
        }
        List<Move> list = new ArrayList<>();
        for (Move m : iterable) {
            list.add(m);
        }
        return list;
    }

}
