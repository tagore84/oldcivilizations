package es.pozoesteban.alberto;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Logger;

import es.pozoesteban.alberto.logging.Match;
import es.pozoesteban.alberto.model.GameState;
import es.pozoesteban.alberto.model.enums.*;
import es.pozoesteban.alberto.model.exceptions.NotFreeCard;
import es.pozoesteban.alberto.model.exceptions.UnknownOrInvalidPosition;
import es.pozoesteban.alberto.model.interfaces.Playable;
import es.pozoesteban.alberto.model.players.Move;
import es.pozoesteban.alberto.model.players.Player;
import es.pozoesteban.alberto.screens.BoardScreen;
import es.pozoesteban.alberto.screens.GameOverScreen;
import es.pozoesteban.alberto.screens.MenuScreen;
import es.pozoesteban.alberto.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class GameApplication extends Game {

    public static final int LOG_LEVEL = Logger.INFO;

    private GameState gameState;
    private static Logger LOG = new Logger("GameApplication", LOG_LEVEL);

    private MenuScreen menuScreen;
    private BoardScreen boardScreen;
    private GameOverScreen gameOverScreen;

    private Player downPlayer;
    private Player upPlayer;

    private boolean simulatedMatch;
    private boolean severalBatchMatchs;
    private List<Player[]> matchsBatch;
    private int matchIndex;

    private List<Match> matchs;

    public void loadGame(GameState gameState) {
        this.gameState = gameState;
        this.simulatedMatch = gameState.getUpPlayer().isAIPlayer() && gameState.getDownPlayer().isAIPlayer();
        this.severalBatchMatchs = false;
        this.matchs = new ArrayList();
    }
    public void simulatedGame(List<Player[]> matchsBatch) {
        this.matchsBatch = matchsBatch;
        this.simulatedMatch = matchsBatch != null && !matchsBatch.isEmpty();
        this.severalBatchMatchs = matchsBatch != null && matchsBatch.size() > 1;
        this.matchIndex = 0;
        this.matchs = new ArrayList();
    }

    @Override
    public void create() {
        try {
            if (this.gameState != null) { // Loaded Game
                startLoadedGame();
            } else if (simulatedMatch) { // Simulated new Game
                if (matchIndex < matchsBatch.size()) {
                    Player one = matchsBatch.get(matchIndex)[0];
                    Player two = matchsBatch.get(matchIndex)[1];
                    matchIndex++;
                    startGame(one.isDownPlayer() ? one : two, one.isDownPlayer() ? two : one);
                } else {
                    Gdx.app.exit();
                    if (severalBatchMatchs) {
                        Utils.saveMatchs(matchs);
                        LOG.info("Finished batch games... bye!");
                    }
                }
            } else { // Normal user game
                goToMenuScreen();
            }
        } catch (Exception ex) {
            LOG.error("FATAL ERROR INITIALIZING GAME APLICATION...", ex);
        }
    }

    //<editor-fold desc="Match management">
    public void startGame(Player down, Player up) throws UnknownOrInvalidPosition, NotFreeCard, CloneNotSupportedException {
        LOG.info("Starting the game...");
        this.downPlayer = down;
        this.upPlayer = up;
        gameState = new GameState(downPlayer, upPlayer);
        goToBoardScreen();
        onStartTurn();
    }
    public void startLoadedGame() throws UnknownOrInvalidPosition, NotFreeCard, CloneNotSupportedException {
        LOG.info("Starting loaded game...");
        downPlayer = gameState.getDownPlayer();
        upPlayer = gameState.getUpPlayer();
        goToBoardScreen();
        onStartTurn();
    }
    public void endGame() {
        Match matchLogging = gameState.getMatchLogging();
        LOG.info(matchLogging.toString());
        if (simulatedMatch) {
            matchs.add(matchLogging);
            if (severalBatchMatchs && matchs.size() > 29) {
                Utils.saveMatchs(matchs);
                matchs.clear();
            }
            create();
        } else {
            goToGameOverScreen();
        }
    }
    //</editor-fold>

    //<editor-fold desc="Turn management">
    public void onStartTurn() throws UnknownOrInvalidPosition, NotFreeCard, CloneNotSupportedException {
        Player currentPlayer = gameState.getCurrentPlayer();
        if (!currentPlayer.isCloned()) LOG.debug("\t" + currentPlayer + " playing turn " + gameState.getTurn() + " [" + gameState.getCurrentEra() + "]");
        currentPlayer.onBegingTurn(gameState);
    }
    public void onCurrentPlayerEndTurn() throws UnknownOrInvalidPosition, NotFreeCard, CloneNotSupportedException {
        if (gameState.getWinner() == null) {
            gameState.nextPlayer();
//            Utils.saveGame(gameState);
            onStartTurn();
        } else {
            endGame();
        }
    }

    public void humanMoveDo(Move move) throws UnknownOrInvalidPosition, NotFreeCard, CloneNotSupportedException {
        LOG.info("humanMoveDo... move: " + move);
        gameState.play(move);
        onCurrentPlayerEndTurn();
    }
    public boolean isValidMove(Move move) {
        Action action = move.getAction();
        switch (action) {
            case BUILD_WONDER: case BUILD_CARD:
                Playable playable = action == Action.BUILD_WONDER ? move.getWonderBuilt() : (Playable) move.getEntityChoosed();
                int[] moneyCost = gameState.getCurrentPlayer().calcMoneyCost(gameState, playable);
                if (moneyCost[2] == 0 && (moneyCost[0] + moneyCost[1]) > gameState.getCurrentPlayer().getMoney()) return false;
            case SELL:
                if (gameState.getCurrentEra() == Era.WONDER_SELECTION) return false;
                if (gameState.getTurnStatus() != TurnStatus.NORMAL_TURN) return false;
                break;
            case INIT_SELECT_WONDER:
                if (gameState.getCurrentEra() != Era.WONDER_SELECTION) return false;
                break;
            case SELECT_PROGRESS_TOKEN:
                if (gameState.getTurnStatus() != TurnStatus.CHOOSING_PROGRESS_TOKEN) return false;
                break;
        }
        return true;
    }
    //</editor-fold>

    //<editor-fold desc="Screens Navigation">
    public void goToMenuScreen() {
        if (menuScreen == null) menuScreen = new MenuScreen(this);
        setScreen(menuScreen);
    }
    public void goToBoardScreen() {
        if (boardScreen == null) boardScreen = new BoardScreen(this);
        setScreen(boardScreen);
    }
    public void goToGameOverScreen() {
        if (gameOverScreen == null) gameOverScreen = new GameOverScreen(this);
        setScreen(gameOverScreen);
    }
    //</editor-fold>

    //<editor-fold desc="Getters & Setters">
    public GameState getGameState() {
        return gameState;
    }
    public boolean isSimulatedMatch() {
        return simulatedMatch;
    }
    public boolean isSeveralBatchMatchs() {
        return severalBatchMatchs;
    }
    //</editor-fold>


}
