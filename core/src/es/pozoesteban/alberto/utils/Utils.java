package es.pozoesteban.alberto.utils;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.logging.Match;
import es.pozoesteban.alberto.model.GameState;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;

public class Utils {

    public static Random DICE = new Random(1);
    public static final String MATCHS_FILE_PATH = "savedGames\\";
    public static final String MATCHS_FILE_NAME = "matchsSave";

    private static SimpleDateFormat SDF_TIME = new SimpleDateFormat("HH:mm:ss");
    private static DecimalFormat PERCENTAGE = new DecimalFormat("##.#%");
    private static DecimalFormat FORMAT_DOUBLE = new DecimalFormat("#.##");

    private static Logger LOG = new Logger("Utils", LOG_LEVEL);

    public static String doubleToPercentage(double value) {
        return PERCENTAGE.format(value);
    }
    public static String formatDouble(double value) {
        return FORMAT_DOUBLE.format(value);
    }

    public static String timstampToStringDate(long timestamp) {
        return SDF_TIME.format(new Date(timestamp));
    }
    //<editor-fold desc="Disk">
    public static void saveValue(String fileName, int[] values) {
        File file = new File(MATCHS_FILE_PATH + fileName + ".csv");
        String text = "";
        for (int value : values) {
            text += value + ";";
        }
        text += System.getProperty("line.separator");
        try {
            if (!file.exists()) file.createNewFile();
            Files.write(file.toPath(), text.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException ex) {
            LOG.error("Error appending data to " + fileName, ex);
        }
    }

    public static void saveMatchs(List<Match> matchsBatch) {
        try {
            int fileIndex = 0;
            File file = new File(MATCHS_FILE_PATH + MATCHS_FILE_NAME + "_" + fileIndex + ".dat");
            while (file.exists() && file.length() > (1024*1024*3)) {
                file = new File(MATCHS_FILE_PATH + MATCHS_FILE_NAME + "_" + fileIndex++ + ".dat");
            }

            Set<Match> matchSet = new HashSet();
            matchSet.addAll(matchsBatch);
            Set<Match> loaded = readMatchs(file);
            matchSet.addAll(loaded);
            LOG.info("Saving file..." + file.getName());
            FileOutputStream f = new FileOutputStream(file);
            ObjectOutputStream o = new ObjectOutputStream(f);
            o.writeObject(matchSet);
            o.close();
            f.close();
        } catch (Exception e) {
            LOG.error("Error saving matchs...", e);
        }
    }
    public static Set<Match> readMatchs(File file) {
        Set<Match> matchs = new HashSet();
        try {
            if(file.exists() && !file.isDirectory()) {
                FileInputStream fi = new FileInputStream(file);
                ObjectInputStream oi = new ObjectInputStream(fi);
                Object object = oi.readObject();

                matchs.addAll((Set<Match>) object);

                oi.close();
                fi.close();
            }
        } catch (Exception e) {
            LOG.error("Error reading matchs save files...", e);
        }
        return matchs;
    }

    public static void saveGame(GameState gameState) {
        String fileName = "game_" + gameState.hashCode()%1000 + "_" + gameState.getCurrentEra().getValue() + "_" + gameState.getTurn() + ".game";
        try {
            File file = new File(MATCHS_FILE_PATH + fileName);
            FileOutputStream f = new FileOutputStream(file);
            ObjectOutputStream o = new ObjectOutputStream(f);
            o.writeObject(gameState);
            o.close();
            f.close();
            LOG.info("Saved game [" + fileName + "]");
        } catch (Exception ex) {
            LOG.error("Error saving the game...", ex);
        }
    }

    public static GameState loadGame(String fileName) throws IOException, ClassNotFoundException {
        if (!fileName.contains(".game")) fileName = fileName + ".game";
        File file = new File(MATCHS_FILE_PATH + fileName);

        FileInputStream fi = new FileInputStream(file);
        ObjectInputStream oi = new ObjectInputStream(fi);
        Object object = oi.readObject();

        GameState gameState = (GameState) object;
        oi.close();
        fi.close();
        return gameState;
    }
    //</editor-fold>

}
