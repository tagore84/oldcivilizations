package es.pozoesteban.alberto.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;

import java.util.ArrayList;
import java.util.List;

import es.pozoesteban.alberto.GameApplication;
import es.pozoesteban.alberto.actors.*;
import es.pozoesteban.alberto.model.entities.ProgressToken;
import es.pozoesteban.alberto.model.entities.Wonder;
import es.pozoesteban.alberto.model.entities.Card;
import es.pozoesteban.alberto.model.enums.CardPosition;
import es.pozoesteban.alberto.screens.board_layouts.BoardLayout;
import es.pozoesteban.alberto.screens.board_layouts.BoardLayoutFactory;

public class BoardScreen extends BaseScreen {

    public final static BoardLayout LAYOUT = BoardLayoutFactory.get();
    public static final float WIDTH_SCREEN = Gdx.graphics.getWidth();
    public static final float HEIGHT_SCREEN = Gdx.graphics.getHeight();

    private static final String FIRST_ERA_CARDS_PNG = "firstEraCards.png";
    private static final String SECOND_ERA_CARDS_PNG = "secondEraCards.png";
    private static final String THIRD_ERA_CARDS_PNG = "thirdEraCards.png";
    private static final String FIRST_ERA_CARDS_RESUME_PNG = "firstEraCardsResume.png";
    private static final String SECOND_ERA_CARDS_RESUME_PNG = "secondEraCardsResume.png";
    private static final String THIRD_ERA_CARDS_RESUME_PNG = "thirdEraCardsResume.png";
    private static final String WONDERS_COMPLETE_PNG = "wondersComplete.png";
    private static final String WONDERS_RESUME_PNG = "wondersResume.png";
    private static final String PROGRESS_TOKENS_PNG = "progressTokens.png";

    private static final String SELLER_PRICE_PNG = "seller.png";
    private static final String MONEY_PNG = "money.png";

    private static final String BOARD_PNG = "board.png";

    private static final String CUADRICULA_PNG = "cuadricula_550_309.png";

    public static float MOVEMENT_TIME = 0.5f;


    protected List<CardActor> firstEraCards;
    protected List<CardActor> secondEraCards;
    protected List<CardActor> thirdEraCards;
    protected List<WonderActor> wonders;
    protected List<ProgressTokenActor> progressTokens;
    protected BoardActor boardActor;
    protected MoneyActor moneyDownActor;
    protected MoneyActor moneyUpActor;
    protected Texture firstEraCardsTexture;
    protected Texture firstEraCardsResumeTexture;
    protected Texture secondEraCardsTexture;
    protected Texture secondEraCardsResumeTexture;
    protected Texture thirdEraCardsTexture;
    protected Texture thirdEraCardsResumeTexture;
    protected Texture wondersCompleteTexture;
    protected Texture wondersResumeTexture;
    protected Texture progressTokensTexture;
    protected Texture moneyTexture;
    protected Texture sellerTexture;
    protected Texture boardTexture;
    protected Texture cuadricula;
    protected BackgroundActor cuadriculaActor;

    public BoardScreen(GameApplication gameApplication) {
        super(gameApplication);
        boardTexture = new Texture(BOARD_PNG);
        moneyTexture = new Texture(MONEY_PNG);
        sellerTexture = new Texture(SELLER_PRICE_PNG);
        firstEraCardsTexture = new Texture(FIRST_ERA_CARDS_PNG);
        secondEraCardsTexture = new Texture(SECOND_ERA_CARDS_PNG);
        thirdEraCardsTexture = new Texture(THIRD_ERA_CARDS_PNG);
        firstEraCardsResumeTexture = new Texture(FIRST_ERA_CARDS_RESUME_PNG);
        secondEraCardsResumeTexture = new Texture(SECOND_ERA_CARDS_RESUME_PNG);
        thirdEraCardsResumeTexture = new Texture(THIRD_ERA_CARDS_RESUME_PNG);
        wondersCompleteTexture = new Texture(WONDERS_COMPLETE_PNG);
        wondersResumeTexture = new Texture(WONDERS_RESUME_PNG);
        progressTokensTexture = new Texture(PROGRESS_TOKENS_PNG);
        cuadricula = new Texture(CUADRICULA_PNG);
    }

    protected void addActorToStage() {
        Group background = new Group();
        Group boardGroup = new Group();
        Group moneyGroup = new Group();
        Group row0Group = new Group();
        Group row1Group = new Group();
        Group row2Group = new Group();
        Group row3Group = new Group();
        Group row4Group = new Group();
        Group row5Group = new Group();
        Group row6Group = new Group();
        Group wondersGroup = new Group();
        Group progressTokensGroup = new Group();
        // the order is important in the following two lines
        stage.addActor(background);
        stage.addActor(boardGroup);
        stage.addActor(moneyGroup);
        stage.addActor(row6Group);
        stage.addActor(row5Group);
        stage.addActor(row4Group);
        stage.addActor(row3Group);
        stage.addActor(row2Group);
        stage.addActor(row1Group);
        stage.addActor(row0Group);
        stage.addActor(progressTokensGroup);
        stage.addActor(wondersGroup);

        background.addActor(cuadriculaActor);

        boardGroup.addActor(boardActor);

        moneyGroup.addActor(moneyDownActor);
        moneyGroup.addActor(moneyUpActor);

        for (CardActor cardActor : firstEraCards) {
            switch ((CardPosition)cardActor.getEntity().peekPosition()) {
                case ON_BOARD_0: case ON_BOARD_1: case ON_BOARD_2:
                case ON_BOARD_3: case ON_BOARD_4: case ON_BOARD_5:
                    row1Group.addActor(cardActor);
                    break;
                case ON_BOARD_6: case ON_BOARD_7: case ON_BOARD_8:
                case ON_BOARD_9: case ON_BOARD_10:
                    row2Group.addActor(cardActor);
                    break;
                case ON_BOARD_11: case ON_BOARD_12:
                case ON_BOARD_13: case ON_BOARD_14:
                    row3Group.addActor(cardActor);
                    break;
                case ON_BOARD_15: case ON_BOARD_16: case ON_BOARD_17:
                    row4Group.addActor(cardActor);
                    break;
                case ON_BOARD_18: case ON_BOARD_19:
                    row5Group.addActor(cardActor);
                    break;
            }
        }
        for (CardActor cardActor : secondEraCards) {
            switch ((CardPosition)cardActor.getEntity().peekPosition()) {
                case ON_BOARD_0:
                case ON_BOARD_1:
                    row1Group.addActor(cardActor);
                    break;
                case ON_BOARD_2:
                case ON_BOARD_3:
                case ON_BOARD_4:
                    row2Group.addActor(cardActor);
                    break;
                case ON_BOARD_5:
                case ON_BOARD_6:
                case ON_BOARD_7:
                case ON_BOARD_8:
                    row3Group.addActor(cardActor);
                    break;
                case ON_BOARD_9:
                case ON_BOARD_10:
                case ON_BOARD_11:
                case ON_BOARD_12:
                case ON_BOARD_13:
                    row4Group.addActor(cardActor);
                    break;
                case ON_BOARD_14:
                case ON_BOARD_15:
                case ON_BOARD_16:
                case ON_BOARD_17:
                case ON_BOARD_18:
                case ON_BOARD_19:
                    row5Group.addActor(cardActor);
                    break;
            }
        }
        for (CardActor cardActor : thirdEraCards) {
            switch ((CardPosition)cardActor.getEntity().peekPosition()) {
                case ON_BOARD_0: case ON_BOARD_1:
                    row0Group.addActor(cardActor);
                    break;
                case ON_BOARD_2: case ON_BOARD_3: case ON_BOARD_4:
                    row1Group.addActor(cardActor);
                    break;
                case ON_BOARD_5: case ON_BOARD_6:
                case ON_BOARD_7: case ON_BOARD_8:
                    row2Group.addActor(cardActor);
                    break;
                case ON_BOARD_9: case ON_BOARD_10:
                    row3Group.addActor(cardActor);
                    break;
                case ON_BOARD_11: case ON_BOARD_12:
                case ON_BOARD_13: case ON_BOARD_14:
                    row4Group.addActor(cardActor);
                    break;
                case ON_BOARD_15: case ON_BOARD_16: case ON_BOARD_17:
                    row5Group.addActor(cardActor);
                    break;
                case ON_BOARD_18: case ON_BOARD_19:
                    row6Group.addActor(cardActor);
                    break;
            }
        }
        for (WonderActor wonderActor : wonders) {
            wondersGroup.addActor(wonderActor);
        }
        for (ProgressTokenActor progressToken : progressTokens) {
            progressTokensGroup.addActor(progressToken);
        }
    }
    protected void createActors() {
        cuadriculaActor = new BackgroundActor(cuadricula);


        boardActor =  new BoardActor(boardTexture);

        moneyDownActor = new MoneyActor(moneyTexture, sellerTexture, gameApplication.getGameState().getDownPlayer());
        moneyUpActor = new MoneyActor(moneyTexture, sellerTexture, gameApplication.getGameState().getUpPlayer());

        firstEraCards = new ArrayList();
        for (Card card : gameApplication.getGameState().getFirstEraCards()) {
            firstEraCards.add(new CardActor(gameApplication, card, firstEraCardsTexture, firstEraCardsResumeTexture));
        }
        secondEraCards = new ArrayList();
        for (Card card : gameApplication.getGameState().getSecondEraCards()) {
            secondEraCards.add(new CardActor(gameApplication, card, secondEraCardsTexture, secondEraCardsResumeTexture));
        }
        thirdEraCards = new ArrayList();
        for (Card card : gameApplication.getGameState().getThirdEraCards()) {
            thirdEraCards.add(new CardActor(gameApplication, card, thirdEraCardsTexture, thirdEraCardsResumeTexture));
        }

        wonders = new ArrayList();
        for (Wonder wonder : gameApplication.getGameState().getWonders()) {
            wonders.add(new WonderActor(gameApplication, wonder, wondersCompleteTexture, wondersResumeTexture));
        }

        progressTokens = new ArrayList();
        for (ProgressToken token : gameApplication.getGameState().getProgressTokens()) {
            progressTokens.add(new ProgressTokenActor(gameApplication, token, progressTokensTexture));
        }
    }

    @Override
    public void show() {
        super.show();
        stage.setDebugAll(false);

        createActors();
        addActorToStage();
    }

    @Override
    public void hide() {
        super.hide();
    }

    @Override
    public void render(float delta) {
        super.render(delta); // ToDo el tutorial no lo hace, stackoverflow sí, mejora el parpadeo? o hay que quitarlo?
        Gdx.gl.glClearColor(1f, 1f, 1f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act();
        stage.draw();
    }

    @Override
    public void dispose() {
        firstEraCardsTexture.dispose();
        secondEraCardsTexture.dispose();
        thirdEraCardsTexture.dispose();
        firstEraCardsResumeTexture.dispose();
        secondEraCardsResumeTexture.dispose();
        thirdEraCardsResumeTexture.dispose();
        wondersCompleteTexture.dispose();
        wondersResumeTexture.dispose();
        progressTokensTexture.dispose();
        boardTexture.dispose();
        moneyTexture.dispose();
        sellerTexture.dispose();
        cuadricula.dispose();
        stage.dispose();
    }
}
