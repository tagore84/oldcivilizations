package es.pozoesteban.alberto.screens;

import es.pozoesteban.alberto.model.exceptions.UnknownOrInvalidPosition;

public interface BoardScreenPosition {
    public int getIndex() throws UnknownOrInvalidPosition;
}
