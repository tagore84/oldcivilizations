package es.pozoesteban.alberto.screens.board_layouts;

import com.badlogic.gdx.utils.Logger;

import es.pozoesteban.alberto.actors.CardActor;
import es.pozoesteban.alberto.actors.WonderActor;
import es.pozoesteban.alberto.model.entities.Card;
import es.pozoesteban.alberto.model.entities.Entity;
import es.pozoesteban.alberto.model.entities.ProgressToken;
import es.pozoesteban.alberto.model.entities.Wonder;
import es.pozoesteban.alberto.model.enums.Action;
import es.pozoesteban.alberto.model.enums.Era;
import es.pozoesteban.alberto.model.players.Move;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;
import static es.pozoesteban.alberto.screens.BoardScreen.WIDTH_SCREEN;
import static es.pozoesteban.alberto.screens.BoardScreen.HEIGHT_SCREEN;

public class FiveZonesLayout implements BoardLayout {

    private static Logger LOG = new Logger("FiveZonesLayout", LOG_LEVEL);

    private final static float FIGURE_OVERLAPED = 0.75f;

    private final static float[] CARDS_UP = new float[]{0.01f, 0.88f, 0.79f, 0.99f};
    private final static float[] PROGRESS_TOKENS_UP = new float[]{0.88f, 0.99f, 0.79f, 0.99f};
    private final static float[] WONDERS_UP = new float[]{0.01f, 0.99f, 0.70f, 0.78f};

    private final static float[] WONDERS_DOWN = new float[]{0.01f, 0.99f, 0.22f, 0.30f};
    private final static float[] CARDS_DOWN = new float[]{0.01f, 0.88f, 0.01f, 0.21f};
    private final static float[] PROGRESS_TOKENS_DOWN = new float[]{0.88f, 0.99f, 0.01f, 0.21f};

    private final static float[] FIGURE = new float[]{0.01f, 0.74f, 0.31f, 0.69f};
    private final static float[] GRAVEYARD = new float[]{0.75f, 0.99f, 0.31f, 0.69f};
    private final static float[] PROGRESS_TOKENS_ON_BOARD = new float[]{0.87f, 0.98f, 0.35f, 0.65f};

    private final static float[] BOARD = new float[]{0.75f, 0.99f, 0.21f, 0.79f};

    private final static float[] SELECTABLE_ZONE = new float[]{0.01f, 0.75f, 0.30f, 0.70f};


    @Override
    public Move guessMove(boolean isPlayerDown, Entity entity, float[] center) {
        if (entity instanceof Wonder) {
            if (selectedByDown(center) && isPlayerDown) return new Move(entity, Action.INIT_SELECT_WONDER, null, false);
            if (selectedByUp(center) && !isPlayerDown) return new Move(entity, Action.INIT_SELECT_WONDER, null, false);
        } else if (entity instanceof ProgressToken) {
            if (selectedByDown(center) && isPlayerDown) return new Move(entity, Action.SELECT_PROGRESS_TOKEN, null, false);
            if (selectedByUp(center) && !isPlayerDown) return new Move(entity, Action.SELECT_PROGRESS_TOKEN, null, false);
        } else if (entity instanceof Card) {
            if (onGraveyard(center)) return new Move(entity, Action.SELL, null, false);
            if (cardPlayedByDown(center) && isPlayerDown) return new Move(entity, Action.BUILD_CARD, null, false);
            if (cardPlayedByUp(center) && !isPlayerDown) return new Move(entity, Action.BUILD_CARD, null, false);
            int wonderIndex = isPlayerDown ? onWonderDown(center) : onWonderUp(center);
            if (wonderIndex >= 0) return new Move(entity, Action.BUILD_WONDER, wonderIndex, false);
        }
        return null;
    }

    //<editor-fold desc="Sizes">
    @Override
    public float[] wonderCompleteAbsoluteSize() {
        float[] rel = wonderRelativeSize();
        return new float[]{rel[0] * WIDTH_SCREEN, rel[1] * HEIGHT_SCREEN};
    }

    @Override
    public float[] wonderResumeAbsoluteSize() {
        return wonderCompleteAbsoluteSize();
    }

    @Override
    public float[] progressTokensAbsoluteSize() {
        float[] rel = progressTokenRelativeSize();
        return new float[]{rel[0] * HEIGHT_SCREEN, rel[1] * HEIGHT_SCREEN};
    }
    @Override
    public float[] cardCompleteAbsoluteSize() {
        float[] rel = cardCompleteRelativeSize();
        return new float[]{rel[0] * WIDTH_SCREEN, rel[1] * HEIGHT_SCREEN};
    }
    @Override
    public float[] cardResumeAbsoluteSize() {
        float[] rel = cardResumeRelativeSize();
        return new float[]{rel[0] * WIDTH_SCREEN, rel[1] * HEIGHT_SCREEN};
    }
    //</editor-fold>

    //<editor-fold desc="ProgressTokens">
    @Override
    public float[] progressTokenOnBoard(int index) {
        float xLeft = WIDTH_SCREEN * PROGRESS_TOKENS_ON_BOARD[0];
        float xRight = WIDTH_SCREEN * PROGRESS_TOKENS_ON_BOARD[1];
        float yButton = HEIGHT_SCREEN * PROGRESS_TOKENS_ON_BOARD[2];
        float yTop = HEIGHT_SCREEN * PROGRESS_TOKENS_ON_BOARD[3];
        float slotHight = (yTop - yButton) / 5.0f;
        yButton = yButton + (slotHight*index);
        yTop = yButton + (progressTokenRelativeSize()[1] * HEIGHT_SCREEN);
        return new float[]{xLeft, yButton};
    }

    @Override
    public float[] progressTokenSelectable(int index) {
        float center = (SELECTABLE_ZONE[3] - SELECTABLE_ZONE[2]) / 2.0f;
        float sloteWidth = (SELECTABLE_ZONE[1] - SELECTABLE_ZONE[0]) / 5.0f;
        float x0 = (SELECTABLE_ZONE[0] + (sloteWidth * index)) * WIDTH_SCREEN;
        float y0 = (center - (progressTokenRelativeSize()[0] / 2.0f)) * HEIGHT_SCREEN;
        return new float[]{x0, y0};
    }

    @Override
    public float[] progressTokenPlayed(boolean isPlayerDown, int index) {
        float left = isPlayerDown ? PROGRESS_TOKENS_DOWN[0] : PROGRESS_TOKENS_UP[0];
        float right = isPlayerDown ? PROGRESS_TOKENS_DOWN[1] : PROGRESS_TOKENS_UP[1];
        float button = isPlayerDown ? PROGRESS_TOKENS_DOWN[2] : PROGRESS_TOKENS_UP[2];

        float sloteWidth = (right - left) / 5.0f;
        left = left + (sloteWidth * index);
        return new float[]{left * WIDTH_SCREEN, button * HEIGHT_SCREEN};
    }
    //</editor-fold>

    //<editor-fold desc="Wonders">
    @Override
    public float[] wonders(boolean isPlayerDown, int index) {
        float left = isPlayerDown ? WONDERS_DOWN[0] : WONDERS_UP[0];
        float right = isPlayerDown ? WONDERS_DOWN[1] : WONDERS_UP[1];
        float button = isPlayerDown ? WONDERS_DOWN[2] : WONDERS_UP[2];

        float sloteWidth = (right - left) / 4.0f;
        left = left + (sloteWidth * index);

        LOG.debug((isPlayerDown ? "DOWN Index: " : "UP Index: ") + index + " [" + left + ", " + (left + wonderRelativeSize()[0]) + ", " + button + ", " + (button + wonderRelativeSize()[1]) + "] [" + (left * WIDTH_SCREEN) + ", " + (button * HEIGHT_SCREEN) + "]");
        return new float[]{left * WIDTH_SCREEN, button * HEIGHT_SCREEN};
    }

    @Override
    public float[] wonderSelectable(int index) {
        float centerX = SELECTABLE_ZONE[0] + ((SELECTABLE_ZONE[1] - SELECTABLE_ZONE[0]) / 2);
        float centerY = SELECTABLE_ZONE[2] + ((SELECTABLE_ZONE[3] - SELECTABLE_ZONE[2]) / 2);
        float left1 = centerX - wonderRelativeSize()[0] - 0.1f;
        float left2 = centerX + 0.1f;
        float button1 = centerY - wonderRelativeSize()[1] - 0.1f;
        float button2 = centerY + 0.1f;
        switch (index) {
            case 0:
                return new float[]{left1 * WIDTH_SCREEN, button1 * HEIGHT_SCREEN};
            case 1:
                return new float[]{left2 * WIDTH_SCREEN, button1 * HEIGHT_SCREEN};
            case 2:
                return new float[]{left1 * WIDTH_SCREEN, button2 * HEIGHT_SCREEN};
            case 3:
                return new float[]{left2 * WIDTH_SCREEN, button2 * HEIGHT_SCREEN};
        }
        throw new IllegalArgumentException("Incorrect index at wonderSelectable: " + index);
    }
    //</editor-fold>

    //<editor-fold desc="Cards">
    @Override
    public float[] figure(Era era, int index) {
        float horizontalMargin = ((FIGURE[1]-FIGURE[0]) - (6*cardCompleteRelativeSize()[0])) / 5.0f;
        float verticalOverlapped = ((FIGURE[3] - FIGURE[2]) - cardCompleteRelativeSize()[1]) / ((era == Era.THIRD ? 6 : 4)*cardCompleteRelativeSize()[1]);
        float[] centersX6 = new float[6];
        centersX6[2] = FIGURE[0] + ((FIGURE[1]-FIGURE[0])/2.0f) - cardCompleteRelativeSize()[0];
        centersX6[1] = centersX6[2] - cardCompleteRelativeSize()[0] - horizontalMargin;
        centersX6[0] = centersX6[1] - cardCompleteRelativeSize()[0] - horizontalMargin;
        centersX6[3] = centersX6[2] + cardCompleteRelativeSize()[0] + horizontalMargin;
        centersX6[4] = centersX6[3] + cardCompleteRelativeSize()[0] + horizontalMargin;
        centersX6[5] = centersX6[4] + cardCompleteRelativeSize()[0] + horizontalMargin;

        float[] centersX5 = new float[5];
        centersX5[2] = FIGURE[0] + ((FIGURE[1]-FIGURE[0])/2.0f) - (cardCompleteRelativeSize()[0]/2.0f);
        centersX5[1] = centersX5[2] - cardCompleteRelativeSize()[0] - horizontalMargin;
        centersX5[0] = centersX5[1] - cardCompleteRelativeSize()[0] - horizontalMargin;
        centersX5[3] = centersX5[2] + cardCompleteRelativeSize()[0] + horizontalMargin;
        centersX5[4] = centersX5[3] + cardCompleteRelativeSize()[0] + horizontalMargin;

        float[] centerY = new float[7];
        centerY[3] = FIGURE[2] + ((FIGURE[3]-FIGURE[2])/2.0f) - (cardCompleteRelativeSize()[1]/2.0f);
        centerY[2] = centerY[3] - (cardCompleteRelativeSize()[1] * verticalOverlapped);
        centerY[1] = centerY[2] - (cardCompleteRelativeSize()[1] * verticalOverlapped);
        centerY[0] = centerY[1] - (cardCompleteRelativeSize()[1] * verticalOverlapped);
        centerY[4] = centerY[3] + (cardCompleteRelativeSize()[1] * verticalOverlapped);
        centerY[5] = centerY[4] + (cardCompleteRelativeSize()[1] * verticalOverlapped);
        centerY[6] = centerY[5] + (cardCompleteRelativeSize()[1] * verticalOverlapped);

        switch (era) {
            case FIRST:
                return firstEraFigura(centersX6, centersX5, centerY, index);
            case SECOND:
                return secondEraFigura(centersX6, centersX5, centerY, index);
            case THIRD:
                return thirdEraFigura(centersX6, centersX5, centerY, index);
        }
        throw new IllegalArgumentException("Figure of era " + era + " not found");
    }
    @Override
    public float[] playedCard(boolean isPlayerDown, int index) {
        int xIndex = index / 6;
        int yIndex = index % 6;
        return playedCard(isPlayerDown, xIndex, yIndex);
    }
    @Override
    public float[] wonderedCard(boolean isPlayerDown, int index) {
        float[] wonderPos = wonders(isPlayerDown, index);
        float right = wonderPos[1] + 0.02f;
        float centerY = (wonderPos[3] - wonderPos[2]) / 2.0f;
        float button = centerY - (cardCompleteRelativeSize()[1] / 2.0f);
        float top = centerY + (cardCompleteRelativeSize()[1] / 2.0f);
        float left = right - cardCompleteRelativeSize()[0];
        return new float[]{left * WIDTH_SCREEN, right * WIDTH_SCREEN, button * HEIGHT_SCREEN, top * HEIGHT_SCREEN};
    }
    //</editor-fold>

    //<editor-fold desc="Board">
    public float[] boardAbsoluteSize() {
        return new float[]{board()[1]-board()[0], board()[3]-board()[2]};
    }
    public float[] board() {
        return new float[]{BOARD[0]*WIDTH_SCREEN, BOARD[1]*WIDTH_SCREEN, BOARD[2]*HEIGHT_SCREEN, BOARD[3]*HEIGHT_SCREEN};
    }
    //</editor-fold>

    //<editor-fold desc="Money">
    public float[] moneyAbsoluteSize() {
        return null;//ToDo
    }
    public float[] money(boolean isPlayerDown) {
        return null;//ToDo
    }
    //</editor-fold>

    //<editor-fold desc="Money">
    public float[] graveyardAbsoluteSize() {
        return null;//ToDo
    }
    public float[] graveyard() {
        return null;//ToDo
    }
    //</editor-fold>

    //<editor-fold desc="Private tools">
    private float[] playedCard(boolean isPlayerDown, int xIndex, int yIndex) {
        float left = isPlayerDown ? CARDS_DOWN[0] : CARDS_UP[0];
        float right = isPlayerDown ? CARDS_DOWN[1] : CARDS_UP[1];
        float sloteWidth = (right - left) / 5.0f;
        float x = left + (sloteWidth * xIndex);
        float baseY = isPlayerDown ? CARDS_DOWN[3] : CARDS_UP[2];
        float sloteHeight = isPlayerDown ? (CARDS_DOWN[3] - CARDS_DOWN[2]) / 6.0f : (CARDS_UP[3] - CARDS_UP[2]) / 6.0f;
        if (isPlayerDown) {
            float top = baseY - (sloteHeight * yIndex);
            float button = top - cardResumeRelativeSize()[1];
            LOG.debug("DOWN xIndex: " + xIndex + ", yIndex: " + yIndex + " => (" + x + ", " + button + ")");
            return new float[] {x * WIDTH_SCREEN, button * HEIGHT_SCREEN};
        } else {
            float button = baseY + (sloteHeight * yIndex);
            LOG.debug("UP xIndex: " + xIndex + ", yIndex: " + yIndex + " => (" + x + ", " + button + ")");
            return new float[] {x * WIDTH_SCREEN, button * HEIGHT_SCREEN};
        }
    }
    private float[] firstEraFigura(float[] centersX6, float[] centersX5, float[] centerY, int index) {
        float x, y;
        if (index < 6){
            x = centersX6[index];
            y = centerY[1];
        } else if (index < 11){
            x = centersX5[index-6];
            y = centerY[2];
        } else if (index < 15) {
            x = centersX6[index-10];
            y = centerY[3];
        } else if (index < 18) {
            x = centersX5[index - 14];
            y = centerY[4];
        } else {
            x = centersX6[index - 16];
            y = centerY[5];
        }
        LOG.debug("Index: " + index + " [" + x + ", " + (x + cardCompleteRelativeSize()[0]) + ", " + y + ", " + (y + cardCompleteRelativeSize()[1]) + "] [" + (x * WIDTH_SCREEN) + ", " + (y * HEIGHT_SCREEN) + "]");
        return new float[]{x * WIDTH_SCREEN, y * HEIGHT_SCREEN};
    }
    private float[] secondEraFigura(float[] centersX6, float[] centersX5, float[] centerY, int index) {
        float x, y;
        if (index < 2){
            x = centersX6[2 + index];
            y = centerY[1];
        } else if (index < 5){
            x = centersX5[index-1];
            y = centerY[2];
        } else if (index < 9) {
            x = centersX6[index-4];
            y = centerY[3];
        } else if (index < 14) {
            x = centersX5[index - 9];
            y = centerY[4];
        } else {
            x = centersX6[index - 14];
            y = centerY[5];
        }
        LOG.debug("Index: " + index + " [" + x + ", " + (x + cardCompleteRelativeSize()[0]) + ", " + y + ", " + (y + cardCompleteRelativeSize()[1]) + "] [" + (x * WIDTH_SCREEN) + ", " + (y * HEIGHT_SCREEN) + "]");
        return new float[]{x * WIDTH_SCREEN, y * HEIGHT_SCREEN};
    }
    private float[] thirdEraFigura(float[] centersX6, float[] centersX5, float[] centerY, int index) {
        float x, y;
        if (index < 2){
            x = centersX6[2 + index];
            y = centerY[0];
        } else if (index < 5){
            x = centersX5[index-1];
            y = centerY[1];
        } else if (index < 9) {
            x = centersX6[index-4];
            y = centerY[2];
        } else if (index == 9) {
            x = centersX5[1];
            y = centerY[3];
        } else if (index == 10) {
            x = centersX5[3];
            y = centerY[3];
        } else if(index < 15) {
            x = centersX6[index-10];
            y = centerY[4];
        } else if(index < 18) {
            x = centersX5[index-14];
            y = centerY[5];
        } else {
            x = centersX6[index-16];
            y = centerY[6];
        }
        LOG.debug("Index: " + index + " [" + x + ", " + (x + cardCompleteRelativeSize()[0]) + ", " + y + ", " + (y + cardCompleteRelativeSize()[1]) + "] [" + (x * WIDTH_SCREEN) + ", " + (y * HEIGHT_SCREEN) + "]");
        return new float[]{x * WIDTH_SCREEN, y * HEIGHT_SCREEN};
    }
    private float[] progressTokenRelativeSize() {
        float x = Math.min((PROGRESS_TOKENS_UP[1]-PROGRESS_TOKENS_UP[0]), (PROGRESS_TOKENS_UP[3]-PROGRESS_TOKENS_UP[2]));
        x = Math.min(x, (PROGRESS_TOKENS_ON_BOARD[1]-PROGRESS_TOKENS_ON_BOARD[0]));
        x = Math.min(x, ((PROGRESS_TOKENS_ON_BOARD[3]-PROGRESS_TOKENS_ON_BOARD[2])/5.0f));
        return new float[]{x, x}; // Son cuadrados (redondos)
    }
    private float[] wonderRelativeSize() {
        float x = ((WONDERS_DOWN[1] - WONDERS_DOWN[0]) / 4.0f) * 0.99f;
        float y = (WONDERS_DOWN[3] - WONDERS_DOWN[2]) * 0.99f;
        float xy = x / y;
        float ab = (float)WonderActor.REGION_COMPLETE_WIDTH / (float)WonderActor.REGION_COMPLETE_HEIGHT;
        if (xy > ab) {
            x = y * ab / (WIDTH_SCREEN/HEIGHT_SCREEN);
        } else if (xy < ab){
            y = x / (ab / (WIDTH_SCREEN/HEIGHT_SCREEN));
        }
        return new float[]{x, y};
    }
    private float[] cardCompleteRelativeSize() {
        float x = (FIGURE[1] - FIGURE[0]) / 6.0f * 0.98f;
        float y = (FIGURE[3] - FIGURE[2]) / (7.0f * (1 - FIGURE_OVERLAPED));
        float xy = x / y;
        float ab = (float)CardActor.REGION_COMPLETE_WIDTH / (float)CardActor.REGION_COMPLETE_HEIGHT;
        if (xy > ab) {
            x = y * ab / (WIDTH_SCREEN/HEIGHT_SCREEN);
        } else if (xy < ab){
            y = x / (ab / (WIDTH_SCREEN/HEIGHT_SCREEN));
        }
        return new float[]{x, y};
    }
    private float[] cardResumeRelativeSize() {
        float x = (CARDS_DOWN[1]-CARDS_DOWN[0]) * 0.99f / 5.0f;
        float y = (CARDS_DOWN[3]-CARDS_DOWN[2]) / 6.f;
        float xy = x / y;
        float ab = (float)CardActor.REGION_RESUME_WIDTH / (float)CardActor.REGION_RESUME_HEIGHT;
        if (xy > ab) {
            x = y * ab / (WIDTH_SCREEN/HEIGHT_SCREEN);
        } else if (xy < ab){
            y = x / (ab / (WIDTH_SCREEN/HEIGHT_SCREEN));
        }
        return new float[]{x, y};
    }

    private int onWonderUp(float[] center) {
        if (in(center, WONDERS_UP)) {
            float x = center[0];
            if (x < (WIDTH_SCREEN / 4.0f)) return 0;
            if (x < (WIDTH_SCREEN / 2.0f)) return 1;
            if (x < (WIDTH_SCREEN / 0.75f)) return 2;
            return 3;
        }
        return -1;
    }
    private int onWonderDown(float[] center) {
        if (in(center, WONDERS_DOWN)) {
            float x = center[0];
            if (x < (WIDTH_SCREEN / 4.0f)) return 0;
            if (x < (WIDTH_SCREEN / 2.0f)) return 1;
            if (x < (WIDTH_SCREEN / 0.75f)) return 2;
            return 3;
        }
        return -1;
    }
    private boolean cardPlayedByDown(float[] center) {
        return in(center, CARDS_DOWN);
    }
    private boolean cardPlayedByUp(float[] center) {
        return in(center, CARDS_UP);
    }
    private boolean selectedByDown(float[] center) {
        return in(center, new float[]{WONDERS_DOWN[0], WONDERS_DOWN[1], CARDS_DOWN[2], WONDERS_DOWN[3]});
    }
    private boolean selectedByUp(float[] center) {
        return in(center, new float[]{WONDERS_DOWN[0], WONDERS_DOWN[1], WONDERS_DOWN[2], CARDS_UP[3]});
    }
    private boolean onGraveyard(float[] center) {
        return in(center, GRAVEYARD);
    }
    private boolean in(float[] centerXYAbs, float[] rangeXXYYRelative) {
        float xMin = rangeXXYYRelative[0] * WIDTH_SCREEN;
        float xMax = rangeXXYYRelative[1] * WIDTH_SCREEN;
        float yMin = rangeXXYYRelative[2] * HEIGHT_SCREEN;
        float yMax = rangeXXYYRelative[3] * HEIGHT_SCREEN;
        return centerXYAbs[0] >= xMin &&
                centerXYAbs[0] <= xMax &&
                centerXYAbs[1] >= yMin &&
                centerXYAbs[1] <= yMax;
    }
    //</editor-fold>

}
