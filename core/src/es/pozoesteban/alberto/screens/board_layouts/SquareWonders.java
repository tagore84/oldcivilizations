package es.pozoesteban.alberto.screens.board_layouts;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.actors.CardActor;
import es.pozoesteban.alberto.actors.MoneyActor;
import es.pozoesteban.alberto.actors.WonderActor;
import es.pozoesteban.alberto.model.entities.Card;
import es.pozoesteban.alberto.model.entities.Entity;
import es.pozoesteban.alberto.model.entities.ProgressToken;
import es.pozoesteban.alberto.model.entities.Wonder;
import es.pozoesteban.alberto.model.enums.Action;
import es.pozoesteban.alberto.model.enums.Era;
import es.pozoesteban.alberto.model.players.Move;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;
import static es.pozoesteban.alberto.screens.BoardScreen.HEIGHT_SCREEN;
import static es.pozoesteban.alberto.screens.BoardScreen.WIDTH_SCREEN;

public class SquareWonders implements BoardLayout {



    private static Logger LOG = new Logger("SquareWonders", LOG_LEVEL);

    // ToDo hay Que darle la vuelta al tablero!

    private final static float FIGURE_OVERLAPED = 0.75f;

    private final static float X_MARGIN = 0.005f;
    private final static float Y_MARGIN = 0.005f;


    private static final float SIZE_FACTOR = 0.995f;
    private static final float CARDS_BY_ROW = 4.0f;
    private static final float CARDS_BY_COLUMN = 8.0f;

    private final static float X_LEFT = 0f;
    private final static float X_W_RIGHT_C_LEFT = 0.42f;
    private final static float X_F_RIGHT_B_LEFT = 0.72f;
    private final static float X_PT_BOARD_LEFT = 0.74f;
    private final static float X_PT_BOARD_RIGHT = 0.79f;
    private final static float X_MONEY_LEFT = 0.70f;
    private final static float X_MONEY_RIGHT = 0.81f;
    private final static float X_RIGHT = 1f;


    private final static float Y_DOWN_BUTTON = 0f;
    private final static float Y_BOARD_BUTTON = 0.23f; // ToDo
    private final static float Y_DOWN_TOP = 0.33f;
    private final static float Y_MONEY_DOWN_BUTTON = 0.28f;
    private final static float Y_MONEY_DOWN_TOP = 0.36f;
    private final static float Y_PT_BOARD_BUTTON = 0.38f; // ToDo
    private final static float Y_PT_DOWN_TOP = 0.40f; // ToDo
    private final static float Y_PT_UP_DOWN = 0.60f; // ToDo
    private final static float Y_PT_BOARD_TOP = 0.65f; // ToDo
    private final static float Y_UP_BUTTON = 0.67f;
    private final static float Y_BOARD_TOP = 0.77f; // ToDo
    private final static float Y_MONEY_UP_BUTTON = 0.65f;
    private final static float Y_MONEY_UP_TOP = 0.72f;
    private final static float Y_UP_TOP = 1f;


    private final static float[] WONDERS_DOWN = new float[]{X_LEFT, X_W_RIGHT_C_LEFT, Y_DOWN_BUTTON, Y_DOWN_TOP};
    private final static float[] CARDS_DOWN = new float[]{X_W_RIGHT_C_LEFT, X_RIGHT, Y_DOWN_BUTTON, Y_DOWN_TOP};
    private final static float[] PROGRESS_TOKENS_DOWN = new float[]{X_LEFT, X_RIGHT, Y_DOWN_TOP, Y_PT_DOWN_TOP};

    private final static float[] WONDERS_UP = new float[]{X_LEFT, X_W_RIGHT_C_LEFT, Y_UP_BUTTON, Y_UP_TOP};
    private final static float[] CARDS_UP = new float[]{X_W_RIGHT_C_LEFT, X_RIGHT, Y_UP_BUTTON, Y_UP_TOP};
    private final static float[] PROGRESS_TOKENS_UP = new float[]{X_LEFT, X_RIGHT, Y_PT_UP_DOWN, Y_UP_BUTTON};

    private final static float[] MONEY_DOWN = new float[]{X_MONEY_LEFT, X_MONEY_RIGHT, Y_MONEY_DOWN_BUTTON, Y_MONEY_DOWN_TOP};
    private final static float[] MONEY_UP = new float[]{X_MONEY_LEFT, X_MONEY_RIGHT, Y_MONEY_UP_BUTTON, Y_MONEY_UP_TOP};


    private final static float[] FIGURE = new float[]{X_LEFT, X_F_RIGHT_B_LEFT, Y_DOWN_TOP, Y_UP_BUTTON};
    private final static float[] BOARD = new float[]{X_F_RIGHT_B_LEFT, X_RIGHT, Y_BOARD_BUTTON, Y_BOARD_TOP};
    private final static float[] PROGRESS_TOKENS_ON_BOARD = new float[]{X_PT_BOARD_LEFT, X_PT_BOARD_RIGHT, Y_PT_BOARD_BUTTON, Y_PT_BOARD_TOP};

    private final static float[] SELECTABLE_ZONE = new float[]{X_LEFT, X_F_RIGHT_B_LEFT, Y_DOWN_TOP, Y_UP_BUTTON};

    public Move guessMove(boolean isPlayerDown, Entity entity, float[] center) {
        if (entity instanceof Wonder) {
            if (selectedByDown(center) && isPlayerDown) return new Move(entity, Action.INIT_SELECT_WONDER, null, false);
            if (selectedByUp(center) && !isPlayerDown) return new Move(entity, Action.INIT_SELECT_WONDER, null, false);
        } else if (entity instanceof ProgressToken) {
            if (selectedByDown(center) && isPlayerDown) return new Move(entity, Action.SELECT_PROGRESS_TOKEN, null, false);
            if (selectedByUp(center) && !isPlayerDown) return new Move(entity, Action.SELECT_PROGRESS_TOKEN, null, false);
        } else if (entity instanceof Card) {
            if (onMoneyDown(center) && isPlayerDown) return new Move(entity, Action.SELL, null, false);
            if (onMoneyUp(center) && !isPlayerDown) return new Move(entity, Action.SELL, null, false);
            if (cardPlayedByDown(center) && isPlayerDown) return new Move(entity, Action.BUILD_CARD, null, false);
            if (cardPlayedByUp(center) && !isPlayerDown) return new Move(entity, Action.BUILD_CARD, null, false);
            int wonderIndex = isPlayerDown ? onWonderDown(center) : onWonderUp(center);
            if (wonderIndex >= 0) return new Move(entity, Action.BUILD_WONDER, wonderIndex, false);
        }
        return null;
    }

    //<editor-fold desc="Sizes">
    @Override
    public float[] wonderCompleteAbsoluteSize() {
        float[] rel = wonderCompleteRelativeSize();
        return new float[]{rel[0] * WIDTH_SCREEN, rel[1] * HEIGHT_SCREEN};
    }
    public float[] wonderResumeAbsoluteSize() {
        float[] rel = wonderResumeRelativeSize();
        return new float[]{rel[0] * WIDTH_SCREEN, rel[1] * HEIGHT_SCREEN};
    }
    @Override
    public float[] progressTokensAbsoluteSize() {
        float[] rel = progressTokenRelativeSize();
        return new float[]{rel[0] * HEIGHT_SCREEN, rel[1] * HEIGHT_SCREEN};
    }
    @Override
    public float[] cardCompleteAbsoluteSize() {
        float[] rel = cardCompleteRelativeSize();
        return new float[]{rel[0] * WIDTH_SCREEN, rel[1] * HEIGHT_SCREEN};
    }
    @Override
    public float[] cardResumeAbsoluteSize() {
        float[] rel = cardResumeRelativeSize();
        return new float[]{rel[0] * WIDTH_SCREEN, rel[1] * HEIGHT_SCREEN};
    }
    @Override
    public float[] boardAbsoluteSize() {
        float[] rel = boardRelativeSize();
        return new float[]{rel[0] * WIDTH_SCREEN, rel[1] * HEIGHT_SCREEN};
    }
    @Override
    public float[] moneyAbsoluteSize() {
        float[] rel = moneyRelativeSize();
        return new float[]{rel[0] * WIDTH_SCREEN, rel[1] * HEIGHT_SCREEN};
    }
    //</editor-fold>

    //<editor-fold desc="ProgressTokens">
    @Override
    public float[] progressTokenOnBoard(int index) {
        float xLeft = WIDTH_SCREEN * PROGRESS_TOKENS_ON_BOARD[0];
        float yButton = HEIGHT_SCREEN * PROGRESS_TOKENS_ON_BOARD[2];
        float yTop = HEIGHT_SCREEN * PROGRESS_TOKENS_ON_BOARD[3];
        float slotHight = (yTop - yButton) / 5.0f;
        yButton = yButton + (slotHight*index);
        yTop = yButton + (progressTokenRelativeSize()[1] * HEIGHT_SCREEN);
        return new float[]{xLeft, yButton};
    }

    @Override
    public float[] progressTokenSelectable(int index) {
        float center = (SELECTABLE_ZONE[3] - SELECTABLE_ZONE[2]) / 2.0f;
        float sloteWidth = (SELECTABLE_ZONE[1] - SELECTABLE_ZONE[0]) / 5.0f;
        float x0 = (SELECTABLE_ZONE[0] + (sloteWidth * index)) * WIDTH_SCREEN;
        float y0 = (center - (progressTokenRelativeSize()[0] / 2.0f)) * HEIGHT_SCREEN;
        return new float[]{x0, y0};
    }

    @Override
    public float[] progressTokenPlayed(boolean isPlayerDown, int index) {
        float left = isPlayerDown ? PROGRESS_TOKENS_DOWN[0] : PROGRESS_TOKENS_UP[0];
        left += X_MARGIN;
        float right = isPlayerDown ? PROGRESS_TOKENS_DOWN[1] : PROGRESS_TOKENS_UP[1];
        right -= X_MARGIN;
        float button = isPlayerDown ? PROGRESS_TOKENS_DOWN[2] : PROGRESS_TOKENS_UP[2];
        button += Y_MARGIN;

        float sloteWidth = (right - left) / 5.0f;
        left = left + (sloteWidth * index);
        return new float[]{left * WIDTH_SCREEN, button * HEIGHT_SCREEN};
    }
    //</editor-fold>

    //<editor-fold desc="Cards">
    @Override
    public float[] figure(Era era, int index) {
        float horizontalMargin = ((FIGURE[1]-FIGURE[0]) - (6*cardCompleteRelativeSize()[0])) / 5.0f;
        float verticalOverlapped = ((FIGURE[3] - FIGURE[2]) - cardCompleteRelativeSize()[1]) / ((era == Era.THIRD ? 6 : 4)*cardCompleteRelativeSize()[1]);
        float[] centersX6 = new float[6];
        centersX6[2] = FIGURE[0] + ((FIGURE[1]-FIGURE[0])/2.0f) - cardCompleteRelativeSize()[0];
        centersX6[1] = centersX6[2] - cardCompleteRelativeSize()[0] - horizontalMargin;
        centersX6[0] = centersX6[1] - cardCompleteRelativeSize()[0] - horizontalMargin;
        centersX6[3] = centersX6[2] + cardCompleteRelativeSize()[0] + horizontalMargin;
        centersX6[4] = centersX6[3] + cardCompleteRelativeSize()[0] + horizontalMargin;
        centersX6[5] = centersX6[4] + cardCompleteRelativeSize()[0] + horizontalMargin;

        float[] centersX5 = new float[5];
        centersX5[2] = FIGURE[0] + ((FIGURE[1]-FIGURE[0])/2.0f) - (cardCompleteRelativeSize()[0]/2.0f);
        centersX5[1] = centersX5[2] - cardCompleteRelativeSize()[0] - horizontalMargin;
        centersX5[0] = centersX5[1] - cardCompleteRelativeSize()[0] - horizontalMargin;
        centersX5[3] = centersX5[2] + cardCompleteRelativeSize()[0] + horizontalMargin;
        centersX5[4] = centersX5[3] + cardCompleteRelativeSize()[0] + horizontalMargin;

        float[] centerY = new float[7];
        centerY[3] = FIGURE[2] + ((FIGURE[3]-FIGURE[2])/2.0f) - (cardCompleteRelativeSize()[1]/2.0f);
        centerY[2] = centerY[3] - (cardCompleteRelativeSize()[1] * verticalOverlapped);
        centerY[1] = centerY[2] - (cardCompleteRelativeSize()[1] * verticalOverlapped);
        centerY[0] = centerY[1] - (cardCompleteRelativeSize()[1] * verticalOverlapped);
        centerY[4] = centerY[3] + (cardCompleteRelativeSize()[1] * verticalOverlapped);
        centerY[5] = centerY[4] + (cardCompleteRelativeSize()[1] * verticalOverlapped);
        centerY[6] = centerY[5] + (cardCompleteRelativeSize()[1] * verticalOverlapped);

        switch (era) {
            case FIRST:
                return firstEraFigura(centersX6, centersX5, centerY, index);
            case SECOND:
                return secondEraFigura(centersX6, centersX5, centerY, index);
            case THIRD:
                return thirdEraFigura(centersX6, centersX5, centerY, index);
            default:
                // ToDo volver a poner y solucionar! LOG.error("Era = " + era + ", index = " + index);
                return new float[]{-1f * cardCompleteAbsoluteSize()[0], -1f * cardCompleteAbsoluteSize()[1]};
        }
//        throw new IllegalArgumentException("Figure of era " + era + " not found");
    }
    @Override
    public float[] playedCard(boolean isPlayerDown, int index) {
        int xIndex = index / (int)CARDS_BY_COLUMN;
        int yIndex = index % (int)CARDS_BY_COLUMN;
        return playedCard(isPlayerDown, xIndex, yIndex);
    }
    @Override
    public float[] wonderedCard(boolean isPlayerDown, int index) {
        float[] wonderPos = wonders(isPlayerDown, index);
        float button = wonderPos[1];
        float left = wonderPos[0] + wonderResumeRelativeSize()[0] - (cardCompleteRelativeSize()[0] * 0.9f);
        return new float[]{left * WIDTH_SCREEN, button * HEIGHT_SCREEN};
    }
    //</editor-fold>

    //<editor-fold desc="Wonders">
    @Override
    public float[] wonders(boolean isPlayerDown, int index) {
        float left = isPlayerDown ? WONDERS_DOWN[0] : WONDERS_UP[0];
        left += X_MARGIN;
        float right = isPlayerDown ? WONDERS_DOWN[1] : WONDERS_UP[1];
        right -= X_MARGIN;
        float button = isPlayerDown ? WONDERS_DOWN[2] : WONDERS_UP[2];
        button += Y_MARGIN;
        float top = isPlayerDown ? WONDERS_DOWN[3] : WONDERS_UP[3];
        top -= Y_MARGIN;

        float sloteWidth = (right - left) / 2.0f;
        float sloteHeight = (top - button) / 2.0f;

        float x = left + (index % 2) * sloteWidth;
        float y = button + (index / 2) * sloteHeight;

        return new float[]{x * WIDTH_SCREEN, y * HEIGHT_SCREEN};
    }

    @Override
    public float[] wonderSelectable(int index) {
        float left1 = SELECTABLE_ZONE[0];
        left1 += X_MARGIN;
        float left2 = SELECTABLE_ZONE[0] + wonderCompleteRelativeSize()[0] + X_MARGIN;
        float button1 = SELECTABLE_ZONE[2];
        button1 += Y_MARGIN;
        float button2 = SELECTABLE_ZONE[2] + wonderCompleteRelativeSize()[1] + Y_MARGIN;
        switch (index) {
            case 0:
                return new float[]{left1 * WIDTH_SCREEN, button1 * HEIGHT_SCREEN};
            case 1:
                return new float[]{left2 * WIDTH_SCREEN, button1 * HEIGHT_SCREEN};
            case 2:
                return new float[]{left1 * WIDTH_SCREEN, button2 * HEIGHT_SCREEN};
            case 3:
                return new float[]{left2 * WIDTH_SCREEN, button2 * HEIGHT_SCREEN};
        }
        throw new IllegalArgumentException("Incorrect index at wonderSelectable: " + index);
    }
    //</editor-fold>

    //<editor-fold desc="Board">
    @Override
    public float[] board() {
        return new float[]{(BOARD[0]+X_MARGIN)*WIDTH_SCREEN, (BOARD[1]-X_MARGIN)*WIDTH_SCREEN, (BOARD[2]+Y_MARGIN)*HEIGHT_SCREEN, (BOARD[3]-Y_MARGIN)*HEIGHT_SCREEN};
    }
    //</editor-fold>

    //<editor-fold desc="Money">
    @Override
    public float[] money(boolean isPlayerDown) {
        return isPlayerDown ?
                new float[]{(MONEY_DOWN[0]+X_MARGIN) * WIDTH_SCREEN, (MONEY_DOWN[1]-X_MARGIN) * WIDTH_SCREEN, (MONEY_DOWN[2]+Y_MARGIN) * HEIGHT_SCREEN, (MONEY_DOWN[3]-Y_MARGIN) * HEIGHT_SCREEN} :
                new float[]{(MONEY_UP[0]+X_MARGIN) * WIDTH_SCREEN, (MONEY_UP[1]-X_MARGIN) * WIDTH_SCREEN, (MONEY_UP[2]+Y_MARGIN) * HEIGHT_SCREEN, (MONEY_UP[3]-Y_MARGIN) * HEIGHT_SCREEN};
    }
    //</editor-fold>

    //<editor-fold desc="Private Cards tools">
    private float[] playedCard(boolean isPlayerDown, int xIndex, int yIndex) {
        float left = isPlayerDown ? CARDS_DOWN[0] : CARDS_UP[0];
        left += X_MARGIN;
        float right = isPlayerDown ? CARDS_DOWN[1] : CARDS_UP[1];
        right -= X_MARGIN;
        float sloteWidth = (right - left) / CARDS_BY_ROW;
        float x = left + (sloteWidth * xIndex);
        float width = (CARDS_DOWN[1]-CARDS_DOWN[0]) * SIZE_FACTOR / CARDS_BY_ROW;
        float baseY = isPlayerDown ? (CARDS_DOWN[3]-Y_MARGIN) : (CARDS_UP[2]+Y_MARGIN);
        float sloteHeight = isPlayerDown ? (CARDS_DOWN[3] - CARDS_DOWN[2]) / CARDS_BY_COLUMN : (CARDS_UP[3] - CARDS_UP[2]) / CARDS_BY_COLUMN;
        if (isPlayerDown) {
            float top = baseY - (sloteHeight * yIndex);
            float button = top - cardResumeRelativeSize()[1];
            LOG.debug("DOWN xIndex: " + xIndex + ", yIndex: " + yIndex + " => (" + x + ", " + button + ")");
            return new float[] {x * WIDTH_SCREEN, button * HEIGHT_SCREEN};
        } else {
            float button = baseY + (sloteHeight * yIndex);
            LOG.debug("UP xIndex: " + xIndex + ", yIndex: " + yIndex + " => (" + x + ", " + button + ")");
            return new float[] {x * WIDTH_SCREEN, button * HEIGHT_SCREEN};
        }
    }
    private float[] firstEraFigura(float[] centersX6, float[] centersX5, float[] centerY, int index) {
        float x, y;
        if (index < 6){
            x = centersX6[index];
            y = centerY[1];
        } else if (index < 11){
            x = centersX5[index-6];
            y = centerY[2];
        } else if (index < 15) {
            x = centersX6[index-10];
            y = centerY[3];
        } else if (index < 18) {
            x = centersX5[index - 14];
            y = centerY[4];
        } else {
            x = centersX6[index - 16];
            y = centerY[5];
        }
        LOG.debug("Index: " + index + " [" + x + ", " + (x + cardCompleteRelativeSize()[0]) + ", " + y + ", " + (y + cardCompleteRelativeSize()[1]) + "] [" + (x * WIDTH_SCREEN) + ", " + (y * HEIGHT_SCREEN) + "]");
        return new float[]{x * WIDTH_SCREEN, y * HEIGHT_SCREEN};
    }
    private float[] secondEraFigura(float[] centersX6, float[] centersX5, float[] centerY, int index) {
        float x, y;
        if (index < 2){
            x = centersX6[2 + index];
            y = centerY[1];
        } else if (index < 5){
            x = centersX5[index-1];
            y = centerY[2];
        } else if (index < 9) {
            x = centersX6[index-4];
            y = centerY[3];
        } else if (index < 14) {
            x = centersX5[index - 9];
            y = centerY[4];
        } else {
            x = centersX6[index - 14];
            y = centerY[5];
        }
        LOG.debug("Index: " + index + " [" + x + ", " + (x + cardCompleteRelativeSize()[0]) + ", " + y + ", " + (y + cardCompleteRelativeSize()[1]) + "] [" + (x * WIDTH_SCREEN) + ", " + (y * HEIGHT_SCREEN) + "]");
        return new float[]{x * WIDTH_SCREEN, y * HEIGHT_SCREEN};
    }
    private float[] thirdEraFigura(float[] centersX6, float[] centersX5, float[] centerY, int index) {
        float x, y;
        if (index < 2){
            x = centersX6[2 + index];
            y = centerY[0];
        } else if (index < 5){
            x = centersX5[index-1];
            y = centerY[1];
        } else if (index < 9) {
            x = centersX6[index-4];
            y = centerY[2];
        } else if (index == 9) {
            x = centersX5[1];
            y = centerY[3];
        } else if (index == 10) {
            x = centersX5[3];
            y = centerY[3];
        } else if(index < 15) {
            x = centersX6[index-10];
            y = centerY[4];
        } else if(index < 18) {
            x = centersX5[index-14];
            y = centerY[5];
        } else {
            x = centersX6[index-16];
            y = centerY[6];
        }
        LOG.debug("Index: " + index + " [" + x + ", " + (x + cardCompleteRelativeSize()[0]) + ", " + y + ", " + (y + cardCompleteRelativeSize()[1]) + "] [" + (x * WIDTH_SCREEN) + ", " + (y * HEIGHT_SCREEN) + "]");
        return new float[]{x * WIDTH_SCREEN, y * HEIGHT_SCREEN};
    }
    //</editor-fold>

    //<editor-fold desc="Private Detect Move tools">
    private boolean in(float[] centerXYAbs, float[] rangeXXYYRelative) {
        float xMin = rangeXXYYRelative[0] * WIDTH_SCREEN;
        float xMax = rangeXXYYRelative[1] * WIDTH_SCREEN;
        float yMin = rangeXXYYRelative[2] * HEIGHT_SCREEN;
        float yMax = rangeXXYYRelative[3] * HEIGHT_SCREEN;
        return centerXYAbs[0] >= xMin &&
                centerXYAbs[0] <= xMax &&
                centerXYAbs[1] >= yMin &&
                centerXYAbs[1] <= yMax;
    }
    private int onWonderUp(float[] center) {
        if (in(center, WONDERS_UP)) {
            float x = center[0];
            float y = center[1];
            float verticalDivisor = WONDERS_UP[0] + (WONDERS_UP[1]-WONDERS_UP[0]) / 2.0f;
            float horizontalDivisor = WONDERS_UP[2] + (WONDERS_UP[3]-WONDERS_UP[2]) / 2.0f;
            if (x > verticalDivisor) {
                if (y > horizontalDivisor) {
                    return 1;
                } else {
                    return 3;
                }
            } else {
                if (y > horizontalDivisor) {
                    return 0;
                } else {
                    return 2;
                }
            }
        }
        return -1;
    }
    private int onWonderDown(float[] center) {
        if (in(center, WONDERS_DOWN)) {
            float x = center[0];
            float y = center[1];
            float verticalDivisor = WONDERS_DOWN[0] + (WONDERS_DOWN[1]-WONDERS_DOWN[0]) / 2.0f;
            float horizontalDivisor = WONDERS_DOWN[2] + (WONDERS_DOWN[3]-WONDERS_DOWN[2]) / 2.0f;
            if (x > verticalDivisor) {
                if (y > horizontalDivisor) {
                    return 1;
                } else {
                    return 3;
                }
            } else {
                if (y > horizontalDivisor) {
                    return 0;
                } else {
                    return 2;
                }
            }
        }
        return -1;
    }
    private boolean cardPlayedByDown(float[] center) {
        return in(center, CARDS_DOWN);
    }
    private boolean onMoneyUp(float[] center) {
        return in(center, MONEY_UP);
    }
    private boolean onMoneyDown(float[] center) {
        return in(center, MONEY_DOWN);
    }
    private boolean cardPlayedByUp(float[] center) {
        return in(center, CARDS_UP);
    }
    private boolean selectedByDown(float[] center) {
        if (in(center, WONDERS_DOWN)) return true;
        if (in(center, CARDS_DOWN)) return true;
        return false;
    }
    private boolean selectedByUp(float[] center) {
        if (in(center, WONDERS_UP)) return true;
        if (in(center, CARDS_UP)) return true;
        return false;
    }
    //</editor-fold>

    //<editor-fold desc="Private Size tools">
    private float[] moneyRelativeSize() {
        float x = (MONEY_DOWN[1] - MONEY_DOWN[0]) * SIZE_FACTOR;
        float y = (MONEY_DOWN[3] - MONEY_DOWN[2]) * SIZE_FACTOR;
        float xy = x / y;
        float ab = (float) MoneyActor.MONEY_REGION_WITH / (float)MoneyActor.MONEY_REGION_HEIGHT;
        if (xy > ab) {
            x = y * ab / (WIDTH_SCREEN/HEIGHT_SCREEN);
        } else if (xy < ab){
            y = x / (ab / (WIDTH_SCREEN/HEIGHT_SCREEN));
        }
        return new float[]{x, y};
    }
    private float[] boardRelativeSize() {
        return new float[]{BOARD[1]-BOARD[0], BOARD[3]-BOARD[2]};
    }
    private float[] progressTokenRelativeSize() {
        float x = Math.min((PROGRESS_TOKENS_UP[1]-PROGRESS_TOKENS_UP[0]), (PROGRESS_TOKENS_UP[3]-PROGRESS_TOKENS_UP[2]));
        x = Math.min(x, (PROGRESS_TOKENS_ON_BOARD[1]-PROGRESS_TOKENS_ON_BOARD[0]));
        x = Math.min(x, ((PROGRESS_TOKENS_ON_BOARD[3]-PROGRESS_TOKENS_ON_BOARD[2])/5.0f));
        return new float[]{x, x}; // Son cuadrados (redondos)
    }
    private float[] wonderCompleteRelativeSize() {
        float x = ((SELECTABLE_ZONE[1] - SELECTABLE_ZONE[0]) / 2.0f) * SIZE_FACTOR;
        float y = ((SELECTABLE_ZONE[3] - SELECTABLE_ZONE[2]) / 2.0f) * SIZE_FACTOR;
        float xy = x / y;
        float ab = (float) WonderActor.REGION_COMPLETE_WIDTH / (float)WonderActor.REGION_COMPLETE_HEIGHT;
        if (xy > ab) {
            x = y * ab / (WIDTH_SCREEN/HEIGHT_SCREEN);
        } else if (xy < ab){
            y = x / (ab / (WIDTH_SCREEN/HEIGHT_SCREEN));
        }
        return new float[]{x, y};
    }
    private float[] wonderResumeRelativeSize() {
        float x = ((WONDERS_DOWN[1] - WONDERS_DOWN[0]) / 2.0f) * SIZE_FACTOR;
        float y = ((WONDERS_DOWN[3] - WONDERS_DOWN[2]) / 2.0f) * SIZE_FACTOR;
        float xy = x / y;
        float ab = (float) WonderActor.REGION_RESUME_WIDTH / (float)WonderActor.REGION_RESUME_HEIGHT;
        if (xy > ab) {
            x = y * ab / (WIDTH_SCREEN/HEIGHT_SCREEN);
        } else if (xy < ab){
            y = x / (ab / (WIDTH_SCREEN/HEIGHT_SCREEN));
        }
        return new float[]{x, y};
    }
    private float[] cardCompleteRelativeSize() {
        float x = (FIGURE[1] - FIGURE[0]) / 6.0f * SIZE_FACTOR;
        float y = (FIGURE[3] - FIGURE[2]) / (7.0f * (1 - FIGURE_OVERLAPED));
        float xy = x / y;
        float ab = (float) CardActor.REGION_COMPLETE_WIDTH / (float)CardActor.REGION_COMPLETE_HEIGHT;
        if (xy > ab) {
            x = y * ab / (WIDTH_SCREEN/HEIGHT_SCREEN);
        } else if (xy < ab){
            y = x / (ab / (WIDTH_SCREEN/HEIGHT_SCREEN));
        }
        return new float[]{x, y};
    }
    private float[] cardResumeRelativeSize() {
        float x = (CARDS_DOWN[1]-CARDS_DOWN[0]) * SIZE_FACTOR / CARDS_BY_ROW;
        float y = (CARDS_DOWN[3]-CARDS_DOWN[2]) / CARDS_BY_COLUMN;
        float xy = x / y;
        float ab = (float)CardActor.REGION_RESUME_WIDTH / (float)CardActor.REGION_RESUME_HEIGHT;
        if (xy > ab) {
            x = y * ab / (WIDTH_SCREEN/HEIGHT_SCREEN);
        } else if (xy < ab){
            y = x / (ab / (WIDTH_SCREEN/HEIGHT_SCREEN));
        }
        return new float[]{x, y};
    }
    //</editor-fold>
}
