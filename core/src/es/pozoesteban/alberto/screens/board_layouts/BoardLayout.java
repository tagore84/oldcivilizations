package es.pozoesteban.alberto.screens.board_layouts;

import es.pozoesteban.alberto.model.entities.Entity;
import es.pozoesteban.alberto.model.enums.Era;
import es.pozoesteban.alberto.model.players.Move;

/**
 * Return positions on board, float[4]{left, right, button, top}
 * War token and debs token made changing the board texture! // ToDo
 */
public interface BoardLayout {

    /**
     * Deduce la acción solicitada a partir de la entidad movida la posición donde se ha dejado.
     * Para poner la Wonder adecuada, se necesita llamar a Move.parse(returned, wonders);
     * @param isPlayerDown
     * @param entity
     * @param center coordenadas X Y donde se ha dejado la entidad, ¡EL CENTRO!
     * @return La acción que se supone que se quería realizar al mover la entidad
     */
    public Move guessMove(boolean isPlayerDown, Entity entity, float[] center);

    //<editor-fold desc="ProgressTokens">
    public float[] progressTokensAbsoluteSize();
    public float[] progressTokenOnBoard(int index);
    public float[] progressTokenSelectable(int index);
    public float[] progressTokenPlayed(boolean isPlayerDown, int index);
    //</editor-fold>

    //<editor-fold desc="Wonders">
    public float[] wonderCompleteAbsoluteSize();
    public float[] wonderResumeAbsoluteSize();
    public float[] wonders(boolean isPlayerDown, int index);
    public float[] wonderSelectable(int index);
    //</editor-fold>

    //<editor-fold desc="Cards">
    public float[] cardResumeAbsoluteSize();
    public float[] cardCompleteAbsoluteSize();
    public float[] figure(Era era, int index);
    public float[] playedCard(boolean isPlayerDown, int index);
    public float[] wonderedCard(boolean isPlayerDown, int index);
    //</editor-fold>

    //<editor-fold desc="Board">
    public float[] boardAbsoluteSize();
    public float[] board();
    //</editor-fold>

    //<editor-fold desc="Money">
    public float[] moneyAbsoluteSize();
    public float[] money(boolean isPlayerDown);
    //</editor-fold>

}
