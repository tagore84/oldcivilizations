package es.pozoesteban.alberto.screens.board_layouts;

public class BoardLayoutFactory {

    /**
     * Aquí solicitamos siempre el layout, de momento para cambiar tocamos el
     * código aquí, en el futuro dar el que toque en función de algún parámetro
     * de configuración...
     * @return BoardLayout a utilizar en el juego...
     */
    public static BoardLayout get() {
//        return new FiveZonesLayout();
//        return new ThreeWondersPlusOne();
        return new SquareWonders();
    }
}
