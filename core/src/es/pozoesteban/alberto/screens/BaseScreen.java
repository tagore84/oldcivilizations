package es.pozoesteban.alberto.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;

import com.badlogic.gdx.scenes.scene2d.Stage;
import es.pozoesteban.alberto.GameApplication;

import java.io.Serializable;

public class BaseScreen implements Screen {

    protected GameApplication gameApplication;
    protected Stage stage;

    public BaseScreen(GameApplication gameApplication){
        this.gameApplication = gameApplication;
    }

    @Override
    public void show() {
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        stage.dispose();
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {

    }

}
