package es.pozoesteban.alberto.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.GameApplication;
import es.pozoesteban.alberto.model.players.HumanPlayer;
import es.pozoesteban.alberto.ai.PedritoElAleatorio;
import es.pozoesteban.alberto.model.players.Player;
import es.pozoesteban.alberto.model.players.PlayerFactory;

import static es.pozoesteban.alberto.GameApplication.LOG_LEVEL;
import static es.pozoesteban.alberto.model.players.PlayerFactory.HUMAN;

public class MenuScreen extends BaseScreen{

    private static Logger LOG = new Logger("MenuScreen", LOG_LEVEL);


    /** The skin that we use to set the style of the buttons. */
    private Skin skin;
    private TextButton play;

    private SelectBox selectPlayerDown;
    private SelectBox selectPlayerUp;

    public MenuScreen(final GameApplication gameApplication) {
        super(gameApplication);

        skin = new Skin(Gdx.files.internal("uiskin.json"));


    }

    public void createActors() {
        play = new TextButton("Play", skin);
        selectPlayerDown = new SelectBox(skin);
        selectPlayerUp = new SelectBox(skin);
        selectPlayerDown.setItems(PlayerFactory.TYPES);
        selectPlayerUp.setItems(PlayerFactory.TYPES);

        play.addCaptureListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                String upTypeDown = selectPlayerDown.getSelected().toString();
                String upTypeUp = selectPlayerUp.getSelected().toString();
                Player down = upTypeDown.equalsIgnoreCase(HUMAN) ? PlayerFactory.buildHuman("DownHuman", true) :
                        PlayerFactory.buildAI(upTypeDown, gameApplication.getGameState(), true);
                Player up = upTypeUp.equalsIgnoreCase(HUMAN) ? PlayerFactory.buildHuman("UpHuman", false) :
                        PlayerFactory.buildAI(upTypeUp, gameApplication.getGameState(), false);           try {
                gameApplication.startGame(down, up);
                } catch (Exception ex) {
                    LOG.error("Error initializing the BoardScreen Game", ex);
                }
            }
        });

        play.setSize(200, 80);
        play.setPosition(40, 0);
        selectPlayerDown.setSize(200, 80);
        selectPlayerDown.setPosition(40, 140);
        selectPlayerUp.setSize(200, 80);
        selectPlayerUp.setPosition(40, 280);
    }

    @Override
    public void show() {
        super.show();

        createActors();

        stage.addActor(play);
        stage.addActor(selectPlayerDown);
        stage.addActor(selectPlayerUp);
    }

    @Override
    public void hide() {
        super.hide();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.2f, 0.3f, 0.5f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act();
        stage.draw();
    }
    @Override
    public void dispose() {
        this.skin.dispose();
        stage.dispose();
    }
}
