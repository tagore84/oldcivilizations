package es.pozoesteban.alberto.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import es.pozoesteban.alberto.GameApplication;
import es.pozoesteban.alberto.ai.*;
import es.pozoesteban.alberto.model.GameState;
import es.pozoesteban.alberto.model.players.HumanPlayer;
import es.pozoesteban.alberto.model.players.Player;
import es.pozoesteban.alberto.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DesktopLauncher {
    public static void main (String[] arg) throws IOException, ClassNotFoundException {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "SWD";
        config.height = 550;
        config.width = (int)((float)config.height * 9f / 16f);
//        config.width = (int)((float)config.height * 9f / 18f);
        config.forceExit = true;
        GameApplication game = new GameApplication();
        if (true) {
            List<Player[]> players = new ArrayList();
            debugUser(players);
            game.simulatedGame(players);
        } else if(false){
            game.loadGame(Utils.loadGame("game_537_3_4.game"));
        }
        new LwjglApplication(game, config);
    }

    private static void insertMatchs(List<Player[]> players) {
        for (int i = 0; i < 300; i++) {
            players.add(new Player[]{new PoncioElPonderado(true, new float[]
                    {Utils.DICE.nextFloat(), Utils.DICE.nextFloat(),Utils.DICE.nextFloat()}),
                    new PoncioElPonderado(false, new float[]
                            {Utils.DICE.nextFloat(), Utils.DICE.nextFloat(),Utils.DICE.nextFloat()})});
        }
    }
    private static void insertMatchs2(List<Player[]> players, GameState gameState) {
        float[][][][] cfg = new float[3][3][3][3];
        cfg[0][0][0] = new float[]{0.25f, 0.25f, 0.25f};
        cfg[0][0][1] = new float[]{0.25f, 0.25f, 0.50f};
        cfg[0][0][2] = new float[]{0.25f, 0.25f, 0.75f};
        cfg[0][1][0] = new float[]{0.25f, 0.50f, 0.25f};
        cfg[0][1][1] = new float[]{0.25f, 0.50f, 0.50f};
        cfg[0][1][2] = new float[]{0.25f, 0.50f, 0.75f};
        cfg[0][2][0] = new float[]{0.25f, 0.75f, 0.25f};
        cfg[0][2][1] = new float[]{0.25f, 0.75f, 0.50f};
        cfg[0][2][2] = new float[]{0.25f, 0.75f, 0.75f};

        cfg[1][0][0] = new float[]{0.50f, 0.25f, 0.25f};
        cfg[1][0][1] = new float[]{0.50f, 0.25f, 0.50f};
        cfg[1][0][2] = new float[]{0.50f, 0.25f, 0.75f};
        cfg[1][1][0] = new float[]{0.50f, 0.50f, 0.25f};
        cfg[1][1][1] = new float[]{0.50f, 0.50f, 0.50f};
        cfg[1][1][2] = new float[]{0.50f, 0.50f, 0.75f};
        cfg[1][2][0] = new float[]{0.50f, 0.75f, 0.25f};
        cfg[1][2][1] = new float[]{0.50f, 0.75f, 0.50f};
        cfg[1][2][2] = new float[]{0.50f, 0.75f, 0.75f};

        cfg[2][0][0] = new float[]{0.75f, 0.25f, 0.25f};
        cfg[2][0][1] = new float[]{0.75f, 0.25f, 0.50f};
        cfg[2][0][2] = new float[]{0.75f, 0.25f, 0.75f};
        cfg[2][1][0] = new float[]{0.75f, 0.50f, 0.25f};
        cfg[2][1][1] = new float[]{0.75f, 0.50f, 0.50f};
        cfg[2][1][2] = new float[]{0.75f, 0.50f, 0.75f};
        cfg[2][2][0] = new float[]{0.75f, 0.75f, 0.25f};
        cfg[2][2][1] = new float[]{0.75f, 0.75f, 0.50f};
        cfg[2][2][2] = new float[]{0.75f, 0.75f, 0.75f};

        for (int a1 = 0; a1 < 3; a1++) {
            for (int b1 = 0; b1 < 3; b1++) {
                for (int c1 = 0; c1 < 3; c1++) {
                    for (int a2 = 0; a2 < 3; a2++) {
                        for (int b2 = 0; b2 < 3; b2++) {
                            for (int c2 = 0; c2 < 3; c2++) {
                                players.add(new Player[]{new PoncioElPonderado(true, cfg[a1][b1][c1]),
                                        new MaximoMinguez(false, new float[]{10f})
//                                        new PoncioElPonderado(game, false, cfg[a2][b2][c2])
                                });
                            }
                        }
                    }
                }
            }
        }
    }
    private static void oneRandomMatch(List<Player[]> players) {
        players.add(new Player[]{
                new PedritoElAleatorio(true),
                new PedritoElAleatorio(false)
        });
    }
    private static void maximoVsPedrito(List<Player[]> players) {
        for (int i = 0; i < 25; i++) {
            players.add(new Player[]{
                    new PedritoElAleatorio(true),
                    new MaximoMinguez(false, new float[]{15})
            });
            players.add(new Player[]{
                    new MaximoMinguez(true, new float[]{15}),
                    new PedritoElAleatorio(false)
            });
        }
    }
    private static void debugTrillo(List<Player[]> players) {
        players.add(new Player[]{
                new TrilloElExpertillo(true, new float[0]),
                new TrilloElExpertillo(false, new float[0])
        });
    }
    private static void debugPedrito(List<Player[]> players) {
        players.add(new Player[]{
                new PedritoElAleatorio(true),
                new PedritoElAleatorio(false)
        });
    }
    private static void debugMaximo(List<Player[]> players) {
        players.add(new Player[]{
                new MaximoMinguez(true, new float[]{2, Integer.MAX_VALUE}),
                new KapitanMikace(false)
        });
    }
    private static void debugUser(List<Player[]> players) {
        players.add(new Player[]{
                new HumanPlayer("Tester", true),
                new PedritoElAleatorio(false)
        });
    }
    private static void testTrilloVsRandom(List<Player[]> players) {
        for (int i = 0; i < 25; i++) {
            players.add(new Player[]{
                    new PedritoElAleatorio(true),
                    new TrilloElExpertillo( false, new float[]{})
            });
            players.add(new Player[]{
                    new TrilloElExpertillo(true, new float[]{}),
                    new PedritoElAleatorio( false)
            });
        }
    }
    private static void testMaximo(List<Player[]> players) {
        for (int i = 0; i < 50; i++) {
            players.add(new Player[]{
                    new MaximoMinguez(true, new float[]{5, Integer.MAX_VALUE}),
                    new MaximoMinguez( false, new float[]{5, Integer.MAX_VALUE})
            });
            players.add(new Player[]{
                    new MaximoMinguez(true, new float[]{5, Integer.MAX_VALUE}),
                    new MaximoMinguez( false, new float[]{5, Integer.MAX_VALUE})
            });
        }
    }
    private static void testMaximoVsRandom(List<Player[]> players) {
        for (int i = 0; i < 50; i++) {
            players.add(new Player[]{
                    new PedritoElAleatorio(true),
                    new MaximoMinguez( false, new float[]{Integer.MAX_VALUE, 1})
            });
            players.add(new Player[]{
                    new MaximoMinguez(true, new float[]{Integer.MAX_VALUE, 1}),
                    new PedritoElAleatorio( false)
            });
        }
    }
    private static void testMaximoVsTrillo(List<Player[]> players) {
        for (int i = 0; i < 25; i++) {
            players.add(new Player[]{
                    new TrilloElExpertillo(true, new float[0]),
                    new MaximoMinguez( false, new float[]{15})
            });
            players.add(new Player[]{
                    new MaximoMinguez(true, new float[]{15}),
                    new TrilloElExpertillo( false, new float[0])
            });
        }
    }
    private static void testPoncioVsRandom(List<Player[]> players) {
        for (int i = 0; i < 25; i++) {
            players.add(new Player[]{
                    new PoncioElPonderado(true, new float[]{0.5f, 0.2f, 0.5f}),
                    new PedritoElAleatorio( false)
            });
            players.add(new Player[]{
                    new PedritoElAleatorio(true),
                    new PoncioElPonderado( false, new float[]{0.5f, 0.2f, 0.5f})
            });
        }
    }

}